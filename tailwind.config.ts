import typography from "@tailwindcss/typography"
import type { Config } from "tailwindcss"
import colors from "tailwindcss/colors"

const config: Config = {
  content: ["./src/**/*.{html,js,svelte,ts}"],
  plugins: [
    typography,
    function ({ addUtilities }) {
      const extendLineThrough = {
        ".line-through-amendment": {
          textDecoration: "line-through",
          "text-decoration-color": "rgba(222, 213, 0, 0.7)",
          "text-decoration-thickness": "2px",
        },
        ".line-through-bill": {
          textDecoration: "line-through",
          "text-decoration-color": "rgba(255, 107, 107, 0.7)",
          "text-decoration-thickness": "2px",
        },
      }
      addUtilities(extendLineThrough)
    },
  ],
  theme: {
    extend: {
      animation: {
        blink: "blinker 300ms ease-in 2",
        "pulse-2": "pulse-2 1.7s cubic-bezier(0.4, 0, 0.6, 1) infinite",
      },
      backgroundImage: {
        /* Graph paper - Heropatterns.com échelle réduite */
        "graph-paper":
          "url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='50' height='50' viewBox='0 0 100 100'%3E%3Crect width='100%' height='100%' fill='%23fff'/%3E%3Cg fill-rule='evenodd'%3E%3Cg fill='%23ded500' fill-opacity='0.4'%3E%3Cpath opacity='.5' d='M96 95h4v1h-4v4h-1v-4h-9v4h-1v-4h-9v4h-1v-4h-9v4h-1v-4h-9v4h-1v-4h-9v4h-1v-4h-9v4h-1v-4h-9v4h-1v-4h-9v4h-1v-4H0v-1h15v-9H0v-1h15v-9H0v-1h15v-9H0v-1h15v-9H0v-1h15v-9H0v-1h15v-9H0v-1h15v-9H0v-1h15v-9H0v-1h15V0h1v15h9V0h1v15h9V0h1v15h9V0h1v15h9V0h1v15h9V0h1v15h9V0h1v15h9V0h1v15h9V0h1v15h4v1h-4v9h4v1h-4v9h4v1h-4v9h4v1h-4v9h4v1h-4v9h4v1h-4v9h4v1h-4v9h4v1h-4v9zm-1 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-9-10h9v-9h-9v9zm10 0h9v-9h-9v9zm10 0h9v-9h-9v9zm10 0h9v-9h-9v9zm10 0h9v-9h-9v9zm10 0h9v-9h-9v9zm10 0h9v-9h-9v9zm10 0h9v-9h-9v9zm9-10v-9h-9v9h9zm-10 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-9-10h9v-9h-9v9zm10 0h9v-9h-9v9zm10 0h9v-9h-9v9zm10 0h9v-9h-9v9zm10 0h9v-9h-9v9zm10 0h9v-9h-9v9zm10 0h9v-9h-9v9zm10 0h9v-9h-9v9zm9-10v-9h-9v9h9zm-10 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-9-10h9v-9h-9v9zm10 0h9v-9h-9v9zm10 0h9v-9h-9v9zm10 0h9v-9h-9v9zm10 0h9v-9h-9v9zm10 0h9v-9h-9v9zm10 0h9v-9h-9v9zm10 0h9v-9h-9v9zm9-10v-9h-9v9h9zm-10 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-10 0v-9h-9v9h9zm-9-10h9v-9h-9v9zm10 0h9v-9h-9v9zm10 0h9v-9h-9v9zm10 0h9v-9h-9v9zm10 0h9v-9h-9v9zm10 0h9v-9h-9v9zm10 0h9v-9h-9v9zm10 0h9v-9h-9v9z'/%3E%3Cpath d='M6 5V0H5v5H0v1h5v94h1V6h94V5H6z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E\")",
        /* Polka dots - Heropatterns.com échelle réduite */
        "polka-dots":
          "url(\"data:image/svg+xml,%3Csvg width='8' height='8' viewBox='0 0 20 20' xmlns='http://www.w3.org/2000/svg'%3E%3Crect width='100%' height='100%' fill='%23fff'/%3E%3Cg fill='%23ded500' fill-opacity='0.4' fill-rule='evenodd'%3E%3Ccircle cx='3' cy='3' r='3'/%3E%3Ccircle cx='13' cy='13' r='3'/%3E%3C/g%3E%3C/svg%3E\")",
      },
      blur: {
        xs: "1.2px",
        xxs: "0.8px",
      },
      colors: {
        gray: colors.neutral,
        "le-bleu": "#343bff",
        "le-bleu-light": "#d2dfff",
        "le-jaune-light": "#EEEA8A",
        "le-jaune": "#ded500",
        "le-jaune-dark": "#9d970b",
        "le-jaune-very-dark": "#6E6A08",
        "le-rouge-bill": "#ff6b6b",
        "le-gris-dispositif": "#5E709E",
        "le-gris-dispositif-ultralight": "#EBEFFA",
        "le-gris-dispositif-light": "#CCD3E7",
        "le-gris-dispositif-dark": "#2F406A",
        "le-vert-validation": "#13CC03",
        "le-vert-validation-dark": "#377330",
        "le-vert": {
          50: "#f1f0e6",
          100: "#e2e1cd",
          200: "#c5c39c",
          300: "#a8a66a",
          400: "#8b8839",
          500: "#6e6a07",
          600: "#635f06",
          700: "#585506",
          800: "#424004",
          900: "#2c2a03",
          950: "#161501",
        },
      },
      fontSize: {
        xxs: ["0.625rem", "0.75rem"],
      },
      keyframes: {
        blinker: {
          "50%": { opacity: "60%" },
        },
        "pulse-2": {
          "0%, 100%": {
            opacity: 1,
          },
          "50%": {
            opacity: 0.2,
          },
        },
      },
      scale: {
        25: "0.25",
      },
      transitionDuration: {
        250: "250ms",
        350: "350ms",
        400: "400ms",
        450: "450ms",
        550: "550ms",
        600: "600ms",
        650: "650ms",
      },
      transitionTimingFunction: {
        "in-quad": "cubic-bezier(.55, .085, .68, .53)",
        "in-cubic": "cubic-bezier(.550, .055, .675, .19)",
        "in-quart": "cubic-bezier(.895, .03, .685, .22)",
        "in-quint": "cubic-bezier(.755, .05, .855, .06)",
        "in-expo": "cubic-bezier(.95, .05, .795, .035)",
        "in-circ": "cubic-bezier(.6, .04, .98, .335)",
        "in-back": "cubic-bezier(.36, 0, .66, -.56)",

        "out-quad": "cubic-bezier(.25, .46, .45, .94)",
        "out-cubic": "cubic-bezier(.215, .61, .355, 1)",
        "out-quart": "cubic-bezier(.165, .84, .44, 1)",
        "out-quint": "cubic-bezier(.23, 1, .32, 1)",
        "out-expo": "cubic-bezier(.19, 1, .22, 1)",
        "out-circ": "cubic-bezier(.075, .82, .165, 1)",
        "out-back": "cubic-bezier(.34, 1.56, .64, 1)",

        "in-out-quad": "cubic-bezier(.455, .03, .515, .955)",
        "in-out-cubic": "cubic-bezier(.645, .045, .355, 1)",
        "in-out-quart": "cubic-bezier(.77, 0, .175, 1)",
        "in-out-quint": "cubic-bezier(.86, 0, .07, 1)",
        "in-out-expo": "cubic-bezier(1, 0, 0, 1)",
        "in-out-circ": "cubic-bezier(.785, .135, .15, .86)",
        "in-out-back": "cubic-bezier(.68, -.6, .32, 1.6)",
      },
      zIndex: {
        25: "25",
      },
    },
    fontFamily: {
      sans: ["Lato", "sans-serif"],
      serif: ["Lora", "serif"],
      mono: [
        "ui-monospace",
        "SFMono-Regular",
        "Menlo",
        "Monaco",
        "Consolas",
        "Liberation Mono",
        "Courier New",
        "monospace",
      ],
    },
    screens: {
      sm: "640px",
      // => @media (min-width: 640px) { ... }
      md: "768px",
      // => @media (min-width: 768px) { ... }
      lg: "1024px",
      // => @media (min-width: 1024px) { ... }
      xl: "1280px",
      // => @media (min-width: 1280px) { ... }
      "2xl": "1536px",
      // => @media (min-width: 1536px) { ... }
      "3xl": "2200px",
      // => @media (min-width: 2200px) { ... }
    },
  },
}

export default config
