import type { PlaywrightTestConfig } from "@playwright/test"

const config: PlaywrightTestConfig = {
  webServer: {
    command: "npm run build && npm run preview",
    port: 3000,
    timeout: 1200000, // in milliseconds
  },
  testDir: "tests",
  testMatch: /(.+\.)?(test|spec)\.[jt]s/,
}

export default config
