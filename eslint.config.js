import { includeIgnoreFile } from "@eslint/compat"
import js from "@eslint/js"
import prettier from "eslint-config-prettier"
import globals from "globals"
import svelte from "eslint-plugin-svelte"
import { fileURLToPath } from "node:url"
import ts from "typescript-eslint"

const gitignorePath = fileURLToPath(new URL("./.gitignore", import.meta.url))

export default ts.config(
  includeIgnoreFile(gitignorePath),
  js.configs.recommended,
  ...ts.configs.recommended,
  ...svelte.configs["flat/recommended"],
  prettier,
  ...svelte.configs["flat/prettier"],
  {
    languageOptions: {
      globals: {
        ...globals.browser,
        ...globals.node,
      },
    },
  },
  {
    files: ["**/*.svelte"],
    languageOptions: {
      parserOptions: {
        parser: ts.parser,
      },
    },
    // rules: {
    //   "import/order": [
    //     "warn",
    //     {
    //       alphabetize: {
    //         order: "asc",
    //         caseInsensitive: true,
    //       },

    //       "newlines-between": "always",
    //     },
    //   ],

    //   "import/no-unresolved": "off",
    // },
    settings: {
      svelte: {
        ignoreWarnings: ["no-irregular-whitespace", "svelte/no-at-html-tags"],
      },
    },
  },
  {
    ignores: ["build/", ".svelte-kit/", "dist/", "gitlab-ci/build/"],
  },
)
