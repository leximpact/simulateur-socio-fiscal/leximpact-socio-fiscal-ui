/// <reference types="@sveltejs/kit" />

// See: https://github.com/isaacHagoel/svelte-dnd-action#typescript
// Needed until following bug is solved:
// https://github.com/sveltejs/language-tools/issues/431.
declare type DndEvent = import("svelte-dnd-action").DndEvent
declare namespace svelte.JSX {
  interface HTMLAttributes<T> {
    onconsider?: (
      event: CustomEvent<DndEvent> & { target: EventTarget & T },
    ) => void
    onfinalize?: (
      event: CustomEvent<DndEvent> & { target: EventTarget & T },
    ) => void
  }
}
