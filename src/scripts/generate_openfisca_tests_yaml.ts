import type { Situation } from "@openfisca/json-model"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"
import YAML from "js-yaml"
import testCasesCoreUnknown from "@leximpact/socio-fiscal-openfisca-json/test_cases.json"
import {
  summaryCalculatedVariablesName,
  otherCalculatedVariablesName,
  variableSummaryByName,
} from "$lib/variables"
import { nonVirtualVariablesName } from "$lib/decompositions"
import type { EntityByKey, Entity, GroupEntity } from "@openfisca/json-model"
import { entityByKey } from "$lib/entities"

type JsonObject = { [key: string]: any }

const optionsDefinitions = [
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    alias: "y",
    help: "Year to ask as simulation output",
    name: "year",
    type: Number,
  },
  {
    defaultOption: true,
    help: "Directory to write OpenFisca test YAML files",
    name: "outdir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function fetchWithRetries(
  url: string,
  options: RequestInit,
  maxRetries = 5,
  delay = 10000,
) {
  for (let attempt = 1; attempt <= maxRetries; attempt++) {
    try {
      const response = await fetch(url, options)

      if (response.ok) {
        return response
      }

      console.warn(
        `Calling Openfisca API. Attempt ${attempt} : Failed with status ${response.status}`,
      )
    } catch (error) {
      console.error(`Error on attempt ${attempt} :`, error)
    }

    if (attempt < maxRetries) {
      console.log(`New attempt in ${delay / 1000}s...`)
      await new Promise((res) => setTimeout(res, delay))
    }
  }

  throw new Error(`Failed calling OpenFisca API after ${maxRetries} attempts`)
}

function removeSpacesFromKeys(
  obj: JsonObject,
  replacedKeys: { [key: string]: string },
): JsonObject {
  const newObj: JsonObject = {}

  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      let newKey: string | number = key
        .normalize("NFD")
        .replace(/[\u0300-\u036f]/g, "")
        .toLowerCase()
        .replace(/ |’|°/g, "_")

      if (newKey !== key) {
        replacedKeys[key] = newKey
      }

      const value = obj[key]

      if (
        typeof value === "object" &&
        value !== null &&
        !Array.isArray(value)
      ) {
        newObj[newKey] = removeSpacesFromKeys(value, replacedKeys)
      } else {
        newObj[newKey] = value
      }
    }
  }

  return newObj
}

function replaceValuesFromReplacedKeys(
  obj: JsonObject,
  replacedKeys: { [key: string]: string },
): JsonObject {
  const newObj: JsonObject = {}

  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      const value = obj[key]

      if (
        typeof value === "object" &&
        value !== null &&
        !Array.isArray(value)
      ) {
        newObj[key] = replaceValuesFromReplacedKeys(value, replacedKeys)
      } else if (
        typeof value === "string" &&
        replacedKeys.hasOwnProperty(value)
      ) {
        newObj[key] = replacedKeys[value]
      } else if (Array.isArray(value)) {
        const newArray = []
        for (const arrayValue of value) {
          if (
            typeof arrayValue === "string" &&
            replacedKeys.hasOwnProperty(arrayValue)
          ) {
            newArray.push(replacedKeys[arrayValue])
          } else {
            newArray.push(arrayValue)
          }
        }
        newObj[key] = newArray
      } else {
        newObj[key] = value
      }
    }
  }

  return newObj
}

function removeZeroValues(obj: any): any {
  if (typeof obj !== "object" || obj === null) {
    return obj
  }

  if (Array.isArray(obj)) {
    return obj.map(removeZeroValues)
  }

  const newObj: any = {}

  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      const value = obj[key]

      if (typeof value === "object" && value !== null) {
        if (Object.values(value).every((v) => v === 0 || v === null)) {
          continue
        }

        const cleanedValue = removeZeroValues(value)
        if (Object.keys(cleanedValue).length > 0) {
          newObj[key] = cleanedValue
        }
      } else {
        newObj[key] = value
      }
    }
  }

  return newObj
}

function cleanSimulatedJson(jsonData: any): any {
  const sections = ["familles", "foyers_fiscaux", "menages", "individus"]

  for (const section of sections) {
    if (jsonData[section]) {
      jsonData[section] = Object.fromEntries(
        Object.entries(jsonData[section]).map(([key, value]) => [
          key,
          removeZeroValues(value),
        ]),
      )
    }
  }

  return jsonData
}

function splitVariablesByCategory(
  entityData: JsonObject,
  outputVariablesList: string[],
) {
  const input: JsonObject = {}
  const output: JsonObject = {}

  for (const [variable, value] of Object.entries(entityData)) {
    if (outputVariablesList.includes(variable)) {
      if (
        variableSummaryByName[variable].value_type === "Enum" &&
        variableSummaryByName[variable].possible_values !== undefined
      ) {
        // Replace Enum values by their index in possible_values
        const newValue: Record<string, number> = {}
        for (const [period, periodValue] of Object.entries(value)) {
          newValue[period] = Object.keys(
            variableSummaryByName[variable].possible_values,
          ).indexOf(periodValue as string)
        }
        output[variable] = newValue
      } else {
        output[variable] = value
      }
    } else {
      input[variable] = value
    }
  }
  return { input, output }
}

function processSection(
  sectionData: JsonObject,
  outputVariablesList: string[],
) {
  const inputSection: JsonObject = {}
  const outputSection: JsonObject = {}

  for (const [entityKey, entityData] of Object.entries(sectionData)) {
    const { input, output } = splitVariablesByCategory(
      entityData,
      outputVariablesList,
    )

    if (Object.keys(input).length > 0) {
      inputSection[entityKey] = input
    }
    if (Object.keys(output).length > 0) {
      outputSection[entityKey] = output
    }
  }

  return { input: inputSection, output: outputSection }
}

function buildYamlOutput(
  name: string,
  year: string,
  simulatedTestCase: JsonObject,
  outputVariablesList: string[],
) {
  const sections = ["familles", "foyers_fiscaux", "menages", "individus"]
  const jsonForYamlOutput: {
    name: string
    period: string
    max_spiral_loops: number
    input: JsonObject
    output: JsonObject
  } = {
    name: name,
    period: year,
    max_spiral_loops: 4,
    input: {},
    output: {},
  }

  for (const section of sections) {
    if (simulatedTestCase[section]) {
      const { input, output } = processSection(
        simulatedTestCase[section],
        outputVariablesList,
      )

      if (Object.keys(input).length > 0) {
        jsonForYamlOutput.input[section] = input
      }
      if (Object.keys(output).length > 0) {
        jsonForYamlOutput.output[section] = output
      }
    }
  }

  return jsonForYamlOutput
}

function mensualiseVariable(obj: any, variable: string) {
  if (!obj || typeof obj !== "object") return obj

  const annuelValues = obj
  const monthlyValues: any = {}

  for (const year in annuelValues) {
    if (typeof annuelValues[year] === "number") {
      const monthlyAmount =
        variableSummaryByName[variable]["set_input"] ===
        "set_input_divide_by_period"
          ? parseFloat((annuelValues[year] / 12).toFixed(2))
          : annuelValues[year]
      for (let month = 1; month <= 12; month++) {
        const monthStr = month < 10 ? `0${month}` : `${month}`
        monthlyValues[`${year}-${monthStr}`] = monthlyAmount
      }
    } else {
      monthlyValues[year] = annuelValues[year]
    }
  }

  obj = monthlyValues
  return obj
}

function getEntityKeyFromKeyPlural(
  keyPlural: string,
  entityByKey: EntityByKey,
): string | null {
  for (const entity of Object.values(entityByKey)) {
    if (entity.key_plural === keyPlural) {
      return entity.key
    }
  }
  return null
}

function isGroupEntity(entity: Entity): entity is GroupEntity {
  return (entity as GroupEntity).roles !== undefined
}

function isKeyOrPlural(str: string): boolean {
  const keys = Object.keys(entityByKey)

  for (const key of keys) {
    const entity = entityByKey[key]
    if (entity.key === str || entity.key_plural === str) {
      return true
    }
    if (isGroupEntity(entity) && entity.roles) {
      for (const role of entity.roles) {
        if (role.key === str || role.key_plural === str) {
          return true
        }
        if (role.subroles) {
          for (const subrole of role.subroles) {
            if (subrole.key === str || subrole.key_plural === str) {
              return true
            }
          }
        }
      }
    }
  }

  return false
}

async function main() {
  console.info("Initializing...")
  const personsEntityKey = Object.entries(entityByKey)
    .filter(([, entity]) => entity.is_person)
    .map(([key]) => key)[0]

  const year = options.year
  const outdir = options.outdir

  const variablesFromDecompositions = [
    ...summaryCalculatedVariablesName,
    ...otherCalculatedVariablesName,
    ...nonVirtualVariablesName,
  ]

  const openFiscaVariablesListApiCall = await fetch(
    "https://api.fr.openfisca.org/latest/variables",
    {
      method: "GET",
    },
  )
  if (!openFiscaVariablesListApiCall.ok) {
    console.error(
      `Error ${
        openFiscaVariablesListApiCall.status
      } while calling Openfisca France API`,
    )
    return
  }

  const openFiscaVariablesList = await openFiscaVariablesListApiCall.json()

  const variablesToCalculate = variablesFromDecompositions.filter(
    (variable: string) => openFiscaVariablesList.hasOwnProperty(variable),
  )

  const testCasesCore = testCasesCoreUnknown as unknown as Situation[]

  for (const testCase of testCasesCore) {
    console.info("Processing ", testCase.id)
    let testCaseContainsMissingVariable = false
    let jsonForApiCall: JsonObject = {}
    const outputVariablesList: Array<string> = []

    jsonForApiCall.input = {}

    const replacedKeys: { [key: string]: string } = {}

    const sections = ["familles", "foyers_fiscaux", "menages", "individus"]

    // Mensualise monthly input variables
    for (const section of sections) {
      if (testCase[section] !== undefined) {
        for (const entityKey in testCase[section]) {
          for (const variable in testCase[section][entityKey]) {
            if (
              variableSummaryByName.hasOwnProperty(variable) &&
              variableSummaryByName[variable].definition_period === "month"
            ) {
              testCase[section][entityKey][variable] = mensualiseVariable(
                testCase[section][entityKey][variable],
                variable,
              )
            } else if (
              !openFiscaVariablesList.hasOwnProperty(variable) &&
              !isKeyOrPlural(variable)
            ) {
              console.warn(
                "variable",
                variable,
                "does not exists in openFisca-France.",
              )
              testCaseContainsMissingVariable = true
            }
          }
        }
      }
    }

    if (testCaseContainsMissingVariable) {
      console.warn("Ignoring test case", testCase.id)
      continue
    }

    for (const section of sections) {
      if (testCase[section] !== undefined) {
        jsonForApiCall.input[section] = removeSpacesFromKeys(
          testCase[section],
          replacedKeys,
        )
        for (const entityKey in jsonForApiCall.input[section]) {
          for (const variable of variablesToCalculate) {
            if (variableSummaryByName[variable] !== undefined) {
              if (
                variableSummaryByName[variable].entity ===
                  getEntityKeyFromKeyPlural(section, entityByKey) &&
                jsonForApiCall.input[section][entityKey][variable] === undefined
              ) {
                outputVariablesList.push(variable)
                switch (variableSummaryByName[variable].definition_period) {
                  case "year": {
                    jsonForApiCall.input[section][entityKey][variable] = {
                      [year]: null,
                    }
                    break
                  }
                  case "month": {
                    const monthVariableSection = {}
                    for (let month = 1; month <= 12; month++) {
                      const monthString = month < 10 ? "0" + month : month
                      const monthPeriod = year + "-" + monthString
                      Object.assign(monthVariableSection, {
                        [monthPeriod]: null,
                      })
                    }
                    jsonForApiCall.input[section][entityKey][variable] =
                      monthVariableSection
                  }
                }
              }
            } else {
              console.log(
                "cette variable n'existe pas dans summary : ",
                variable,
              )
            }
          }
        }
      }
    }

    jsonForApiCall = replaceValuesFromReplacedKeys(jsonForApiCall, replacedKeys)
    console.info("Launching simulation on OpenFisca France API...")
    const openFiscaApiUrl = "https://api.fr.openfisca.org/latest/calculate"
    const openFiscaApiOptions: RequestInit = {
      body: JSON.stringify(jsonForApiCall.input),
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json; charset=utf-8",
      },
      method: "POST",
    }
    let simulatedTestCaseFromOpenFiscaApi: Response | null = null

    try {
      simulatedTestCaseFromOpenFiscaApi = await fetchWithRetries(
        openFiscaApiUrl,
        openFiscaApiOptions,
      )

      if (!simulatedTestCaseFromOpenFiscaApi.ok) {
        console.error(
          `Giving up calling openFisca API because of status ${simulatedTestCaseFromOpenFiscaApi.status}`,
        )
        return
      }
    } catch (error) {
      console.error("Error : ", error)
    }
    console.info("Post-processing...")
    const simulatedTestCase = cleanSimulatedJson(
      await simulatedTestCaseFromOpenFiscaApi!.json(),
    )

    const jsonForYamlOutput = buildYamlOutput(
      testCase.id!,
      year,
      simulatedTestCase,
      outputVariablesList,
    )

    const yamlOutput = YAML.dump(jsonForYamlOutput, {
      noCompatMode: true,
      noRefs: true,
    })

    // Remove quotes around year keys
    const cleanedYaml = yamlOutput.replace(/'(\d{4})':/g, "$1:")
    await fs.writeFile(path.join(outdir, testCase.id + ".yml"), cleanedYaml)
    console.info("YAML file written :", outdir, testCase.id + ".yml")
  }
}

main()
  .then(() => {
    process.exit(0)
  })
  .catch((error: unknown) => {
    console.error(error)
    process.exit(1)
  })
