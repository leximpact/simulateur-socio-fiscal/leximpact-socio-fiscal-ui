import "dotenv/config"
import path from "path"

import fs from "fs-extra"

const simulationsBudgetDir = process.env.SIMULATIONS_BUDGET_DIR as string
const simulationsTestCasesDir = process.env.SIMULATIONS_TEST_CASES_DIR as string

// Remove private budget simulations from index.
const indexFilePath = path.join(simulationsBudgetDir, "index.json")
if (fs.pathExistsSync(indexFilePath)) {
  const publicSimulationsSummary = (
    fs.readJsonSync(indexFilePath) as Array<{ public: boolean }>
  ).filter((summary) => summary.public)
  fs.writeJsonSync(indexFilePath, publicSimulationsSummary, {
    encoding: "utf-8",
    spaces: 2,
  })
}

// Remove every budget simulation from cache.
const budgetsDir = path.join(simulationsBudgetDir, "responses")
if (fs.pathExistsSync(budgetsDir)) {
  if (fs.lstatSync(budgetsDir).isDirectory()) {
    fs.removeSync(budgetsDir)
  }
}

// Remove every test case simulation from cache.
const testCasesDir = path.join(simulationsTestCasesDir, "responses")
if (fs.pathExistsSync(testCasesDir)) {
  if (fs.lstatSync(testCasesDir).isDirectory()) {
    fs.removeSync(testCasesDir)
  }
}

// Remove every test case opengraph image from cache.
const testCasesOpengraphsDir = path.join(simulationsTestCasesDir, "opengraphs")
if (fs.pathExistsSync(testCasesOpengraphsDir)) {
  if (fs.lstatSync(testCasesOpengraphsDir).isDirectory()) {
    fs.removeSync(testCasesOpengraphsDir)
  }
}
