import type { DisplayMode } from "$lib/displays"
import type { ParametricReform } from "$lib/reforms"
import type { Situation } from "$lib/situations"

export interface CachedSimulation {
  date: string
  hash: string
  output_variables: string[]
  parameters: string[]
  public: boolean
  title: string
}

export interface EntitySimulation {
  [key: string]: { [date: string]: number } | string[]
}

export interface FamilleSimulation extends EntitySimulation {
  enfants: string[]
  parents: string[]
}

export interface FoyerFiscalSimulation extends EntitySimulation {
  declarants: string[]
  personnes_a_charge: string[]
}

export type IndividuSimulation = EntitySimulation

export interface MenageSimulation extends EntitySimulation {
  conjoint: string[]
  enfants: string[]
  personne_de_reference: string[]
}

export interface Simulation {
  familles: { [name: string]: FamilleSimulation }
  foyers_fiscaux: { [name: string]: FoyerFiscalSimulation }
  individus: { [name: string]: IndividuSimulation }
  menages: { [name: string]: MenageSimulation }
}

export async function publishTestCaseSimulation(
  blobImageOpenGraph: Blob | null | undefined,
  displayMode: DisplayMode,
  inputInstantsByVariableNameArray: Array<{
    [name: string]: Set<string>
  }>,
  parametricReform: ParametricReform,
  testCases: Situation[],
): Promise<string | undefined> {
  const urlString = "/test_cases/simulations"

  const formData = new FormData()

  if (blobImageOpenGraph !== undefined && blobImageOpenGraph !== null) {
    formData.append("blob", blobImageOpenGraph)
  }

  formData.append(
    "body",
    JSON.stringify({
      displayMode: {
        ...displayMode,
        parameterHash: undefined,
      },
      inputInstantsByVariableNameArray: inputInstantsByVariableNameArray.map(
        (inputInstantsByVariableName) =>
          Object.fromEntries(
            Object.entries(inputInstantsByVariableName).map(
              ([variableName, inputInstants]) => [
                variableName,
                [...inputInstants],
              ],
            ),
          ),
      ),
      parametricReform: parametricReform,
      testCases: testCases,
    }),
  )

  const res = await fetch(urlString, {
    body: formData,
    method: "POST",
  })

  if (!res.ok) {
    console.error(
      `Error ${
        res.status
      } while creating a share link at ${urlString}\n\n${await res.text()}`,
    )
    return
  }

  const { token } = await res.json()

  return token
}
