import reformChangesByNameUnknown from "@leximpact/socio-fiscal-openfisca-json/reforms_changes.json"
import type {
  Decomposition as DecompositionCore,
  ScaleAtInstant,
  Variable,
} from "@openfisca/json-model"

import { metadata } from "$lib/metadata"

export type ParameterReform = ScaleParameterReform | ValueParameterReform

export interface ParameterReformBase {
  start: string
  stop?: string
  type: ParameterReformChangeType
}

export enum ParameterReformChangeType {
  Parameter = "parameter",
  Scale = "scale",
}

export interface ParametricReform {
  [parameterName: string]: ParameterReform
}

export interface ReformChanges {
  decompositions: { [name: string]: DecompositionCore | null }
  editable_processed_parameters: { [key: string]: unknown }
  variables_summaries: { [name: string]: Variable | null }
}

export type ReformChangesByName = { [name: string]: ReformChanges }

export interface ScaleParameterReform extends ParameterReformBase {
  type: ParameterReformChangeType.Scale
  scale: ScaleAtInstant
}

export interface ValueParameterReform extends ParameterReformBase {
  type: ParameterReformChangeType.Parameter
  value: number
}

export const reformChangesByName =
  reformChangesByNameUnknown as ReformChangesByName

export const reformMetadataByName = Object.fromEntries(
  (metadata.reforms ?? []).map((reformMetadata) => [
    reformMetadata.name,
    reformMetadata,
  ]),
)
