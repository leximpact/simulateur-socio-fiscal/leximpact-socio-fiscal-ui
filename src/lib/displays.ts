export interface DisplayMode {
  action?: string // TODO: Remove attribute "action".
  budget?: boolean
  mobileLaw?: boolean // For mobile, when not in edit mode: When true, show law, otherwise show impacts.
  parametersVariableName?: string
  edit?: number
  testCasesIndex: number[] // Contains at least the index of a single test case.
  variableName?: string
  waterfallName: string
  parameterHash?: string
}
