import testCasesCoreUnknown from "@leximpact/socio-fiscal-openfisca-json/test_cases.json"
import {
  type Entity,
  type EntityByKey,
  getRolePersonsIdKey,
  type GroupEntity,
  type PopulationWithoutId,
  type Situation as SituationBase,
  type Variable,
} from "@openfisca/json-model"

import type { CalculationName } from "$lib/calculations.svelte"
import { entityByKey } from "$lib/entities"
import type { ParametricReform } from "$lib/reforms"
import type {
  ValuesByCalculationNameByVariableName,
  VariableValue,
  VariableValueByCalculationName,
} from "$lib/variables"
// import { variableSummaryByName } from "$lib/variables"

export interface ActiveSlider extends Slider {
  stepValue: number
  vectorIndex: number
  yMax: number
  yMin: number
}

export interface Axis {
  count: number
  index?: number
  max: number
  min: number
  name: string
  period?: string
  situationIndex?: number
}

export interface Calculation {
  input?: TestCasesCalculationInput
  running?: boolean
  situationIndex?: number
}

export type CalculationByName = Partial<{
  [calculationName in CalculationName]: Calculation
}>

export type Situation = SituationBase & {
  dixieme?: number
  /// The slider currently used
  slider?: ActiveSlider
  /// List of sliders that can be used for this situation
  sliders?: Slider[]
}

export type SituationWithAxes = Situation & {
  axes?: Axis[][]
}

export interface Slider {
  entity: string // Entity key
  id: string // Population ID
  max: number
  min: number
  name: string // Variable name
  // steps: number // Number of steps to use between min & max; currently always 101.
}

export interface TestCasesCalculationInput {
  period: string
  parametric_reform?: ParametricReform
  reform?: string
  situation: SituationWithAxes
  variables: string[]
}

export const testCasesCore = testCasesCoreUnknown as unknown as Situation[]

// export function buildTestCasesWithoutNonInputVariables(
//   inputInstantsByVariableNameArray: Array<{
//     [name: string]: Set<string>
//   }>,
//   testCases: Situation[],
// ): Situation[] {
//   const entities = Object.values(entityByKey)
//   testCases = [...testCases]
//   for (let [situationIndex, situation] of testCases.entries()) {
//     situation = testCases[situationIndex] = { ...situation }
//     const inputInstantsByVariableName =
//       inputInstantsByVariableNameArray[situationIndex]
//     for (const entity of entities) {
//       let entitySituation = situation[entity.key_plural]
//       if (entitySituation === undefined) {
//         continue
//       }
//       entitySituation = situation[entity.key_plural] = { ...entitySituation }
//       const reservedKeys = getPopulationReservedKeys(entity)
//       for (let [populationId, population] of Object.entries(
//         entitySituation,
//       ).sort(([populationId1], [populationId2]) =>
//         populationId1.localeCompare(populationId2),
//       )) {
//         population = entitySituation[populationId] = { ...population }
//         for (const [variableName, variableValueByInstant] of Object.entries(
//           population,
//         )) {
//           if (reservedKeys.has(variableName)) {
//             continue
//           }
//           const inputVariableValueByInstant: {
//             [instant: string]: VariableValue | null
//           } = {}
//           const inputInstants =
//             inputInstantsByVariableName[variableName] ?? new Set<string>()
//           for (const [instant, variableValue] of Object.entries(
//             variableValueByInstant,
//           )) {
//             if (!inputInstants.has(instant)) {
//               // Remove calculated value.
//               continue
//             }
//             inputVariableValueByInstant[instant] = variableValue
//           }
//           if (Object.keys(inputVariableValueByInstant).length > 0) {
//             population[variableName] = inputVariableValueByInstant
//           } else {
//             delete population[variableName]
//           }
//         }
//       }
//     }
//   }
//   return testCases
// }

export function extractInputInstantsFromTestCases(
  testCases: Situation[],
): Array<{
  [name: string]: Set<string>
}> {
  const inputInstantsByVariableNameArray: Array<{
    [name: string]: Set<string>
  }> = []
  const entities = Object.values(entityByKey)
  for (const situation of testCases) {
    const inputInstantsByVariableName: {
      [name: string]: Set<string>
    } = {}
    inputInstantsByVariableNameArray.push(inputInstantsByVariableName)
    for (const entity of entities) {
      const entitySituation = situation[entity.key_plural]
      if (entitySituation === undefined) {
        continue
      }
      const reservedKeys = getPopulationReservedKeys(entity)
      for (const population of Object.values(entitySituation)) {
        for (const [variableName, variableValueByInstant] of Object.entries(
          population,
        )) {
          if (reservedKeys.has(variableName)) {
            continue
          }
          let inputInstants = inputInstantsByVariableName[variableName]
          if (inputInstants === undefined) {
            inputInstants = inputInstantsByVariableName[variableName] =
              new Set()
          }
          for (const instant of Object.keys(variableValueByInstant)) {
            inputInstants.add(instant)
          }
        }
      }
    }
  }
  return inputInstantsByVariableNameArray
}

export function getCalculatedVariableValueByCalculationName(
  situation: Situation,
  valuesByCalculationNameByVariableName: ValuesByCalculationNameByVariableName,
  variable: Variable,
  populationId: string,
): VariableValueByCalculationName {
  const entity = entityByKey[variable.entity]
  const entitySituation = situation[entity.key_plural]
  const sortedPopulationIds = Object.keys(entitySituation).sort(
    (populationId1, populationId2) =>
      populationId1.localeCompare(populationId2),
  )
  const populationIndex = sortedPopulationIds.indexOf(populationId)
  const situationPopulationCount = Object.keys(entitySituation).length
  const valuesByCalculationName =
    valuesByCalculationNameByVariableName[variable.name] ?? {}
  const vectorIndex = situation.slider?.vectorIndex ?? 0
  const valueIndex = vectorIndex * situationPopulationCount + populationIndex
  return Object.fromEntries(
    Object.entries(valuesByCalculationName).map(([calculationName, values]) => [
      calculationName,
      values[valueIndex] ?? variable.default_value,
    ]),
  )
}

export function getPopulationReservedKeys(entity: Entity): Set<string> {
  return new Set(
    entity.is_person
      ? ["name"]
      : [
          "name",
          ...(entity as GroupEntity).roles.map((role) =>
            getRolePersonsIdKey(role),
          ),
        ],
  )
}

export function getSituationVariableValue(
  situation: Situation,
  variable: Variable,
  populationId: string,
  year: number,
): VariableValue {
  const entity = entityByKey[variable.entity]
  const entitySituation = situation[entity.key_plural]
  return (
    entitySituation?.[populationId]?.[variable.name]?.[year] ??
    variable.default_value
  )
}

export function indexOfSituationPopulationId(
  entity: Entity,
  situation: Situation,
  populationId: string,
): number {
  const entitySituation = situation[entity.key_plural]
  return Object.keys(entitySituation)
    .sort(([populationId1], [populationId2]) =>
      populationId1.localeCompare(populationId2),
    )
    .indexOf(populationId)
}

export function* iterSituationVariablesName(
  entityByKey: EntityByKey,
  situation: Situation,
): Generator<string, void, unknown> {
  const variablesName = new Set<string>()
  for (const entity of Object.values(entityByKey)) {
    const reservedKeys = getPopulationReservedKeys(entity)
    for (const population of Object.values(
      situation[entity.key_plural] ?? {},
    ) as PopulationWithoutId[]) {
      for (const variableName of Object.keys(population)) {
        if (reservedKeys.has(variableName)) {
          continue
        }
        if (!variablesName.has(variableName)) {
          yield variableName
          variablesName.add(variableName)
        }
      }
    }
  }
}

export function isSituationCalculating(
  calculation: CalculationByName,
  situationIndex = undefined,
) {
  return (
    Object.values(calculation).filter(
      (calculation) =>
        calculation.running &&
        (calculation.situationIndex === undefined ||
          calculation.situationIndex === situationIndex),
    ).length > 0
  )
}

export function setSituationVariableValue(
  entityByKey: EntityByKey,
  situation: Situation,
  variable: Variable,
  populationId: string,
  year: number,
  value: VariableValue,
): boolean {
  if (value == null) {
    value = variable.default_value
  }
  const entity = entityByKey[variable.entity]
  const entitySituation = (situation[entity.key_plural!] ??= {})
  const populationSituation = (entitySituation[populationId] ??= {})
  const valueByPeriod = (populationSituation[variable.name] ??= {}) as {
    [date: string]: VariableValue | null
  }
  if (
    valueByPeriod[year - 2] === value &&
    valueByPeriod[year - 1] === value &&
    valueByPeriod[year] === value
  ) {
    return false
  }
  valueByPeriod[year - 2] = value
  valueByPeriod[year - 1] = value
  valueByPeriod[year] = value
  return true
}

// export function updateTestCasesVariableValues(
//   testCases: Situation[],
//   variableName: string,
//   valuesByCalculationNameByVariableNameArray: ValuesByCalculationNameByVariableName[],
//   year: number,
// ): Situation[] {
//   const entityKey = variableSummaryByName[variableName]?.entity
//   if (entityKey === undefined) {
//     return testCases
//   }
//   const entity = entityByKey[entityKey]
//   const values = variableValuesByName[variableName]
//   if (values === undefined) {
//     // Occurs for variables that are in test cases and are not calculated.
//     // For example: variable "depcom_foyer".
//     // For these variables, the situations shouldn't be updated.
//     return testCases
//   }

//   return testCases.map((situation, situationIndex) => {
//     const entitySituation = situation[entity.key_plural] ?? {}
//     const situationPopulationCount = Object.keys(entitySituation).length
//     const baseIndex =
//       (situation.slider?.vectorIndex ?? 0) * situationPopulationCount
//     for (const [situationPopulationIndex, [populationId, population]] of Object.entries(
//       entitySituation,
//     ).sort(([populationId1], [populationId2]) =>
//       populationId1.localeCompare(populationId2),
//     ).entries()) {
//       entitySituation[populationId] = {
//         ...(population ?? {}),
//         [variableName]: {
//           ...((population[variableName] ?? {}) as {
//             [date: string]: VariableValue | null
//           }),
//           [year]: values[baseIndex + situationPopulationIndex],
//         },
//       }
//     }
//     return {
//       ...situation,
//       [entity.key_plural]: entitySituation,
//     }
//   })
// }
