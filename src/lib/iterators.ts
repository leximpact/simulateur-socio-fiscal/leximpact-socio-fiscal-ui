export function* iterToLimit<T>(
  value: Iterable<T>,
  limit?: number | undefined | null,
): Generator<T, void, unknown> {
  let index = 0
  for (const item of value) {
    if (limit != null && index >= limit) {
      break
    }
    yield item
    index++
  }
}

export function* iterToDepth(depth: number): Generator<number, void, unknown> {
  for (let i = 0; i < depth; i++) {
    yield i
  }
}
