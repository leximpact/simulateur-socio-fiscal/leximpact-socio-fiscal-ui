import entityByKeyUnknown from "@leximpact/socio-fiscal-openfisca-json/entities.json"
import type { EntityByKey } from "@openfisca/json-model"

export const entityByKey = entityByKeyUnknown as EntityByKey

export const personEntityKey = Object.entries(entityByKey)
  .filter(([, entity]) => entity.is_person)
  .map(([key]) => key)[0]
