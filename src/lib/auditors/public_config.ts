import {
  auditBoolean,
  auditCleanArray,
  auditFunction,
  auditHttpUrl,
  auditInteger,
  auditRequire,
  auditSetNullish,
  auditString,
  auditStringToBoolean,
  auditStringToNumber,
  auditSwitch,
  auditTrimString,
  auditUnique,
  cleanAudit,
  type Audit,
} from "@auditors/core"

export function auditPublicConfig(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["advanced", "showTutorial"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditSwitch([auditTrimString, auditStringToBoolean], auditBoolean),
      auditSetNullish(false),
    )
  }
  audit.attribute(
    data,
    "apiBaseUrls",
    true,
    errors,
    remainingKeys,
    auditString,
    auditFunction((keys) => keys.split(",")),
    auditCleanArray(auditHttpUrl),
    auditUnique,
    auditRequire,
  )
  for (const key of [
    "appTitle",
    "childrenKey",
    "familyEntityKey",
    "householdEntityKey",
    "taxableHouseholdEntityKey",
  ]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditRequire,
    )
  }
  for (const key of ["baseUrl", "legalUrl", "portalUrl", "territoiresUrl"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditHttpUrl,
      auditRequire,
    )
  }
  for (const key of ["reformName", "revaluationName"]) {
    audit.attribute(data, key, true, errors, remainingKeys, auditTrimString)
  }
  audit.attribute(
    data,
    "hiddenEntitiesKeyPlural",
    true,
    errors,
    remainingKeys,
    auditString,
    auditFunction((keys) => keys.split(",")),
    auditCleanArray(auditTrimString),
    auditUnique,
  )
  audit.attribute(data, "matomo", true, errors, remainingKeys, auditMatomo)
  audit.attribute(
    data,
    "openfiscaRepository",
    true,
    errors,
    remainingKeys,
    auditRepositoryConfig,
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

function auditMatomo(audit: Audit, dataUnknown: unknown): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "prependDomain",
    true,
    errors,
    remainingKeys,
    auditSwitch([auditTrimString, auditStringToBoolean], auditBoolean),
    auditSetNullish(false),
  )
  audit.attribute(
    data,
    "siteId",
    true,
    errors,
    remainingKeys,
    auditStringToNumber,
    auditInteger,
    auditRequire,
  )
  audit.attribute(
    data,
    "subdomains",
    true,
    errors,
    remainingKeys,
    auditTrimString,
  )
  audit.attribute(
    data,
    "url",
    true,
    errors,
    remainingKeys,
    auditHttpUrl,
    // Ensure there is a single trailing "/" in URL.
    auditFunction((url) => url.replace(/\/$/, "") + "/"),
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function auditRepositoryConfig(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["branch", "group", "project"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditRequire,
    )
  }
  for (const key of ["rawUrlTemplate", "urlTemplate"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditHttpUrl,
      auditRequire,
    )
  }

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function validatePublicConfig(data: unknown): [unknown, unknown] {
  return auditPublicConfig(cleanAudit, data)
}
