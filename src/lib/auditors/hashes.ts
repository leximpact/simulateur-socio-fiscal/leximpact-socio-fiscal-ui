import {
  auditChain,
  auditTrimString,
  type Audit,
  type Auditor,
  auditArray,
  auditTest,
  auditFunction,
  auditSetNullish,
} from "@auditors/core"

export function auditHashSingleton(...auditors: Auditor[]) {
  return auditChain(
    auditArray(),
    auditTest(
      (values) => values.length <= 1,
      "Parameter must be present only once in hash",
    ),
    auditFunction((value) => value[0]),
    ...auditors,
  )
}

export function auditSimulationHash(
  audit: Audit,
  hash: unknown,
): [unknown, unknown] {
  if (typeof hash !== "string") {
    return audit.unexpectedType(hash, "string")
  }
  const query = new URLSearchParams((hash as string).replace(/^#/, ""))

  const data: { [key: string]: unknown } = {}
  for (const [key, value] of query.entries()) {
    let values = data[key] as string[] | undefined
    if (values === undefined) {
      values = data[key] = []
    }
    values.push(value)
  }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "parameterHash",
    true,
    errors,
    remainingKeys,
    auditHashSingleton(auditTrimString),
  )

  return audit.reduceRemaining(data, errors, remainingKeys, auditSetNullish({}))
}
