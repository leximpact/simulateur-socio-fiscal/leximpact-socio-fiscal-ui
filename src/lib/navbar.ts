export interface NavbarConfig {
  position: "relative" | "fixed"
  showSearch: boolean
}

export function getNavbarConfig(pageId: string): NavbarConfig {
  if (pageId === "/accueil" || pageId === "/fonctionnement") {
    return {
      position: "fixed",
      showSearch: false,
    }
  }

  return {
    position: "relative",
    showSearch: true,
  }
}
