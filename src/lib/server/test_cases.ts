import path from "path"

import serverConfig from "$lib/server/server_config"

export function getTestCasesRequestsDirPath(digest: string) {
  return path.join(
    serverConfig.simulationsTestCasesDir,
    "requests",
    digest.substring(0, 2),
  )
}
export function getTestCasesRequestsFilePath(digest: string) {
  return path.join(getTestCasesRequestsDirPath(digest), `${digest}.json`)
}

export function getTestCasesCacheDirPath(digest: string) {
  return path.join(
    serverConfig.simulationsTestCasesDir,
    "responses",
    digest.substring(0, 2),
  )
}
export function getTestCasesCacheFilePath(digest: string) {
  return path.join(getTestCasesCacheDirPath(digest), `${digest}.json`)
}

export function getTestCasesOpenGraphsDirPath(digest: string) {
  return path.join(
    serverConfig.simulationsTestCasesDir,
    "opengraphs",
    digest.substring(0, 2),
  )
}
export function getTestCasesOpenGraphsFilePath(digest: string) {
  return path.join(getTestCasesOpenGraphsDirPath(digest), `${digest}.png`)
}
