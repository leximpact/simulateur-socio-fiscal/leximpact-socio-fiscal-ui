import {
  ALLOW_ROBOTS,
  BUDGET_API_URL,
  BUDGET_DEMAND_PIPELINE_URL,
  BUDGET_DEMAND_PIPELINE_TOKEN,
  BUDGET_JWT_SECRET,
  GITHUB_PERSONAL_ACCESS_TOKEN,
  JWT_SECRET,
  OPENID_CONNECT_CLIENT_ID,
  OPENID_CONNECT_CLIENT_SECRET,
  OPENID_CONNECT_ISSUER_URL,
  SIMULATIONS_BUDGET_DIR,
  SIMULATIONS_TEST_CASES_DIR,
} from "$env/static/private"
import { validateServerConfig } from "$lib/server/auditors/server_config"

export interface ServerConfig {
  allowRobots: boolean
  budgetApiUrl?: string
  budgetDemandPipelineUrl?: string
  budgetDemandPipelineToken?: string
  budgetJwtSecret?: string
  githubPersonalAccessToken?: string
  jwtSecret: string
  openIdConnect?: {
    clientId: string
    clientSecret: string
    issuerUrl: string
  }
  simulationsBudgetDir: string
  simulationsTestCasesDir: string
}

const [validConfig, error] = validateServerConfig({
  allowRobots: ALLOW_ROBOTS,
  budgetApiUrl: BUDGET_API_URL,
  budgetDemandPipelineUrl: BUDGET_DEMAND_PIPELINE_URL,
  budgetDemandPipelineToken: BUDGET_DEMAND_PIPELINE_TOKEN,
  budgetJwtSecret: BUDGET_JWT_SECRET,
  githubPersonalAccessToken: GITHUB_PERSONAL_ACCESS_TOKEN,
  jwtSecret: JWT_SECRET,
  openIdConnect: OPENID_CONNECT_CLIENT_ID
    ? {
        clientId: OPENID_CONNECT_CLIENT_ID,
        clientSecret: OPENID_CONNECT_CLIENT_SECRET,
        issuerUrl: OPENID_CONNECT_ISSUER_URL,
      }
    : null,
  simulationsBudgetDir: SIMULATIONS_BUDGET_DIR,
  simulationsTestCasesDir: SIMULATIONS_TEST_CASES_DIR,
}) as [ServerConfig, unknown]
if (error !== null) {
  console.error(
    `Error in configuration:\n${JSON.stringify(
      validConfig,
      null,
      2,
    )}\nError:\n${JSON.stringify(error, null, 2)}`,
  )
  process.exit(-1)
}
const serverConfig = validConfig

export default serverConfig
