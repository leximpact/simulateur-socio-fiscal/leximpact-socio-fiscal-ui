import path from "path"

import serverConfig from "$lib/server/server_config"

export function getBudgetRequestsDirPath(digest: string) {
  return path.join(
    serverConfig.simulationsBudgetDir,
    "requests",
    digest.substring(0, 2),
  )
}
export function getBudgetRequestsFilePath(digest: string) {
  return path.join(getBudgetRequestsDirPath(digest), `${digest}.json`)
}

export function getBudgetCacheDirPath(digest: string) {
  return path.join(
    serverConfig.simulationsBudgetDir,
    "responses",
    digest.substring(0, 2),
  )
}
export function getBudgetCacheFilePath(digest: string) {
  return path.join(getBudgetCacheDirPath(digest), `${digest}.json`)
}
