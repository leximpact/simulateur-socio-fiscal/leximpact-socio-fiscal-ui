import { auditSetNullish, cleanAudit, type Audit } from "@auditors/core"

function auditLoginLogoutQuery(
  audit: Audit,
  query: URLSearchParams | null,
): [unknown, unknown] {
  if (query == null) {
    return [query, null]
  }
  if (!(query instanceof URLSearchParams)) {
    return audit.unexpectedType(query, "URLSearchParams")
  }

  const data: { [key: string]: string[] } = {}
  for (const [key, value] of query.entries()) {
    let values = data[key]
    if (values === undefined) {
      values = data[key] = []
    }
    values.push(value)
  }
  const errors = {}
  const remainingKeys = new Set(Object.keys(data))

  return audit.reduceRemaining(data, errors, remainingKeys, auditSetNullish({}))
}

export function validateLoginLogoutQuery(
  query: URLSearchParams,
): [unknown, unknown] {
  return auditLoginLogoutQuery(cleanAudit, query)
}
