import {
  auditBoolean,
  auditHttpUrl,
  auditRequire,
  auditSetNullish,
  auditStringToBoolean,
  auditSwitch,
  auditTrimString,
  cleanAudit,
  type Audit,
} from "@auditors/core"

export function auditServerConfig(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "allowRobots",
    true,
    errors,
    remainingKeys,
    auditSwitch([auditTrimString, auditStringToBoolean], auditBoolean),
    auditSetNullish(false),
  )
  for (const key of ["budgetApiUrl", "budgetDemandPipelineUrl"]) {
    audit.attribute(data, key, true, errors, remainingKeys, auditHttpUrl)
  }
  for (const key of [
    "jwtSecret",
    "simulationsBudgetDir",
    "simulationsTestCasesDir",
  ]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditRequire,
    )
  }
  for (const key of [
    "budgetDemandPipelineToken",
    "budgetJwtSecret",
    "githubPersonalAccessToken",
  ]) {
    audit.attribute(data, key, true, errors, remainingKeys, auditTrimString)
  }
  audit.attribute(
    data,
    "openIdConnect",
    true,
    errors,
    remainingKeys,
    auditOpenIdConnect,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

function auditOpenIdConnect(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  // for (const key of [
  //   "cert",
  //   "decryptionCertificatePath",
  //   "decryptionPrivateKeyPath",
  //   "identifierFormat",
  //   "signatureCertificatePath",
  //   "signaturePrivateKeyPath",
  // ]) {
  //   audit.attribute(data, key, true, errors, remainingKeys, auditTrimString)
  // }
  for (const key of ["clientId", "clientSecret"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditRequire,
    )
  }
  audit.attribute(
    data,
    "issuerUrl",
    true,
    errors,
    remainingKeys,
    auditHttpUrl,
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function validateServerConfig(data: unknown): [unknown, unknown] {
  return auditServerConfig(cleanAudit, data)
}
