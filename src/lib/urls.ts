import { waterfalls } from "$lib/decompositions"
import type { DisplayMode } from "$lib/displays"

export interface SelfTargetAProps {
  href: string
  "data-sveltekit-noscroll"?: boolean
}

export function newSelfTargetAProps(url: string): SelfTargetAProps {
  return {
    href: url,
  }
}

export function newSimulationUrl(displayMode: DisplayMode): string {
  const parametersQuery: URLSearchParams = new URLSearchParams()
  if (displayMode.budget) {
    parametersQuery.append("budget", "true")
  }
  if (displayMode.edit !== undefined) {
    parametersQuery.append("edit", displayMode.edit.toString())
  }
  if (displayMode.mobileLaw) {
    parametersQuery.append("law", "true")
  }
  if (
    displayMode.testCasesIndex !== undefined &&
    displayMode.testCasesIndex.length > 0
  ) {
    for (const testCaseIndex of displayMode.testCasesIndex) {
      parametersQuery.append("test_case", testCaseIndex.toString())
    }
  }
  if (displayMode.variableName !== undefined) {
    parametersQuery.append("variable", displayMode.variableName)
  }
  if (displayMode.parametersVariableName !== undefined) {
    parametersQuery.append("parameters", displayMode.parametersVariableName)
  }
  if (
    displayMode.waterfallName !== undefined &&
    displayMode.waterfallName !== waterfalls[0].name
  ) {
    parametersQuery.append("waterfall", displayMode.waterfallName)
  }
  const parametersQueryString = parametersQuery.toString()

  const hashesQuery: URLSearchParams = new URLSearchParams()
  if (displayMode.parameterHash !== undefined) {
    hashesQuery.append("parameterHash", displayMode.parameterHash)
  }
  const hashesQueryString = hashesQuery.toString()

  return `/${parametersQueryString ? "?" + parametersQueryString : ""}${
    hashesQueryString ? "#" + hashesQueryString : ""
  }`
}

export function stringifyQuery(queryParameters?: {
  [key: string]: string | string[]
}): string {
  return Object.entries(queryParameters ?? {})
    .reduce(
      (
        queryArray: string[],
        [key, value]: [string, string | string[]],
      ): string[] => {
        if (Array.isArray(value)) {
          for (const item of value) {
            queryArray.push(
              `${encodeURIComponent(key)}=${encodeURIComponent(item)}`,
            )
          }
        } else {
          queryArray.push(
            `${encodeURIComponent(key)}=${encodeURIComponent(value ?? "")}`,
          )
        }
        return queryArray
      },
      [],
    )
    .join("&")
    .replace(/\s/g, "+")
}
