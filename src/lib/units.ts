// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import unitsUnknown from "@leximpact/socio-fiscal-openfisca-json/units.yaml"
import {
  getUnitAtDate as getUnitAtDateOriginal,
  getUnitLabel as getUnitLabelOriginal,
  getUnitShortLabel as getUnitShortLabelOriginal,
  type ConstantUnit,
  type PeriodUnit,
  type Unit,
} from "@openfisca/json-model"

const frenchPluralRules = new Intl.PluralRules(["fr-FR"])
export const units = unitsUnknown as Unit[]
const unitByName = Object.fromEntries(units.map((unit) => [unit.name, unit]))

export function getUnitAtDate(
  name: string | undefined | null,
  date: string,
): ConstantUnit | undefined {
  return getUnitAtDateOriginal(unitByName, name, date)
}

export function getUnitByName(
  name: string | undefined | null,
): Unit | undefined {
  if (name === undefined || name === null) {
    return undefined
  }
  return unitByName[name]
}

export function getUnitLabel(
  name: string | undefined | null,
  date: string,
  { periodUnit, value = 123 }: { periodUnit?: PeriodUnit; value?: number } = {},
): string {
  const unitLabel = getUnitLabelOriginal(
    unitByName,
    name,
    date,
    frenchPluralRules.select(value ?? 0),
  )
  if (
    unitLabel === "" ||
    periodUnit === undefined ||
    !["day", "month", "year"].includes(periodUnit)
  ) {
    return unitLabel
  }
  return `${unitLabel}/an`
}

export function getUnitShortLabel(
  name: string | undefined | null,
  date: string,
  { periodUnit, value = 123 }: { periodUnit?: PeriodUnit; value?: number } = {},
): string {
  const unitShortLabel = getUnitShortLabelOriginal(
    unitByName,
    name,
    date,
    frenchPluralRules.select(value ?? 0),
  )
  if (
    unitShortLabel === "" ||
    periodUnit === undefined ||
    !["day", "month", "year"].includes(periodUnit)
  ) {
    return unitShortLabel
  }
  return `${unitShortLabel}/an`
}
