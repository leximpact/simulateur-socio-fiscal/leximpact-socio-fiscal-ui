import type { Parameter } from "@openfisca/json-model"
export interface RepositoryConfig {
  branch: string
  group: string
  project: string
  rawUrlTemplate: string
  urlTemplate: string
}

export function newParameterRepositoryRawUrl(
  { branch, group, project, rawUrlTemplate }: RepositoryConfig,
  parameter: Parameter,
): string | undefined {
  const path = parameter.file_path;
  if (path === undefined) {
    return undefined;
  }
  const url = rawUrlTemplate
    .replace("$<group>", group)
    .replace("$<project>", project)
    .replace("$<branch>", branch)
    .replace("$<path>", path);
  return url;
}

export function newParameterRepositoryUrl(
  { branch, group, project, urlTemplate }: RepositoryConfig,
  parameter: Parameter,
): string | undefined {
  const path = parameter.file_path;
  if (path === undefined) {
    return undefined;
  }
  const url = urlTemplate
    .replace("$<group>", group)
    .replace("$<project>", project)
    .replace("$<branch>", branch)
    .replace("$<path>", path);
  return url;
}
