import metadataUnknown from "@leximpact/socio-fiscal-openfisca-json/metadata.json"
import type { Metadata } from "@openfisca/json-model"

export const metadata: Metadata = metadataUnknown
