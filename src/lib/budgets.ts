export type BudgetCalculationComparison =
  | "base"
  | "bill"
  | "amendement_base"
  | "amendement_plf"

export interface BudgetCalculationResult {
  compare_before_after?:
    | {
        [compareWith in BudgetCalculationComparison]: BudgetPopulationComparison | null
      }
    | null
  quantiles: BudgetQuantile[]
  state_budget: StateBudget
}

export interface BudgetPopulationComparison {
  above_after: number
  lower_after: number
  neutral: number
  non_zero_after: number
  non_zero_before: number
  tolerance_factor_used: number
  total: number // Total number of French population (constant for base, reform & amendment)
}

export interface BudgetQuantile {
  assiette_csg_abattue: number
  assiette_csg_abattue_max: number
  assiette_csg_abattue_mean: number
  assiette_csg_abattue_min: number
  assiette_csg_abattue_sum: number
  count: number
  csg_deductible_retraite: number
  csg_deductible_retraite_max: number
  csg_deductible_retraite_mean: number
  csg_deductible_retraite_min: number
  csg_deductible_retraite_sum: number
  csg_deductible_salaire: number
  csg_deductible_salaire_max: number
  csg_deductible_salaire_mean: number
  csg_deductible_salaire_min: number
  csg_deductible_salaire_sum: number
  csg_imposable_retraite: number
  csg_imposable_retraite_max: number
  csg_imposable_retraite_mean: number
  csg_imposable_retraite_min: number
  csg_imposable_retraite_sum: number
  csg_imposable_salaire: number
  csg_imposable_salaire_max: number
  csg_imposable_salaire_mean: number
  csg_imposable_salaire_min: number
  csg_imposable_salaire_sum: number
  fraction: number
  irpp_economique: number
  irpp_economique_max: number
  irpp_economique_mean: number
  irpp_economique_min: number
  irpp_economique_sum: number
  af: number
  af_max: number
  af_mean: number
  af_min: number
  af_sum: number
  quantile_num: 1
  rfr_max: number
  rfr_mean: number
  rfr_min: number
  rfr_sum: number
  rfr_par_part_max: number
  rfr_par_part_mean: number
  rfr_par_part_min: number
  rfr_par_part_sum: number
  revenus_menage_par_uc_max: number
  revenus_menage_par_uc_mean: number
  revenus_menage_par_uc_min: number
  revenus_menage_par_uc_sum: number
  revenus_menage_max: number
  revenus_menage_mean: number
  revenus_menage_min: number
  revenus_menage_sum: number
}

export interface BudgetQuantileByCalculationName {
  law: BudgetQuantile
  revaluation?: BudgetQuantile
  bill?: BudgetQuantile
  amendment?: BudgetQuantile
}

export interface BudgetSimulation {
  errors?: unknown[]
  result?: {
    amendement: BudgetCalculationResult
    base: BudgetCalculationResult
    plf: BudgetCalculationResult
    revaluation: BudgetCalculationResult
  }
  hash?: string
  isPublic: boolean
}

export interface StateBudget {
  assiette_csg_abattue: number
  csg_deductible_retraite: number
  csg_deductible_salaire: number
  csg_imposable_retraite: number
  csg_imposable_salaire: number
  irpp_economique: number
  rfr_par_part: number
}
