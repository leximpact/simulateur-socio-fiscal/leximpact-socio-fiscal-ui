import variableSummaryByNameUnknown from "@leximpact/socio-fiscal-openfisca-json/variables_summaries.json"
import type {
  Formula,
  Reference,
  Variable,
  VariableByName,
} from "@openfisca/json-model"

import type { CalculationName } from "$lib/calculations.svelte"
import { reformChangesByName } from "$lib/reforms"

export type BudgetVariableOrganisme =
  | "agirc_arrco"
  | "etat"
  | "securite_sociale"
  | "unedic"

export type BudgetVariableType = "allegement" | "prelevement" | "prestation"

export interface BudgetVariable {
  allegementBaseVariableName?: string
  // Variables groupées par l'impact budgétaire commun qu'elles partagent (celle du nom du groupe) :
  // l'impact budgétaire des variables "filles" d'un groupe correspond à celui de la variable "parent" de ce groupe
  duplicate?: Set<string>
  label?: string
  labels: {
    default: string
    of: string
    ofThe: string
    the: string
    theAlt?: string
  }
  name?: string
  organisme: BudgetVariableOrganisme
  outputVariables: string[]
  quantileBaseVariable: string[]
  quantileCompareVariables: string[]
  type: BudgetVariableType
}

export interface InstantFormulaAndReferences {
  formula?: Formula
  instant: string
  references: Reference[]
}

export type ValuesByCalculationNameByVariableName = {
  [variableName: string]: VariableValuesByCalculationName
}

export type VariableValueByCalculationName = Partial<{
  [calculationName in CalculationName]: VariableValue
}>

export type VariableValuesByCalculationName = Partial<{
  [calculationName in CalculationName]: VariableValues
}>

export type VariableValue = boolean | number | string

export type VariableValues = boolean[] | number[] | string[]

export const budgetEditableParametersNameByVariableName: {
  [variableName: string]: Set<string>
} = {
  af: new Set([
    // Allocation familiale de base
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cm.nb_enfants_min_pour_allocation",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cm.age1",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cm.age2",
    "prestations_sociales.prestations_familiales.bmaf.bmaf",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cm.taux.enf2",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cm.taux.enf3",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cm.taux.nb_enf2",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cm.taux.nb_enf3",
    // Majoration pour âge
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_maj.maj_age_deux_enfants.age1",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_maj.maj_age_deux_enfants.age2",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_maj.maj_age_deux_enfants.taux1",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_maj.maj_age_deux_enfants.taux2",
    // Allocation forfaitaire
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_maj.majoration_enfants.allocation_forfaitaire.age_minimum",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_maj.majoration_enfants.allocation_forfaitaire.taux",
    // Complément dégressif base
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cond_ress.majoration_plafond_par_enfant_supplementaire",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cond_ress.plafond_tranche_2_base",
    // Modulation
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cm.modulation.taux_tranche_1",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cm.modulation.taux_tranche_2",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cm.modulation.taux_tranche_3",
    // Plafonds de ressources
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cond_ress.plafond_tranche_1",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cond_ress.plafond_tranche_2",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cond_ress.plafond_tranche_1_base",
    // DOM
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_maj_dom.allocations_familiales_un_enfant.yaml",
  ]),
  af_base: new Set([
    // Allocation familiale de base
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cm.nb_enfants_min_pour_allocation",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cm.age1",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cm.age2",
    "prestations_sociales.prestations_familiales.bmaf.bmaf",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cm.taux.enf2",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cm.taux.enf3",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cm.taux.nb_enf2",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cm.taux.nb_enf3",
    // Modulation
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cm.modulation.taux_tranche_1",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cm.modulation.taux_tranche_2",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cm.modulation.taux_tranche_3",
    // Plafonds de ressources
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cond_ress.majoration_plafond_par_enfant_supplementaire",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cond_ress.plafond_tranche_1",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cond_ress.plafond_tranche_2",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cond_ress.plafond_tranche_1_base",    
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cond_ress.plafond_tranche_2_base",
    // DOM
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_maj_dom.allocations_familiales_un_enfant.yaml",
  ]),
  af_majoration: new Set([
    // Majoration pour âge
    "prestations_sociales.prestations_familiales.bmaf.bmaf",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_maj.maj_age_deux_enfants.age1",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_maj.maj_age_deux_enfants.age2",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_maj.maj_age_deux_enfants.taux1",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_maj.maj_age_deux_enfants.taux2",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_maj_dom.majoration_premier_enfant.taux_tranche_1",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_maj_dom.majoration_premier_enfant.taux_tranche_2",
  ]),
  af_allocation_forfaitaire: new Set([
    // Allocation familiale de base
    "prestations_sociales.prestations_familiales.bmaf.bmaf",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cm.nb_enfants_min_pour_allocation",
    // Allocation forfaitaire
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_maj.majoration_enfants.allocation_forfaitaire.age_minimum",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_maj.majoration_enfants.allocation_forfaitaire.taux",
    // Modulation
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cm.modulation.taux_tranche_1",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cm.modulation.taux_tranche_2",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cm.modulation.taux_tranche_3",
  ]),
  af_allocation_forfaitaire_complement_degressif: new Set([
    // Complément dégressif
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cond_ress.plafond_tranche_2_base",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cond_ress.majoration_plafond_par_enfant_supplementaire",
  ]),
  af_complement_degressif: new Set([
    // Complément dégressif
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cond_ress.majoration_plafond_par_enfant_supplementaire",
    "prestations_sociales.prestations_familiales.prestations_generales.af.af_cond_ress.plafond_tranche_2_base",
  ]),
  agirc_arrco_employeur: new Set([
    "prelevements_sociaux.regimes_complementaires_retraite_secteur_prive.agirc_arrco.employeur.agirc_arrco",
  ]),
  agirc_arrco_salarie: new Set([
    "prelevements_sociaux.regimes_complementaires_retraite_secteur_prive.agirc_arrco.salarie.agirc_arrco",
  ]),
  allegement_general: new Set([
    "prelevements_sociaux.reductions_cotisations_sociales.allegement_general.ensemble_des_entreprises.plafond",
    "prelevements_sociaux.reductions_cotisations_sociales.allegement_general.ensemble_des_entreprises.entreprises_de_50_salaries_et_plus",
    "prelevements_sociaux.reductions_cotisations_sociales.allegement_general.ensemble_des_entreprises.entreprises_de_moins_de_50_salaries",
  ]),
  contribution_equilibre_general_employeur: new Set([
    "prelevements_sociaux.regimes_complementaires_retraite_secteur_prive.ceg.employeur.ceg",
  ]),
  contribution_equilibre_general_salarie: new Set([
    "prelevements_sociaux.regimes_complementaires_retraite_secteur_prive.ceg.salarie.ceg",
  ]),
  contribution_solidarite_autonomie: new Set([
    "prelevements_sociaux.cotisations_securite_sociale_regime_general.csa.employeur.csa",
  ]),
  cotisations_employeur_assurance_chomage: new Set([]),
  chomage_employeur: new Set([
    "prelevements_sociaux.cotisations_regime_assurance_chomage.chomage.employeur.chomage",
  ]),
  ags: new Set([
    "prelevements_sociaux.cotisations_regime_assurance_chomage.ags.employeur.ags",
  ]),
  csg_deductible_retraite: new Set([
    "prelevements_sociaux.contributions_sociales.csg.remplacement.pensions_retraite_invalidite.deductible.taux_median",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.pensions_retraite_invalidite.deductible.taux_plein",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.pensions_retraite_invalidite.deductible.taux_reduit",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.pensions_retraite_invalidite.imposable.taux_median",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.pensions_retraite_invalidite.imposable.taux_plein",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.pensions_retraite_invalidite.imposable.taux_reduit",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.seuils.seuil_ir",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.seuils.seuil_rfr1.demi_part_suppl_rfr1",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.seuils.seuil_rfr1.seuil_rfr1",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.seuils.seuil_rfr2.demi_part_suppl_rfr2",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.seuils.seuil_rfr2.seuil_rfr2",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.seuils.seuil_rfr3.demi_part_suppl_rfr3",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.seuils.seuil_rfr3.seuil_rfr3",
    "prelevements_sociaux.pss.plafond_securite_sociale_mensuel",
  ]),
  csg_deductible_salaire: new Set([
    "prelevements_sociaux.contributions_sociales.csg.activite.deductible.abattement",
    "prelevements_sociaux.contributions_sociales.csg.activite.deductible.taux",
    "prelevements_sociaux.contributions_sociales.csg.activite.imposable.abattement",
    "prelevements_sociaux.contributions_sociales.csg.activite.imposable.taux",
    "prelevements_sociaux.pss.plafond_securite_sociale_mensuel",
  ]),
  csg_imposable_retraite: new Set([
    "prelevements_sociaux.contributions_sociales.csg.remplacement.pensions_retraite_invalidite.deductible.taux_median",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.pensions_retraite_invalidite.deductible.taux_plein",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.pensions_retraite_invalidite.deductible.taux_reduit",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.pensions_retraite_invalidite.imposable.taux_median",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.pensions_retraite_invalidite.imposable.taux_plein",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.pensions_retraite_invalidite.imposable.taux_reduit",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.seuils.seuil_ir",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.seuils.seuil_rfr1.demi_part_suppl_rfr1",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.seuils.seuil_rfr1.seuil_rfr1",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.seuils.seuil_rfr2.demi_part_suppl_rfr2",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.seuils.seuil_rfr2.seuil_rfr2",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.seuils.seuil_rfr3.demi_part_suppl_rfr3",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.seuils.seuil_rfr3.seuil_rfr3",
    "prelevements_sociaux.pss.plafond_securite_sociale_mensuel",
  ]),
  csg_imposable_salaire: new Set([
    "prelevements_sociaux.contributions_sociales.csg.activite.deductible.abattement",
    "prelevements_sociaux.contributions_sociales.csg.activite.deductible.taux",
    "prelevements_sociaux.contributions_sociales.csg.activite.imposable.abattement",
    "prelevements_sociaux.contributions_sociales.csg.activite.imposable.taux",
    "prelevements_sociaux.pss.plafond_securite_sociale_mensuel",
  ]),
  csg_retraite: new Set([
    "prelevements_sociaux.contributions_sociales.csg.remplacement.pensions_retraite_invalidite.deductible.taux_median",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.pensions_retraite_invalidite.deductible.taux_plein",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.pensions_retraite_invalidite.deductible.taux_reduit",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.pensions_retraite_invalidite.imposable.taux_median",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.pensions_retraite_invalidite.imposable.taux_plein",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.pensions_retraite_invalidite.imposable.taux_reduit",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.seuils.seuil_ir",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.seuils.seuil_rfr1.demi_part_suppl_rfr1",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.seuils.seuil_rfr1.seuil_rfr1",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.seuils.seuil_rfr2.demi_part_suppl_rfr2",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.seuils.seuil_rfr2.seuil_rfr2",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.seuils.seuil_rfr3.demi_part_suppl_rfr3",
    "prelevements_sociaux.contributions_sociales.csg.remplacement.seuils.seuil_rfr3.seuil_rfr3",
    "prelevements_sociaux.pss.plafond_securite_sociale_mensuel",
  ]),
  csg_salaire: new Set([
    "prelevements_sociaux.contributions_sociales.csg.activite.deductible.abattement",
    "prelevements_sociaux.contributions_sociales.csg.activite.deductible.taux",
    "prelevements_sociaux.contributions_sociales.csg.activite.imposable.abattement",
    "prelevements_sociaux.contributions_sociales.csg.activite.imposable.taux",
    "prelevements_sociaux.pss.plafond_securite_sociale_mensuel",
  ]),
  famille_net_allegement: new Set([]),
  famille: new Set([
    "prelevements_sociaux.cotisations_securite_sociale_regime_general.famille.employeur.famille",
  ]),
  allegement_cotisation_allocations_familiales: new Set([
    "prelevements_sociaux.reductions_cotisations_sociales.allegement_cotisation_allocations_familiales.reduction",
    "prelevements_sociaux.reductions_cotisations_sociales.allegement_cotisation_allocations_familiales.plafond_smic_2023_12_31",
    "prelevements_sociaux.reductions_cotisations_sociales.allegement_cotisation_allocations_familiales.plafond_smic_courant",
  ]),
  fnal: new Set([
    "prelevements_sociaux.autres_taxes_participations_assises_salaires.fnal.contribution_moins_de_50_salaries",
    "prelevements_sociaux.autres_taxes_participations_assises_salaires.fnal.contribution_plus_de_50_salaries",
  ]),
  irpp_economique: new Set([
    // Barème de l'impôt sur le revenu
    "impot_revenu.bareme_ir_depuis_1945.bareme",
    // Quotient familial - Règles générales
    "impot_revenu.calcul_impot_revenu.plaf_qf.quotient_familial.conj",
    "impot_revenu.calcul_impot_revenu.plaf_qf.quotient_familial.enf1",
    "impot_revenu.calcul_impot_revenu.plaf_qf.quotient_familial.enf2",
    "impot_revenu.calcul_impot_revenu.plaf_qf.quotient_familial.enf3_et_sup",
    // "impot_revenu.calcul_impot_revenu.plaf_qf.quotient_familial.inv1",
    // "impot_revenu.calcul_impot_revenu.plaf_qf.quotient_familial.inv2",
    // "impot_revenu.calcul_impot_revenu.plaf_qf.quotient_familial.not31b",
    // "impot_revenu.calcul_impot_revenu.plaf_qf.quotient_familial.not32",
    // "impot_revenu.calcul_impot_revenu.plaf_qf.quotient_familial.not41",
    // "impot_revenu.calcul_impot_revenu.plaf_qf.quotient_familial.not42",
    // "impot_revenu.calcul_impot_revenu.plaf_qf.quotient_familial.not6",
    // "impot_revenu.calcul_impot_revenu.plaf_qf.quotient_familial.isol",
    // "impot_revenu.calcul_impot_revenu.plaf_qf.quotient_familial.cdcd",
    // Quotient familial - Plafonds
    "impot_revenu.calcul_impot_revenu.plaf_qf.plafond_avantages_procures_par_demi_part.general",
    // Décote
    "impot_revenu.calcul_impot_revenu.plaf_qf.decote.seuil_celib",
    "impot_revenu.calcul_impot_revenu.plaf_qf.decote.seuil_couple",
    "impot_revenu.calcul_impot_revenu.plaf_qf.decote.taux",
  ]),
  mmid_employeur_net_allegement: new Set([]),
  mmid_employeur: new Set([
    "prelevements_sociaux.cotisations_securite_sociale_regime_general.mmid.employeur.maladie",
  ]),
  allegement_cotisation_maladie: new Set([
    "prelevements_sociaux.reductions_cotisations_sociales.alleg_gen.mmid.plafond_smic_2023_12_31",
    "prelevements_sociaux.reductions_cotisations_sociales.alleg_gen.mmid.plafond_smic_courant",
    "prelevements_sociaux.reductions_cotisations_sociales.alleg_gen.mmid.taux",
  ]),
  vieillesse_deplafonnee_employeur: new Set([
    "prelevements_sociaux.cotisations_securite_sociale_regime_general.cnav.employeur.vieillesse_deplafonnee",
  ]),
  vieillesse_deplafonnee_salarie: new Set([
    "prelevements_sociaux.cotisations_securite_sociale_regime_general.cnav.salarie.vieillesse_deplafonnee",
  ]),
  vieillesse_plafonnee_employeur: new Set([
    "prelevements_sociaux.cotisations_securite_sociale_regime_general.cnav.employeur.vieillesse_plafonnee",
  ]),
  vieillesse_plafonnee_salarie: new Set([
    "prelevements_sociaux.cotisations_securite_sociale_regime_general.cnav.salarie.vieillesse_plafonnee",
  ]),
}
export const budgetEditableParametersName = new Set<string>()
for (const parametersName of Object.values(
  budgetEditableParametersNameByVariableName,
)) {
  for (const parameterName of parametersName) {
    budgetEditableParametersName.add(parameterName)
  }
}

// Variables dont l'impact budgétaire ne peut pas être calculé mais qui ont un layout custom
export const budgetVariablesNameRelated = new Set([""])

export const budgetVariablesConfig: { [variableName: string]: BudgetVariable } =
  {
    af: {
      duplicate: new Set([
        "af_base",
        "af_majoration",
        "af_allocation_forfaitaire",
        "af_complement_degressif",
        "af_allocation_forfaitaire_complement_degressif",
      ]),
      labels: {
        default: "AF",
        of: "d'allocations familiales",
        ofThe: "de l'allocation familiale",
        the: "les allocations familiales",
      },
      organisme: "securite_sociale",
      outputVariables: [
        "revenus_menage_par_uc",
        "af",
        "af_base",
        "af_majoration",
        "af_allocation_forfaitaire",
        "af_complement_degressif",
        "af_allocation_forfaitaire_complement_degressif",
      ],
      quantileBaseVariable: ["revenus_menage_par_uc"],
      quantileCompareVariables: ["af", "revenus_menage"],
      type: "prestation",
    },
    agirc_arrco_employeur: {
      labels: {
        default: "Cotis. employeur AGIRC-ARRCO",
        of: "de cotisation employeur de retraite complémentaire",
        ofThe: "de la cotisation employeur de retraite complémentaire",
        the: "les cotisations employeur de retraite complémentaire",
      },
      organisme: "agirc_arrco",
      outputVariables: ["rfr_par_part", "agirc_arrco_employeur"],
      quantileBaseVariable: ["rfr_par_part"],
      quantileCompareVariables: ["agirc_arrco_employeur", "rfr"],
      type: "prelevement",
    },
    agirc_arrco_salarie: {
      labels: {
        default: "Cotis. salariale AGIRC-ARRCO",
        of: "de cotisations salariales de retraite complémentaire",
        ofThe: "de la cotisation salariale de retraite complémentaire",
        the: "les cotisations salariales de retraite complémentaire",
      },
      organisme: "agirc_arrco",
      outputVariables: ["rfr_par_part", "agirc_arrco_salarie"],
      quantileBaseVariable: ["rfr_par_part"],
      quantileCompareVariables: ["agirc_arrco_salarie", "rfr"],
      type: "prelevement",
    },
    ags: {
      labels: {
        default: "AGS",
        of: "de contribution au régime de garantie des salaires",
        ofThe: "de la contribution au régime de garantie des salaires",
        the: "les contributions au régime de garantie des salaires",
      },
      organisme: "unedic",
      outputVariables: ["rfr_par_part", "ags"],
      quantileBaseVariable: ["rfr_par_part"],
      quantileCompareVariables: ["ags", "rfr"],
      type: "prelevement",
    },
    allegement_cotisation_allocations_familiales: {
      allegementBaseVariableName: "famille",
      labels: {
        default: "Allègement cotis. AF",
        of: "de cotisations d'allocations familiales",
        ofThe: "de l'allègement de la cotisation d'allocations familiales",
        the: "les cotisations d'allocations familiales",
      },
      organisme: "securite_sociale",
      outputVariables: [
        "rfr_par_part",
        "allegement_cotisation_allocations_familiales",
        "famille",
      ],
      quantileBaseVariable: ["rfr_par_part"],
      quantileCompareVariables: [
        "allegement_cotisation_allocations_familiales",
        "famille",
        "rfr",
      ],
      type: "allegement",
    },
    allegement_cotisation_maladie: {
      allegementBaseVariableName: "mmid_employeur",
      labels: {
        default: "Allègement cotis. MMID",
        of: "de cotisations maladie-maternité-invalidité-décès",
        ofThe: "de l'allègement de la cotisation MMID",
        the: "les cotisations maladie-maternité-invalidité-décès",
      },
      organisme: "securite_sociale",
      outputVariables: [
        "rfr_par_part",
        "allegement_cotisation_maladie",
        "mmid_employeur",
      ],
      quantileBaseVariable: ["rfr_par_part"],
      quantileCompareVariables: [
        "allegement_cotisation_maladie",
        "mmid_employeur",
        "rfr",
      ],
      type: "allegement",
    },
    allegement_general: {
      allegementBaseVariableName: "cotisations_allegement_general",
      labels: {
        default: "Réduction générale",
        of: "de cotisations employeur concernées par la réduction générale",
        ofThe: "de la réduction générale des cotisations employeur",
        the: "les cotisations employeur concernées par la réduction générale",
      },
      organisme: "securite_sociale",
      outputVariables: [
        "rfr_par_part",
        "allegement_general",
        "cotisations_allegement_general",
      ],
      quantileBaseVariable: ["rfr_par_part"],
      quantileCompareVariables: [
        "allegement_general",
        "rfr",
        "cotisations_allegement_general",
      ],
      type: "allegement",
    },
    chomage_employeur: {
      labels: {
        default: "Cotis. chômage",
        of: "de cotisation assurance chômage",
        ofThe: "de la cotisation d'assurance chômage",
        the: "les cotisations d'assurance chômage",
      },
      organisme: "unedic",
      outputVariables: ["rfr_par_part", "chomage_employeur"],
      quantileBaseVariable: ["rfr_par_part"],
      quantileCompareVariables: ["chomage_employeur", "rfr"],
      type: "prelevement",
    },
    contribution_equilibre_general_employeur: {
      labels: {
        default: "CEG employeur",
        of: "de contribution employeur d'équilibre général",
        ofThe: "de la contribution employeur d'équilibre général",
        the: "les contributions employeur d'équilibre général",
      },
      organisme: "agirc_arrco",
      outputVariables: [
        "rfr_par_part",
        "contribution_equilibre_general_employeur",
      ],
      quantileBaseVariable: ["rfr_par_part"],
      quantileCompareVariables: [
        "contribution_equilibre_general_employeur",
        "rfr",
      ],
      type: "prelevement",
    },
    contribution_equilibre_general_salarie: {
      labels: {
        default: "CEG salariale",
        of: "de contribution salariale d'équilibre général",
        ofThe: "de la contribution salariale d'équilibre général",
        the: "les contributions salariales d'équilibre général",
      },
      organisme: "agirc_arrco",
      outputVariables: [
        "rfr_par_part",
        "contribution_equilibre_general_salarie",
      ],
      quantileBaseVariable: ["rfr_par_part"],
      quantileCompareVariables: [
        "contribution_equilibre_general_salarie",
        "rfr",
      ],
      type: "prelevement",
    },
    contribution_solidarite_autonomie: {
      labels: {
        default: "CSA",
        of: "de contribution solidarité autonomie",
        ofThe: "de la contribution solidarité autonomie",
        the: "les contributions solidarité autonomie",
      },
      organisme: "securite_sociale",
      outputVariables: ["rfr_par_part", "contribution_solidarite_autonomie"],
      quantileBaseVariable: ["rfr_par_part"],
      quantileCompareVariables: ["contribution_solidarite_autonomie", "rfr"],
      type: "prelevement",
    },
    cotisations_allegement_general: {
      labels: {
        default: "Cotisations concernées par la réduction générale",
        of: "de cotisations concernées par la réduction générale",
        ofThe: "des cotisations concernées par la réduction générale",
        the: "les cotisations concernées par la réduction générale",
      },
      organisme: "securite_sociale",
      outputVariables: [
        "rfr_par_part",
        "allegement_general",
        "cotisations_allegement_general",
      ],
      quantileBaseVariable: ["rfr_par_part"],
      quantileCompareVariables: [
        "allegement_general",
        "rfr",
        "cotisations_allegement_general",
      ],
      type: "prelevement",
    },
    cotisations_employeur_assurance_chomage: {
      labels: {
        default: "Cotis. chômage totale",
        of: "de cotisations d'assurance chômage",
        ofThe: "de la cotisation d'assurance chômage",
        the: "les cotisations d'assurance chômage",
      },
      organisme: "unedic",
      outputVariables: [
        "rfr_par_part",
        "cotisations_employeur_assurance_chomage",
      ],
      quantileBaseVariable: ["rfr_par_part"],
      quantileCompareVariables: [
        "cotisations_employeur_assurance_chomage",
        "rfr",
      ],
      type: "prelevement",
    },
    csg_retraite: {
      labels: {
        default: "CSG",
        of: "de CSG retraites",
        ofThe: "de CSG sur les retraites",
        the: "la CSG",
        theAlt: "la CSG retraites",
      },
      organisme: "securite_sociale",
      outputVariables: [
        "rfr_par_part", // "assiette_csg_abattue",
        "csg_retraite",
      ],
      quantileBaseVariable: ["rfr_par_part"], // ["assiette_csg_abattue"],
      quantileCompareVariables: ["csg_retraite", "rfr"],
      type: "prelevement",
    },
    csg_deductible_retraite: {
      labels: {
        default: "CSG déductible retraites",
        of: "de CSG déductible retraites",
        ofThe: "de CSG déductible sur les retraites",
        the: "la CSG déductible retraites",
        theAlt: "la CSG déductible retraites",
      },
      organisme: "securite_sociale",
      outputVariables: [
        "rfr_par_part", // "assiette_csg_abattue",
        "csg_deductible_retraite",
      ],
      quantileBaseVariable: ["rfr_par_part"], // ["assiette_csg_abattue"],
      quantileCompareVariables: ["csg_deductible_retraite", "rfr"],
      type: "prelevement",
    },
    csg_imposable_retraite: {
      labels: {
        default: "CSG imposable retraites",
        of: "de CSG imposable retraites",
        ofThe: "de CSG imposable sur les retraites",
        the: "la CSG imposable retraites",
        theAlt: "la CSG imposable retraites",
      },
      organisme: "securite_sociale",
      outputVariables: [
        "rfr_par_part", // "assiette_csg_abattue",
        "csg_imposable_retraite",
      ],
      quantileBaseVariable: ["rfr_par_part"], // ["assiette_csg_abattue"],
      quantileCompareVariables: ["csg_imposable_retraite", "rfr"],
      type: "prelevement",
    },
    csg_salaire: {
      labels: {
        default: "CSG",
        of: "de CSG salaires",
        ofThe: "de CSG sur les salaires",
        the: "la CSG",
        theAlt: "la CSG salaires",
      },
      organisme: "securite_sociale",
      outputVariables: [
        "rfr_par_part", // "assiette_csg_abattue",
        "csg_salaire",
      ],
      quantileBaseVariable: ["rfr_par_part"], // ["assiette_csg_abattue"],
      quantileCompareVariables: ["csg_salaire", "rfr"],
      type: "prelevement",
    },
    csg_imposable_salaire: {
      labels: {
        default: "CSG imposable salaires",
        of: "de CSG imposable salaires",
        ofThe: "de CSG imposable sur les salaires",
        the: "la CSG imposable salaires",
        theAlt: "la CSG imposable salaires",
      },
      organisme: "securite_sociale",
      outputVariables: [
        "rfr_par_part", // "assiette_csg_abattue",
        "csg_imposable_salaire",
      ],
      quantileBaseVariable: ["rfr_par_part"], // ["assiette_csg_abattue"],
      quantileCompareVariables: ["csg_imposable_salaire", "rfr"],
      type: "prelevement",
    },
    csg_deductible_salaire: {
      labels: {
        default: "CSG déductible salaires",
        of: "de CSG déductible salaires",
        ofThe: "de CSG déductible sur les salaires",
        the: "la CSG déductible salaires",
        theAlt: "la CSG déductible salaires",
      },
      organisme: "securite_sociale",
      outputVariables: [
        "rfr_par_part", // "assiette_csg_abattue",
        "csg_deductible_salaire",
      ],
      quantileBaseVariable: ["rfr_par_part"], // ["assiette_csg_abattue"],
      quantileCompareVariables: ["csg_deductible_salaire", "rfr"],
      type: "prelevement",
    },
    famille: {
      labels: {
        default: "Cotisations brutes AF",
        of: "de cotisation brute d'allocations familiales",
        ofThe: "de la cotisation brute d'allocations familiales",
        the: "les cotisations brutes d'allocations familiales",
      },
      organisme: "securite_sociale",
      outputVariables: ["rfr_par_part", "famille"],
      quantileBaseVariable: ["rfr_par_part"],
      quantileCompareVariables: ["famille", "rfr"],
      type: "prelevement",
    },
    famille_net_allegement: {
      labels: {
        default: "Cotis. AF après allègement",
        of: "de cotisation d'allocations familiales après allègement",
        ofThe: "de la cotisation d'allocations familiales après allègement",
        the: "les cotisations d'allocations familiales après allègement",
      },
      organisme: "securite_sociale",
      outputVariables: ["rfr_par_part", "famille_net_allegement"],
      quantileBaseVariable: ["rfr_par_part"],
      quantileCompareVariables: ["famille_net_allegement", "rfr"],
      type: "prelevement",
    },
    fnal: {
      labels: {
        default: "FNAL",
        of: "de contribution d'aide au logement",
        ofThe: "de la contribution d'aide au logement",
        the: "les contributions d'aide au logement",
      },
      organisme: "securite_sociale",
      outputVariables: ["rfr_par_part", "fnal"],
      quantileBaseVariable: ["rfr_par_part"],
      quantileCompareVariables: ["fnal", "rfr"],
      type: "prelevement",
    },
    irpp_economique: {
      labels: {
        default: "IR",
        of: "d'IR",
        ofThe: "de l'IR",
        the: "l'IR",
        theAlt: "l'impôt sur le revenu",
      },
      organisme: "etat",
      outputVariables: ["rfr_par_part", "irpp_economique"],
      quantileBaseVariable: ["rfr_par_part"],
      quantileCompareVariables: ["irpp_economique", "rfr"],
      type: "prelevement",
    },
    mmid_employeur: {
      labels: {
        default: "Cotis. MMID brute",
        of: "de cotisation MMID brute",
        ofThe: "de la cotisation MMID brute",
        the: "les cotisations MMID brute",
      },
      organisme: "securite_sociale",
      outputVariables: ["rfr_par_part", "mmid_employeur"],
      quantileBaseVariable: ["rfr_par_part"],
      quantileCompareVariables: ["mmid_employeur", "rfr"],
      type: "prelevement",
    },
    mmid_employeur_net_allegement: {
      labels: {
        default: "Cotis. MMID après allègement",
        of: "de cotisation MMID après allègement",
        ofThe: "de la cotisation MMID après allègement",
        the: "les cotisations MMID après allègement",
      },
      organisme: "securite_sociale",
      outputVariables: ["rfr_par_part", "mmid_employeur_net_allegement"],
      quantileBaseVariable: ["rfr_par_part"],
      quantileCompareVariables: ["mmid_employeur_net_allegement", "rfr"],
      type: "prelevement",
    },
    vieillesse_employeur: {
      labels: {
        default: "Cotis. employeur vieillesse",
        of: "de cotisation employeur d'assurance vieillesse",
        ofThe: "de la cotisation employeur d'assurance vieillesse",
        the: "les cotisations employeur d'assurance vieillesse",
      },
      organisme: "securite_sociale",
      outputVariables: ["rfr_par_part", "vieillesse_employeur"],
      quantileBaseVariable: ["rfr_par_part"],
      quantileCompareVariables: ["vieillesse_employeur", "rfr"],
      type: "prelevement",
    },
    vieillesse_plafonnee_employeur: {
      labels: {
        default: "Cotis. employeur vieillesse plafonndée",
        of: "de part plafonnée de cotis. employeur vieillesse",
        ofThe: "de la part plafonnée de cotis. employeur vieillesse",
        the: "les parts plafonnées de cotis. employeur vieillesse",
      },
      organisme: "securite_sociale",
      outputVariables: ["rfr_par_part", "vieillesse_plafonnee_employeur"],
      quantileBaseVariable: ["rfr_par_part"],
      quantileCompareVariables: ["vieillesse_plafonnee_employeur", "rfr"],
      type: "prelevement",
    },
    vieillesse_deplafonnee_employeur: {
      labels: {
        default: "Cotis. employeur vieillesse déplafonnée",
        of: "de part déplafonnée de cotis. employeur vieillesse",
        ofThe: "de la part déplafonnée de cotis. employeur vieillesse",
        the: "les parts déplafonnées de cotis. employeur vieillesse",
      },
      organisme: "securite_sociale",
      outputVariables: ["rfr_par_part", "vieillesse_deplafonnee_employeur"],
      quantileBaseVariable: ["rfr_par_part"],
      quantileCompareVariables: ["vieillesse_deplafonnee_employeur", "rfr"],
      type: "prelevement",
    },
    vieillesse_salarie: {
      labels: {
        default: "Cotis. salariale vieillesse",
        of: "de cotisations salariale d'assurance vieillesse",
        ofThe: "de la cotisation salariale d'assurance vieillesse",
        the: "les cotisations salariales d'assurance vieillesse",
      },
      organisme: "securite_sociale",
      outputVariables: ["rfr_par_part", "vieillesse_salarie"],
      quantileBaseVariable: ["rfr_par_part"],
      quantileCompareVariables: ["vieillesse_salarie", "rfr"],
      type: "prelevement",
    },
    vieillesse_deplafonnee_salarie: {
      labels: {
        default: "Cotis. salariale vieillesse déplafonnée",
        of: "de part déplafonnée de cotis. salariale vieillesse",
        ofThe: "de la part déplafonnée de cotis. salariale vieillesse",
        the: "les parts déplafonnées de cotis. salariale vieillesse",
      },
      organisme: "securite_sociale",
      outputVariables: ["rfr_par_part", "vieillesse_deplafonnee_salarie"],
      quantileBaseVariable: ["rfr_par_part"],
      quantileCompareVariables: ["vieillesse_deplafonnee_salarie", "rfr"],
      type: "prelevement",
    },
    vieillesse_plafonnee_salarie: {
      labels: {
        default: "Cotis. salariale vieillesse plafonnée",
        of: "de part plafonnée de cotis. salariale vieillesse",
        ofThe: "de la part plafonnée de cotis. salariale vieillesse",
        the: "les parts plafonnées de cotis. salariale vieillesse",
      },
      organisme: "securite_sociale",
      outputVariables: ["rfr_par_part", "vieillesse_plafonnee_salarie"],
      quantileBaseVariable: ["rfr_par_part"],
      quantileCompareVariables: ["vieillesse_plafonnee_salarie", "rfr"],
      type: "prelevement",
    },
  }

export const budgetVariableNameByVariableName: {
  [variableName: string]: string
} = {
  ...Object.fromEntries(
    Object.entries(budgetVariablesConfig).reduce(
      (arr: [string, string][], [name, config]) => {
        arr.push([name, name])
        for (const duplicate of config.duplicate ?? []) {
          arr.push([duplicate, name])
        }
        return arr
      },
      [],
    ),
  ),
  ...Object.fromEntries(
    [...budgetVariablesNameRelated.values()].map((variableName) => [
      variableName,
      variableName,
    ]),
  ),
}

// Variables dont l'impact budgétaire peut être calculé
// (pas vraiment, mais pour lesquelles on montre un impact budgétaire)
export const budgetVariablesName = new Set(
  Object.keys(budgetVariableNameByVariableName),
)

export const oilTypes = [
  "essence_sp95",
  "essence_sp95_e10",
  "essence_sp98",
  "essence_e85",
  "gazole_b7",
  "gazole_b10",
  "gpl_carburant",
]

/// Name of variables that must be calculated to be displayed in
/// test case summaries.
export const summaryCalculatedVariablesName = [
  "assiette_csg_revenus_capital",
  "assiette_csg_plus_values",
  "niveau_de_vie",
  "plus_values_base_large",
  "rente_viagere_titre_onereux_net",
  "rsa",
  "zone_apl",
].concat(
  oilTypes.reduce((names: string[], name: string) => {
    names.push(
      `depense_${name}_ttc`,
      `nombre_litres_${name}`,
      // `prix_${name}_hors_remise_ttc_sortie`,
      `prix_${name}_ttc`,
      `tva_sur_${name}`,
      `${name}_ticpe`,
    )
    return names
  }, []),
)

/// Autres variables à calculer
export const otherCalculatedVariablesName = [
  "niveau_de_vie",
  "unites_consommation",
  "taxes_tous_carburants",
]

export const variableSummaryByName =
  variableSummaryByNameUnknown as VariableByName

export const variableSummaryByNameByReformName: {
  [name: string]: VariableByName
} = Object.fromEntries(
  Object.entries(reformChangesByName).map(([reformName, reformChanges]) => [
    reformName,
    patchVariableSummaryByName(
      variableSummaryByName,
      reformChanges.variables_summaries,
    ),
  ]),
)

export function buildInstantFormulaAndReferencesArray(
  variable: Variable,
): InstantFormulaAndReferences[] {
  const instantFormulaAndReferencesByInstant: {
    [instant: string]: InstantFormulaAndReferences
  } = Object.fromEntries(
    Object.entries(variable.formulas ?? {}).map(([instant, formula]) => [
      instant,
      { formula, instant, references: [] },
    ]),
  )
  if (variable.reference !== undefined) {
    for (const [instant, references] of Object.entries(variable.reference)) {
      if (instantFormulaAndReferencesByInstant[instant] === undefined) {
        instantFormulaAndReferencesByInstant[instant] = {
          instant,
          references,
        }
      } else {
        instantFormulaAndReferencesByInstant[instant].references = references
      }
    }
  }
  return Object.entries(instantFormulaAndReferencesByInstant)
    .sort(([instant1], [instant2]) => instant2.localeCompare(instant1))
    .map(([, instantFormulaAndReferences]) => instantFormulaAndReferences)
}

export function getVariableParametersName(
  variable: Variable,
  date: string,
): string[] | undefined {
  if (variable.input || variable.formulas === undefined) {
    // Variable is an input variable => No parameter.
    return undefined
  }
  const dates = Object.keys(variable.formulas).reverse()
  let formula: Formula | undefined = undefined
  for (const bestDate of dates) {
    if (bestDate <= date) {
      formula = variable.formulas[bestDate]
      break
    }
  }
  if (formula == null) {
    // No candidate date less than or equal to date found.
    return undefined
  }
  return formula.parameters
}

export function* iterVariableInputVariables(
  variable: Variable | undefined,
  date: string,
  encounteredVariablesName: Set<string> = new Set(),
): Generator<Variable, void> {
  if (variable === undefined) {
    console.error(
      `iterVariableInputVariables(undefined, ...): Given variable is undefined`,
    )
    return
  }

  const name = variable.name
  if (encounteredVariablesName.has(name)) {
    return
  }
  encounteredVariablesName.add(name)

  if (variable.input) {
    // Variable has been customized to an input variable.
    yield variable
    return
  }
  const formulas = variable.formulas
  if (formulas === undefined) {
    // Variable is an input variable.
    yield variable
    return
  }
  const dates = Object.keys(formulas).reverse()
  let formula = undefined
  for (const bestDate of dates) {
    if (bestDate <= date) {
      formula = formulas[bestDate]
      break
    }
  }
  if (formula == null) {
    // No candidate date less than or equal to date found.
    return
  }
  const referredVariablesName = formula.variables
  if (referredVariablesName === undefined) {
    return
  }
  for (const referredVariableName of referredVariablesName) {
    const referredVariable = variableSummaryByName[referredVariableName]
    yield* iterVariableInputVariables(
      referredVariable,
      date,
      encounteredVariablesName,
    )
  }
}

export function* iterVariableParametersName(
  variable: Variable,
  date: string,
  reformName: string | undefined = undefined,
  encounteredParametersName: Set<string> = new Set(),
  encounteredVariablesName: Set<string> = new Set(),
): Generator<string, void> {
  const name = variable.name
  if (encounteredVariablesName.has(name)) {
    return
  }
  encounteredVariablesName.add(name)

  const formulas = variable.formulas
  if (formulas === undefined) {
    return
  }
  const dates = Object.keys(formulas).reverse()
  let formula = undefined
  for (const bestDate of dates) {
    if (bestDate <= date) {
      formula = formulas[bestDate]
      break
    }
  }
  if (formula == null) {
    // No candidate date less than or equal to date found.
    return
  }

  const referredVariablesName = formula.variables
  if (referredVariablesName !== undefined) {
    for (const referredVariableName of referredVariablesName) {
      const referredVariable = (
        reformName == undefined
          ? variableSummaryByName
          : variableSummaryByNameByReformName[reformName]
      )[referredVariableName]
      if (referredVariable === undefined) {
        console.warn(
          `iterVariableParametersName: Undefined variable ${referredVariableName}`,
        )
        continue
      }
      yield* iterVariableParametersName(
        referredVariable,
        date,
        reformName,
        encounteredParametersName,
        encounteredVariablesName,
      )
    }
  }

  const referredParametersName = formula.parameters
  if (referredParametersName !== undefined) {
    for (const referredParameterName of referredParametersName) {
      if (encounteredParametersName.has(referredParameterName)) {
        continue
      }
      encounteredParametersName.add(referredParameterName)
      yield referredParameterName
    }
  }
}

function patchVariableSummaryByName(
  variableSummaryByName: VariableByName,
  patch: { [name: string]: Variable | null },
): VariableByName {
  if (Object.keys(patch).length === 0) {
    return variableSummaryByName
  }
  const patchedVariableSummaryByName = { ...variableSummaryByName }
  for (const [name, variableSummaryPatch] of Object.entries(patch)) {
    if (variableSummaryPatch === null) {
      // This case should not occur, because a reform should always
      // have all the variables of the original tax-benefit system.
      delete patchedVariableSummaryByName[name]
    } else {
      patchedVariableSummaryByName[name] = variableSummaryPatch
    }
  }
  return patchedVariableSummaryByName
}
