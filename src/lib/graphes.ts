import { type CalculationName } from "$lib/calculations.svelte"
import type { VisibleRowForGraph } from "$lib/decompositions"

export interface VariableGraphRow extends VisibleRowForGraph {
  hide: boolean
  ignore: boolean
  summedValues: number[]
}

export interface VariableGraph {
  depth: number
  label: string
  name: string
  open: boolean
  parent: string | undefined
  rows: VariableGraphRow[]
  trunk: boolean
}

export interface VariableCustomization {
  stroke: {
    borderstyle?: string
    color: string
    dasharray?: string
    width?: string
  }
  pattern?: { background?: string; foreground?: string; size?: number }
  selected: "true" | "false" | "always"
}

export const defaultCustomization: {
  [waterfallName: string]: {
    [type: string]: {
      [calculationName in CalculationName]: VariableCustomization
    }
  }
} = {
  brut_to_disponible: {
    base: {
      law: {
        stroke: {
          color: "#565656",
          dasharray: "1 2",
          borderstyle: "dashed",
        },
        pattern: {
          background: "rgba(210, 223, 255, .6)",
          size: 40,
        },
        selected: "true",
      },
      bill: {
        stroke: {
          color: "#565656",
          dasharray: "1 2",
          borderstyle: "dashed",
        },
        pattern: {
          background: "rgba(210, 223, 255, .6)",
          size: 40,
        },
        selected: "always",
      },
      amendment: {
        stroke: {
          color: "#565656",
          dasharray: "1 2",
          borderstyle: "dashed",
        },
        pattern: {
          background: "rgba(210, 223, 255, .6)",
          size: 40,
        },
        selected: "always",
      },
    },
    prelevement: {
      law: {
        stroke: {
          color: "#565656",
        },
        pattern: {
          foreground:
            "data:image/svg+xml,%3Csvg width='40' height='40' viewBox='0 0 40 40' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%23bbbbbb' fill-opacity='1' fill-rule='evenodd'%3E%3Cpath d='M 40,30 30,40 H 40 Z M 40,10 10,40 H 20 L 40,20 Z M 30,0 0,30 V 40 L 40,0 Z M 10,0 0,10 V 20 L 20,0 Z'/%3E%3C/g%3E%3C/svg%3E",
          size: 40,
        },
        selected: "true",
      },
      bill: {
        stroke: {
          color: "#565656",
        },
        pattern: {
          foreground:
            "data:image/svg+xml,%3Csvg width='40' height='40' viewBox='0 0 40 40' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%23ff6b6b' fill-opacity='1' fill-rule='evenodd'%3E%3Cpath d='M 40,30 30,40 H 40 Z M 40,10 10,40 H 20 L 40,20 Z M 30,0 0,30 V 40 L 40,0 Z M 10,0 0,10 V 20 L 20,0 Z'/%3E%3C/g%3E%3C/svg%3E",
          size: 40,
        },
        selected: "true",
      },
      amendment: {
        stroke: {
          color: "#565656",
        },
        pattern: {
          foreground:
            "data:image/svg+xml,%3Csvg width='40' height='40' viewBox='0 0 40 40' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%23ded500' fill-opacity='1' fill-rule='evenodd'%3E%3Cpath d='M 40,30 30,40 H 40 Z M 40,10 10,40 H 20 L 40,20 Z M 30,0 0,30 V 40 L 40,0 Z M 10,0 0,10 V 20 L 20,0 Z'/%3E%3C/g%3E%3C/svg%3E",
          size: 40,
        },
        selected: "true",
      },
    },
    prestation: {
      law: {
        stroke: {
          color: "#565656",
        },
        pattern: {
          background: "rgba(155, 155, 155, .5)",
        },
        selected: "true",
      },
      bill: {
        stroke: {
          color: "#565656",
        },
        pattern: {
          background: "#ff6b6b",
        },
        selected: "true",
      },
      amendment: {
        stroke: {
          color: "#565656",
        },
        pattern: {
          background: "#ded500",
        },
        selected: "true",
      },
    },
  },
  brut_to_super_brut: {
    base: {
      law: {
        stroke: {
          color: "#565656",
          dasharray: "1 2",
          borderstyle: "dashed",
        },
        pattern: {
          background: "rgba(210, 223, 255, .6)",
          size: 40,
        },
        selected: "always",
      },
      bill: {
        stroke: {
          color: "#565656",
          dasharray: "1 2",
          borderstyle: "dashed",
        },
        pattern: {
          background: "rgba(210, 223, 255, .6)",
          size: 40,
        },
        selected: "always",
      },
      amendment: {
        stroke: {
          color: "#565656",
          dasharray: "1 2",
          borderstyle: "dashed",
        },
        pattern: {
          background: "rgba(210, 223, 255, .6)",
          size: 40,
        },
        selected: "always",
      },
    },
    cotisation: {
      law: {
        stroke: {
          color: "#565656",
        },
        pattern: {
          background: "rgba(59, 59, 59, .5)",
        },
        selected: "true",
      },
      bill: {
        stroke: {
          color: "#565656",
        },
        pattern: {
          background: "rgba(255, 107, 107)",
        },
        selected: "true",
      },
      amendment: {
        stroke: {
          color: "#565656",
        },
        pattern: {
          background: "rgba(222, 213, 0)",
        },
        selected: "true",
      },
    },
    allegement: {
      law: {
        stroke: {
          color: "#565656",
        },
        pattern: {
          background: "#fff",
          foreground:
            "data:image/svg+xml,%3Csvg width='40' height='40' viewBox='0 0 40 40' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%23b2b2b2' fill-opacity='1' fill-rule='evenodd'%3E%3Cpath d='M 40,30 30,40 H 40 Z M 40,10 10,40 H 20 L 40,20 Z M 30,0 0,30 V 40 L 40,0 Z M 10,0 0,10 V 20 L 20,0 Z'/%3E%3C/g%3E%3C/svg%3E",
          size: 20,
        },
        selected: "true",
      },
      bill: {
        stroke: {
          color: "#565656",
        },
        pattern: {
          background: "#fff",
          foreground:
            "data:image/svg+xml,%3Csvg width='40' height='40' viewBox='0 0 40 40' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%23ff6b6b' fill-opacity='1' fill-rule='evenodd'%3E%3Cpath d='M 40,30 30,40 H 40 Z M 40,10 10,40 H 20 L 40,20 Z M 30,0 0,30 V 40 L 40,0 Z M 10,0 0,10 V 20 L 20,0 Z'/%3E%3C/g%3E%3C/svg%3E",
          size: 20,
        },
        selected: "true",
      },
      amendment: {
        stroke: {
          color: "#565656",
        },
        pattern: {
          background: "#fff",
          foreground:
            "data:image/svg+xml,%3Csvg width='40' height='40' viewBox='0 0 40 40' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%23ded500' fill-opacity='1' fill-rule='evenodd'%3E%3Cpath d='M 40,30 30,40 H 40 Z M 40,10 10,40 H 20 L 40,20 Z M 30,0 0,30 V 40 L 40,0 Z M 10,0 0,10 V 20 L 20,0 Z'/%3E%3C/g%3E%3C/svg%3E",
          size: 20,
        },
        selected: "true",
      },
    },
  },
}

export const variableCustomizationsBase = {
  remuneration_brute: {
    stroke: {
      width: "0",
    },
    pattern: {
      background: "rgba(210, 223, 255, .6)",
      size: 40,
    },
    selected: "always",
  },
  chomage_brut: {
    stroke: {
      width: "0",
    },
    pattern: {
      background: "rgba(210, 223, 255, .6)",
      size: 40,
    },
    selected: "always",
  },
  retraite_brute: {
    stroke: {
      width: "0",
    },
    pattern: {
      background: "rgba(210, 223, 255, .6)",
      size: 40,
    },
    selected: "always",
  },
  revenu_disponible: {
    stroke: {
      color: "#000000",
      width: "4",
    },
    selected: "true",
  },
  revenu_disponible_bill: {
    stroke: {
      color: "#ff6b6b",
      width: "4",
    },
    selected: "true",
  },
  revenu_disponible_amendment: {
    stroke: {
      color: "#9d970b",
      width: "4",
    },
    selected: "true",
  },
  salaire_super_brut: {
    stroke: {
      color: "#000000",
      width: "4",
    },
    selected: "true",
  },
  salaire_super_brut_bill: {
    stroke: {
      color: "#ff6b6b",
      width: "4",
    },
    selected: "true",
  },
  salaire_super_brut_amendment: {
    stroke: {
      color: "#9d970b",
      width: "4",
    },
    selected: "true",
  },
}

export function getCustomizations(
  groupName: string,
  calculationName: CalculationName,
): { background?: string; foreground?: string; size?: number }[] {
  switch (groupName) {
    case "prelevements":
      return [
        {
          foreground: `data:image/svg+xml,%3Csvg width='40' height='40' viewBox='0 0 40 40' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='${calculationName === "amendment" ? "%23ded500" : calculationName === "bill" ? "%23ff6b6b" : "%23bbbbbb"}' fill-opacity='1' fill-rule='evenodd'%3E%3Cpath d='M0 40L40 0H20L0 20M40 40V20L20 40'/%3E%3C/g%3E%3C/svg%3E`,
          size: 40,
        },
        {
          background:
            calculationName === "amendment"
              ? "#ded50023"
              : calculationName === "bill"
                ? "#ff6b6b23"
                : "rgba(0, 0, 0, 0.3)",
          foreground:
            "data:image/svg+xml,%3Csvg width='6' height='6' viewBox='0 0 6 6' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%23ffffff' fill-opacity='1' fill-rule='evenodd'%3E%3Cpath d='M5 0h1L0 6V5zM6 5v1H5z'/%3E%3C/g%3E%3C/svg%3E",
          size: 6,
        },
        {
          background:
            calculationName === "amendment"
              ? "#ded50023"
              : calculationName === "bill"
                ? "#ff6b6b23"
                : "rgba(0, 0, 0, 0.1)",
          foreground:
            "data:image/svg+xml,%3Csvg width='6' height='6' viewBox='0 0 6 6' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%23565656' fill-opacity='1' fill-rule='evenodd'%3E%3Cpath d='M5 0h1L0 6V5zM6 5v1H5z'/%3E%3C/g%3E%3C/svg%3E",
          size: 6,
        },
        {
          foreground: `data:image/svg+xml,%3Csvg width='40' height='40' viewBox='0 0 40 40' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='${calculationName === "amendment" ? "%23ded500" : calculationName === "bill" ? "%23ff6b6b" : "%23bbbbbb"}' fill-opacity='1' fill-rule='evenodd'%3E%3Cpath d='M 40,30 30,40 H 40 Z M 40,10 10,40 H 20 L 40,20 Z M 30,0 0,30 V 40 L 40,0 Z M 10,0 0,10 V 20 L 20,0 Z'/%3E%3C/g%3E%3C/svg%3E`,
          size: 20,
        },
      ]
    case "prestations":
      return [
        {
          background:
            calculationName === "amendment"
              ? "#ded500"
              : calculationName === "bill"
                ? "#ff6b6b"
                : "rgba(155, 155, 155, 0.5)",
        },
        {
          background:
            calculationName === "amendment"
              ? "#ded500"
              : calculationName === "bill"
                ? "#ff6b6b"
                : "rgba(155, 155, 155, 0.5)",
          foreground:
            "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='4' height='4' viewBox='0 0 4 4'%3E%3Cpath fill='%23636363' fill-opacity='1' d='M1 3h1v1H1V3zm2-2h1v1H3V1z'%3E%3C/path%3E%3C/svg%3E",
          size: 4,
        },
        {
          background:
            calculationName === "amendment"
              ? "#ded500"
              : calculationName === "bill"
                ? "#ff6b6b"
                : "rgba(155, 155, 155, 0.5)",
          foreground:
            "data:image/svg+xml,%3Csvg width='20' height='20' viewBox='0 0 20 20' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%23636363' fill-opacity='1' fill-rule='evenodd'%3E%3Ccircle cx='3' cy='3' r='3'/%3E%3Ccircle cx='13' cy='13' r='3'/%3E%3C/g%3E%3C/svg%3E",
          size: 20,
        },
        {
          background:
            calculationName === "amendment"
              ? "#ded500"
              : calculationName === "bill"
                ? "#ff6b6b"
                : "rgba(155, 155, 155, 0.5)",
          foreground:
            "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'%3E%3Cg fill='%23636363' fill-opacity='1'%3E%3Cpolygon fill-rule='evenodd' points='8 4 12 6 8 8 6 12 4 8 0 6 4 4 6 0 8 4'/%3E%3C/g%3E%3C/svg%3E",
          size: 24,
        },
        {
          background:
            calculationName === "amendment"
              ? "#ded500"
              : calculationName === "bill"
                ? "#ff6b6b"
                : "rgba(155, 155, 155, 0.5)",
          foreground:
            "data:image/svg+xml,%3Csvg width='60' height='60' viewBox='0 0 60 60' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='none' fill-rule='evenodd'%3E%3Cg fill='%23636363' fill-opacity='1'%3E%3Cpath d='M36 34v-4h-2v4h-4v2h4v4h2v-4h4v-2h-4zm0-30V0h-2v4h-4v2h4v4h2V6h4V4h-4zM6 34v-4H4v4H0v2h4v4h2v-4h4v-2H6zM6 4V0H4v4H0v2h4v4h2V6h4V4H6z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E",
          size: 60,
        },
        {
          background:
            calculationName === "amendment"
              ? "#ded500"
              : calculationName === "bill"
                ? "#ff6b6b"
                : "rgba(155, 155, 155, 0.5)",
          foreground:
            "data:image/svg+xml,%3Csvg width='12' height='16' viewBox='0 0 12 16' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M4 .99C4 .445 4.444 0 5 0c.552 0 1 .45 1 .99v4.02C6 5.555 5.556 6 5 6c-.552 0-1-.45-1-.99V.99zm6 8c0-.546.444-.99 1-.99.552 0 1 .45 1 .99v4.02c0 .546-.444.99-1 .99-.552 0-1-.45-1-.99V8.99z' fill='%23636363' fill-opacity='1' fill-rule='evenodd'/%3E%3C/svg%3E",
          size: 12,
        },
      ]
    case "cotisations_employeur":
      return [
        {
          background:
            calculationName === "amendment"
              ? "#ded500"
              : calculationName === "bill"
                ? "#ff6b6b"
                : "rgba(59, 59, 59, 0.5)",
        },
        {
          background:
            calculationName === "amendment"
              ? "#ded500"
              : calculationName === "bill"
                ? "#ff6b6b"
                : "rgba(59,59,59,0.5)",
          foreground:
            "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='4' height='4' viewBox='0 0 4 4'%3E%3Cpath fill='%23636363' fill-opacity='1' d='M1 3h1v1H1V3zm2-2h1v1H3V1z'%3E%3C/path%3E%3C/svg%3E",
          size: 4,
        },
        {
          background:
            calculationName === "amendment"
              ? "#ded500"
              : calculationName === "bill"
                ? "#ff6b6b"
                : "rgba(59,59,59,0.5)",
          foreground:
            "data:image/svg+xml,%3Csvg width='20' height='20' viewBox='0 0 20 20' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%23737373' fill-opacity='1' fill-rule='evenodd'%3E%3Ccircle cx='3' cy='3' r='3'/%3E%3Ccircle cx='13' cy='13' r='3'/%3E%3C/g%3E%3C/svg%3E",
          size: 20,
        },
        {
          background:
            calculationName === "amendment"
              ? "#ded500"
              : calculationName === "bill"
                ? "#ff6b6b"
                : "rgba(59,59,59,0.5)",
          foreground:
            "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='22' height='22' viewBox='0 0 22 22'%3E%3Cg fill='%23737373' fill-opacity='1'%3E%3Cpath fill-rule='evenodd' d='M 2.75 0 L 1.8339844 1.8339844 L 0 2.75 L 1.8339844 3.6660156 L 2.75 5.5 L 3.6660156 3.6660156 L 5.5 2.75 L 3.6660156 1.8339844 L 2.75 0 z M 13.75 0 L 12.833984 1.8339844 L 11 2.75 L 12.833984 3.6660156 L 13.75 5.5 L 14.666016 3.6660156 L 16.5 2.75 L 14.666016 1.8339844 L 13.75 0 z M 2.75 11 L 1.8339844 12.833984 L 0 13.75 L 1.8339844 14.666016 L 2.75 16.5 L 3.6660156 14.666016 L 5.5 13.75 L 3.6660156 12.833984 L 2.75 11 z M 13.75 11 L 12.833984 12.833984 L 11 13.75 L 12.833984 14.666016 L 13.75 16.5 L 14.666016 14.666016 L 16.5 13.75 L 14.666016 12.833984 L 13.75 11 z'/%3E%3C/g%3E%3C/svg%3E",
          size: 22,
        },
        {
          background:
            calculationName === "amendment"
              ? "#ded500"
              : calculationName === "bill"
                ? "#ff6b6b"
                : "rgba(59,59,59,0.5)",
          foreground:
            "data:image/svg+xml,%3Csvg width='60' height='60' viewBox='0 0 60 60' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='none' fill-rule='evenodd'%3E%3Cg fill='%23737373' fill-opacity='1'%3E%3Cpath d='m 36,34 v -4 h -2 v 4 h -4 v 2 h 4 v 4 h 2 v -4 h 4 V 34 Z M 36,6 v 4 H 34 V 6 H 30 V 4 h 4 V 0 h 2 v 4 h 4 V 6 Z M 6,34 V 30 H 4 v 4 H 0 v 2 h 4 v 4 h 2 v -4 h 4 V 34 Z M 6,4 V 0 H 4 V 4 H 0 v 2 h 4 v 4 H 6 V 6 h 4 V 4 Z m 15,15 v -4 h -2 v 4 h -4 v 2 h 4 v 4 h 2 v -4 h 4 v -2 z m 0,32 v 4 h -2 v -4 h -4 v -2 h 4 v -4 h 2 v 4 h 4 v 2 z M 51,21 v 4 h -2 v -4 h -4 v -2 h 4 v -4 h 2 v 4 h 4 v 2 z m 0,30 v 4 h -2 v -4 h -4 v -2 h 4 v -4 h 2 v 4 h 4 v 2 z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E",
          size: 60,
        },
        {
          background:
            calculationName === "amendment"
              ? "#ded500"
              : calculationName === "bill"
                ? "#ff6b6b"
                : "rgba(59,59,59,0.5)",
          foreground:
            "data:image/svg+xml,%3Csvg width='12' height='16' viewBox='0 0 12 16' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M4 .99C4 .445 4.444 0 5 0c.552 0 1 .45 1 .99v4.02C6 5.555 5.556 6 5 6c-.552 0-1-.45-1-.99V.99zm6 8c0-.546.444-.99 1-.99.552 0 1 .45 1 .99v4.02c0 .546-.444.99-1 .99-.552 0-1-.45-1-.99V8.99z' fill='%23737373' fill-opacity='1' fill-rule='evenodd'/%3E%3C/svg%3E",
          size: 12,
        },
      ]
    case "allegements":
      return [
        {
          background: "#fff",
          foreground: `data:image/svg+xml,%3Csvg width='40' height='40' viewBox='0 0 40 40' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='${calculationName === "amendment" ? "%23ded500" : calculationName === "bill" ? "%23ff6b6b" : "%23b2b2b2"}' fill-opacity='1' fill-rule='evenodd'%3E%3Cpath d='M 40,30 30,40 H 40 Z M 40,10 10,40 H 20 L 40,20 Z M 30,0 0,30 V 40 L 40,0 Z M 10,0 0,10 V 20 L 20,0 Z'/%3E%3C/g%3E%3C/svg%3E`,
          size: 20,
        },
        {
          background:
            calculationName === "amendment"
              ? "#ded500"
              : calculationName === "bill"
                ? "#ff6b6b"
                : "rgba(178, 178, 178, 0.8)",
          foreground:
            "data:image/svg+xml,%3Csvg width='6' height='6' viewBox='0 0 6 6' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%23ffffff' fill-opacity='1' fill-rule='evenodd'%3E%3Cpath d='M 4.2929688,0 0,4.2929688 V 6 H 0.70703125 L 6,0.70703125 V 0 Z M 6,4.2851562 4.2851562,6 H 6 Z'/%3E%3C/g%3E%3C/svg%3E",
          size: 6,
        },
        {
          background:
            calculationName === "amendment"
              ? "#ded500"
              : calculationName === "bill"
                ? "#ff6b6b"
                : "rgba(178, 178, 178, 0.8)",
          foreground:
            "data:image/svg+xml,%3Csvg width='6' height='6' viewBox='0 0 6 6' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%23ffffff' fill-opacity='1' fill-rule='evenodd'%3E%3Cpath d='M1.70711 -1.87648e-07L6 4.29289L6 6L5.29289 6L-3.09086e-08 0.707106L0 -2.62268e-07L1.70711 -1.87648e-07Z M1.71477 6L-2.62268e-07 6L-1.87313e-07 4.28523L1.71477 6Z'/%3E%3C/g%3E%3C/svg%3E",
          size: 6,
        },
        {
          background:
            calculationName === "amendment"
              ? "#ded500"
              : calculationName === "bill"
                ? "#ff6b6b"
                : "rgba(178, 178, 178, 0.8)",
          foreground:
            "data:image/svg+xml,%3Csvg width='6' height='6' viewBox='0 0 6 6' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%23ffffff' fill-opacity='1' fill-rule='evenodd'%3E%3Cpath d='M6 5L6 6L0 -2.62268e-07L1 -2.18557e-07L6 5Z M1 6L-4.37114e-08 6L0 5L1 6Z'/%3E%3C/g%3E%3C/svg%3E",
          size: 6,
        },
      ]
  }
  return []
}
