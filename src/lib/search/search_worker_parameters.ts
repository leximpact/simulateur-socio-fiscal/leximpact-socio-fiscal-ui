import { type Parameter, walkParameters } from "@openfisca/json-model"
import MiniSearch from "minisearch"

import { rootParameter } from "$lib/parameters"

const miniSearch = new MiniSearch({
  fields: ["id", "short_label", "description", "titles"],
  searchOptions: {
    fuzzy: 0.3,
    prefix: true,
    weights: {
      fuzzy: 0.5,
      prefix: 0.75,
    },
  },
})

const parameters: Parameter[] = [...walkParameters(rootParameter)]
  .reverse()
  .filter(
    (value, index, self) => index === self.findIndex((t) => t.id === value.id),
  )
miniSearch.addAll(parameters)

const parametersById = parameters.reduce(
  (byId: { [id: string]: Parameter }, parameter: Parameter) => {
    if (parameter.id !== undefined) byId[parameter.id] = parameter
    return byId
  },
  {},
)

onmessage = function (event: MessageEvent) {
  // The search string (aka the term) is in event.data.
  const result = miniSearch.search(event.data).slice(0, 50)
  const parameterObjects = result.map((item) => parametersById[item.id])
  postMessage(parameterObjects)
}
