export interface SearchVariable {
  label: string
  name: string
  short_label: string
}
