import {
  decompositionCoreByName,
  withLinkedVariableNames,
} from "$lib/decompositions"
import { miniSearch } from "$lib/search/search_common_variables"
import type { SearchVariable } from "$lib/search/search_variable"
import { variableSummaryByName } from "$lib/variables"

export const withLinkedVariables = withLinkedVariableNames
  .map(
    (name: string) =>
      variableSummaryByName[name] ?? { name, ...decompositionCoreByName[name] },
  )
  .map(
    (variable) =>
      ({
        label: variable.label,
        name: variable.name,
        short_label: variable.short_label,
      }) as SearchVariable,
  )

miniSearch.addAll(withLinkedVariables)

onmessage = function (event: MessageEvent) {
  // The search string (aka the term) is in event.data.
  const result = miniSearch.search(event.data).slice(0, 50)
  const variableObjects = result.map((item) =>
    withLinkedVariables.find((variable) => variable.name === item.id),
  )
  postMessage(variableObjects)
}
