import { diacritiquesMinuscule } from "$lib/strings"

function escapeRegex(s: string): string {
  return s.replace(/[/\-\\^$*+?.()|[\]{}]/g, "\\$&")
}

export function parseSearch(search: string | undefined) {
  const terms = search?.split(/\s+/).filter(Boolean)

  if (terms === undefined) {
    return []
  }

  let regexOK = true
  try {
    terms.forEach((term) => new RegExp(term, "gi").test(""))
  } catch (e) {
    regexOK = false
  }

  return terms.map((term) =>
    (regexOK ? term : escapeRegex(term)).replace(
      new RegExp(Object.keys(diacritiquesMinuscule).join("|"), "gi"),
      (m) => diacritiquesMinuscule[m] || m,
    ),
  )
}
