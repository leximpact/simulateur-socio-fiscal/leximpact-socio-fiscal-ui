import MiniSearch from "minisearch"

export const miniSearch = new MiniSearch({
  fields: ["short_label", "label", "name"],
  idField: "name",
  searchOptions: {
    fuzzy: 0.3,
    prefix: true,
    weights: {
      fuzzy: 0.5,
      prefix: 0.75,
    },
  },
})
