<script lang="ts">
  import {
    isAmountScaleParameter,
    iterParameterAncestors,
    newParameterRepositoryUrl,
    ParameterClass,
    parameterLastReviewOrChange,
    type Parameter,
  } from "@openfisca/json-model"
  import { getContext } from "svelte"

  import { goto } from "$app/navigation"
  import ScaleView from "$lib/components/legislation_view/parameters/technical_details_view/ScaleView.svelte"
  import { metadata } from "$lib/metadata"
  import {
    asAmountScaleParameter,
    asRateScaleParameter,
    buildInstantReferencesAndValueArray,
    labelFromScaleType,
    labelFromValueType,
  } from "$lib/parameters"
  import publicConfig from "$lib/public_config"
  import { shared } from "$lib/shared.svelte"
  import { getUnitByName, getUnitShortLabel } from "$lib/units"
  import { type SelfTargetAProps, newSimulationUrl } from "$lib/urls"

  interface Props {
    billParameter: Parameter
    date: string
    parameter?: Parameter
  }

  const { openfiscaRepository } = publicConfig

  // Note: A reform parameters tree is always more complete than a parameters tree before reform.
  // And the children of a reform node parameter always contain the children of the node parameter
  // before reform (albeit with some different value parameters).
  let { billParameter, date, parameter }: Props = $props()

  const dateFormatter = new Intl.DateTimeFormat("fr-FR", { dateStyle: "full" })
    .format
  const newSelfTargetAProps = getContext("newSelfTargetAProps") as (
    url: string,
  ) => SelfTargetAProps

  let lastReviewOrChange = $derived(parameterLastReviewOrChange(billParameter))

  let parameterRepositoryUrl = $derived(
    newParameterRepositoryUrl(metadata, billParameter),
  )
</script>

<main class="flex items-center justify-center bg-graph-paper">
  <div class="max-w-screen-md bg-white">
    <button
      class="ml-10 mt-5 inline-flex cursor-pointer items-center rounded bg-gray-200 p-2 pr-3 text-sm text-black shadow-md hover:bg-gray-300 active:bg-gray-400"
      onclick={() =>
        goto(
          shared.displayMode === undefined
            ? "/"
            : newSimulationUrl(shared.displayMode),
        )}
    >
      <iconify-icon class="text-2xl" icon="ri-arrow-left-line"></iconify-icon>
      <span class="ml-3">Retour au simulateur</span>
    </button>
    <div class="flex-col items-start pb-4">
      <div class="p-10">
        <p class="mb-2 uppercase">Détail du paramètre :</p>

        <div class="mb-4 border-l-2 border-black pl-4">
          <p class="font-serif text-3xl font-bold">
            {billParameter.title}
          </p>
          <span class="font-serif text-sm italic"
            >{billParameter.description}</span
          >
          <div class="mt-7 w-3/5 rounded border p-2">
            <h2 class="text-base">Parents du paramètre&nbsp;:</h2>
            {#each [...iterParameterAncestors(billParameter.parent)] as ancestor}
              <p class="inline font-serif text-sm">
                <!-- svelte-ignore a11y_missing_attribute -->
                <a
                  class="lx-link-text font-serif text-sm text-gray-500"
                  {...newSelfTargetAProps(`/parameters/${ancestor.name}`)}
                  >{ancestor.title}</a
                >
                &nbsp;&gt;&nbsp;
              </p>
            {/each}
            <span class="text-sm"> {billParameter.title}</span>
          </div>
        </div>

        {#if billParameter.class !== ParameterClass.Node}
          <div class="inline-flex items-center">
            {#if lastReviewOrChange === undefined || lastReviewOrChange < (new Date().getFullYear() - 2).toString()}
              <iconify-icon
                class="mr-1 text-[#FFAC33] shadow-none"
                icon="material-symbols:warning-rounded"
                width="24"
                height="24"
              ></iconify-icon>
            {:else}
              <iconify-icon
                class="mr-1 text-le-vert-validation"
                icon="material-symbols:new-releases"
                width="24"
                height="24"
              ></iconify-icon>
            {/if}
            <p class="text-sm text-black">
              {#if lastReviewOrChange !== undefined}Ce paramètre a été {#if billParameter.last_value_still_valid_on === lastReviewOrChange}
                  vérifié
                {:else}
                  mis à jour
                {/if}
                le
                <span class="font-bold"
                  >{dateFormatter(new Date(lastReviewOrChange))}
                </span>
              {:else}
                Ce paramètre n'a pas de date de relecture connue.
              {/if}
              {#if parameter !== undefined}
                ⎪
                <a
                  class="lx-link-text text-sm text-gray-700"
                  href="/parameters/{parameter.name}/edit"
                >
                  Proposer une modification
                </a>
              {/if}
            </p>
          </div>
        {/if}

        {#if parameter !== undefined && parameter.file_path !== undefined}
          <div class="mt-4 flex">
            <a
              class="mx-4 mb-3 flex h-8 grow-0 items-center rounded-md bg-gray-300 px-2 py-1 text-sm uppercase text-black shadow-md hover:bg-le-bleu hover:text-white focus:outline-none md:mx-0"
              href="https://github.com/{openfiscaRepository.group}/{openfiscaRepository.project}/blob/{openfiscaRepository.branch}/{encodeURIComponent(
                parameter.file_path,
              )}"
              target="_blank"
              rel="noreferrer"
              >Voir le paramètre sur OpenFisca<iconify-icon
                class="ml-1 align-[-0.1rem] text-base"
                icon="ri-external-link-line"
              ></iconify-icon></a
            >
          </div>
        {/if}

        {#if billParameter.class === ParameterClass.Node}
          {#if billParameter.children !== undefined}
            <ul class="ml-4">
              {#each Object.entries(billParameter.children) as [childId, child]}
                <li class="my-2">
                  <!-- svelte-ignore a11y_missing_attribute -->
                  <a
                    class="lx-link-text"
                    {...newSelfTargetAProps(`/parameters/${child.name}`)}
                  >
                    {child.title}
                  </a>
                </li>
              {/each}
            </ul>
          {/if}
        {:else if billParameter.class === ParameterClass.Scale}
          <ScaleView parameter={billParameter} />
        {:else if billParameter.class === ParameterClass.Value}
          <!--Tableau des valeurs-->
          <div class="pb-5">
            <h2 class="pb-3 pt-7 text-xl font-bold">
              Historique des valeurs&nbsp;:
            </h2>
            <table
              class="w-full table-auto border-collapse bg-gray-100 text-sm"
            >
              <thead>
                <tr>
                  <th class="border bg-white p-1 text-center font-bold">Date</th
                  >
                  <!-- <th class="border p-1 text-center">Nom</th> -->
                  <th class="border bg-white p-1 text-center font-bold"
                    >Valeur</th
                  >
                  <th class="border bg-white p-1 text-center font-bold"
                    >Unité</th
                  >
                  <th
                    class="items-center border bg-white p-1 text-center font-bold"
                    ><!--Material UI Icon Assignment-->
                    <svg
                      class="mr-1 inline-flex h-4 w-4 fill-current"
                      height="24px"
                      viewBox="0 0 24 24"
                      width="24px"
                      fill="#000000"
                      ><path d="M0 0h24v24H0V0z" fill="none" /><path
                        d="M7 15h7v2H7zm0-4h10v2H7zm0-4h10v2H7zm12-4h-4.18C14.4 1.84 13.3 1 12 1c-1.3 0-2.4.84-2.82 2H5c-.14 0-.27.01-.4.04-.39.08-.74.28-1.01.55-.18.18-.33.4-.43.64-.1.23-.16.49-.16.77v14c0 .27.06.54.16.78s.25.45.43.64c.27.27.62.47 1.01.55.13.02.26.03.4.03h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-7-.25c.41 0 .75.34.75.75s-.34.75-.75.75-.75-.34-.75-.75.34-.75.75-.75zM19 19H5V5h14v14z"
                      /></svg
                    >Références législatives</th
                  >
                </tr>
              </thead>
              <tbody>
                {#each buildInstantReferencesAndValueArray(billParameter) as { instant, references, valueAtInstant }}
                  <tr>
                    <td class="border p-1 text-center font-serif">{instant}</td>
                    {#if valueAtInstant === undefined}
                      <td class="border p-1 text-center italic" colspan="2"
                      ></td>
                    {:else if valueAtInstant === "expected"}
                      <td class="border p-1 text-center italic" colspan="3"
                        >valeur attendue</td
                      >
                    {:else}
                      <!-- TODO: Handle when valueAtInstant.value is a string array or a string by string dict. -->
                      <td class="border p-1 text-center font-serif"
                        >{getUnitByName(
                          valueAtInstant.unit ?? billParameter.unit,
                        )?.ratio && typeof valueAtInstant.value === "number"
                          ? parseFloat((valueAtInstant.value * 100).toFixed(8)) // trick to round value * 100
                          : (valueAtInstant.value ?? "")}</td
                      >
                      <td class="border p-1 text-center"
                        >{getUnitShortLabel(
                          valueAtInstant.unit ?? billParameter.unit,
                          instant,
                        )}</td
                      >
                    {/if}
                    {#if valueAtInstant !== "expected"}
                      <td
                        class="border bg-le-gris-dispositif-ultralight p-1 text-center font-serif text-xs"
                        >{#if references.length > 0}
                          <ul>
                            {#each references as { href, note, title }}
                              <li>
                                {#if href === undefined}{title}{:else}<a
                                    class="lx-link-text"
                                    {href}
                                    rel="noreferrer"
                                    target="_blank">{title ?? "source"}</a
                                  >{/if}
                                {#if note}
                                  <p>{note}</p>
                                {/if}
                              </li>
                            {/each}
                          </ul>{/if}</td
                      >
                    {/if}
                  </tr>
                {/each}
              </tbody>
            </table>
          </div>
          <!--Fin du tableau des valeurs-->
        {/if}

        <h2 class="mt-7 pb-3 text-xl font-bold">Caractéristiques&nbsp;:</h2>
        <div class="">
          {#if billParameter.class === ParameterClass.Node}
            {#if billParameter.unit !== undefined}
              <div class="font-base my-1 flex border-b py-1">
                <p class="mr-1">
                  Unité du paramètre&nbsp;:: <span class="font-bold">
                    {getUnitShortLabel(billParameter.unit, date)}</span
                  >
                </p>
              </div>
            {/if}
          {:else if billParameter.class === ParameterClass.Scale}
            <div>
              <p class="font-base my-1 mr-1 flex py-1">
                Barème&nbsp;:&nbsp;<span class="font-bold">
                  {labelFromScaleType(billParameter.type)}</span
                >
              </p>
              {#if billParameter.threshold_unit !== undefined}
                <p class="font-base my-1 mr-1 flex py-1">
                  Unité de seuil&nbsp;:&nbsp;<span class="font-bold">
                    {getUnitShortLabel(
                      billParameter.threshold_unit,
                      date,
                    )}</span
                  >
                </p>
              {/if}
              {#if isAmountScaleParameter(billParameter)}
                {#if asAmountScaleParameter(billParameter).amount_unit !== undefined}
                  <p class="font-base my-1 mr-1 flex py-1">
                    Unité de montant&nbsp;:&nbsp; <span class="font-bold">
                      {getUnitShortLabel(
                        asAmountScaleParameter(billParameter).amount_unit,
                        date,
                      )}</span
                    >
                  </p>
                {/if}
              {:else if asRateScaleParameter(billParameter).rate_unit !== undefined}
                <p class="font-base my-1 mr-1 flex py-1">
                  Unité de taux&nbsp;:&nbsp;<span class="font-bold">
                    {getUnitShortLabel(
                      asRateScaleParameter(billParameter).rate_unit,
                      date,
                    )}</span
                  >
                </p>
              {/if}
            </div>
          {:else if billParameter.class === ParameterClass.Value}
            <p class="font-base my-1 mr-1 flex py-1">
              Valeur de type&nbsp;:&nbsp;<span class="font-bold"
                >{labelFromValueType(billParameter.type)}</span
              >
            </p>
            {#if billParameter.unit !== undefined}
              <div class="font-base my-1 flex py-1">
                <p class="mr-1">
                  Unité de la valeur&nbsp;:&nbsp;<span class="font-bold"
                    >{getUnitShortLabel(billParameter.unit, date)}</span
                  >
                </p>
              </div>
            {/if}
          {/if}
          {#if billParameter.documentation !== undefined}
            <p class="mr-1">
              Commentaire&nbsp;:&nbsp;<span class="italic"
                >{billParameter.documentation}</span
              >
            </p>
          {/if}
          {#if parameterRepositoryUrl !== undefined}
            <div class="font-base my-1 flex py-1">
              <p>
                <a
                  class="lx-link-text text-sm text-gray-900"
                  href={parameterRepositoryUrl}
                  rel="noreferrer"
                  target="_blank"
                >
                  <svg
                    class="inline h-5 w-5 fill-current"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 24 24"
                    width="24"
                    height="24"
                    ><path fill="none" d="M0 0h24v24H0z" /><path
                      d="M12 2C6.475 2 2 6.475 2 12a9.994 9.994 0 0 0 6.838 9.488c.5.087.687-.213.687-.476 0-.237-.013-1.024-.013-1.862-2.512.463-3.162-.612-3.362-1.175-.113-.288-.6-1.175-1.025-1.413-.35-.187-.85-.65-.013-.662.788-.013 1.35.725 1.538 1.025.9 1.512 2.338 1.087 2.912.825.088-.65.35-1.087.638-1.337-2.225-.25-4.55-1.113-4.55-4.938 0-1.088.387-1.987 1.025-2.688-.1-.25-.45-1.275.1-2.65 0 0 .837-.262 2.75 1.026a9.28 9.28 0 0 1 2.5-.338c.85 0 1.7.112 2.5.337 1.912-1.3 2.75-1.024 2.75-1.024.55 1.375.2 2.4.1 2.65.637.7 1.025 1.587 1.025 2.687 0 3.838-2.337 4.688-4.562 4.938.362.312.675.912.675 1.85 0 1.337-.013 2.412-.013 2.75 0 .262.188.574.688.474A10.016 10.016 0 0 0 22 12c0-5.525-4.475-10-10-10z"
                    /></svg
                  >
                  Formule OpenFisca sur Github</a
                >
              </p>
            </div>
          {/if}
        </div>

        {#if billParameter.referring_variables !== undefined}
          <!--Informations connexes-->
          <section class="mt-7">
            <h2 class="pb-3 pt-7 text-xl font-bold">
              Formules de calcul où le paramètre apparaît :
            </h2>
            <ul class="list-inside list-disc">
              {#each billParameter.referring_variables as variableName}
                <li>
                  <!-- svelte-ignore a11y_missing_attribute -->
                  <a
                    class="lx-link-text text-gray-900"
                    {...newSelfTargetAProps(`/variables/${variableName}`)}
                    >{variableName}</a
                  >
                </li>
              {/each}
            </ul>
          </section>
        {/if}
        {#if parameter !== undefined && parameter.file_path !== undefined}
          <div class="my-4 flex justify-end">
            <a
              class="inline-block shrink-0 rounded bg-le-bleu px-4 py-2 text-center text-sm uppercase text-white shadow-md hover:bg-blue-900"
              href="/parameters/{parameter.name}/edit"
            >
              Proposer une modification
            </a>
          </div>
        {/if}
      </div>
    </div>
  </div>
</main>
