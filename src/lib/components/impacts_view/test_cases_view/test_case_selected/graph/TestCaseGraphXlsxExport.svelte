<script lang="ts">
  import {
    isAmountScaleParameter,
    type MaybeNumberValue,
    type NumberValue,
    type ScaleParameter,
    scaleParameterUsesBase,
    type Unit,
    type VariableByName,
    type Waterfall,
  } from "@openfisca/json-model"
  import XLSX from "xlsx-js-style"

  import { page } from "$app/stores"
  import type { GraphDomain } from "$lib/components/ui_transverse_components/piece_of_cake/types"
  import { decompositionCoreByName } from "$lib/decompositions"
  import type { DisplayMode } from "$lib/displays"
  import { entityByKey } from "$lib/entities"
  import type { VariableGraph } from "$lib/graphes"
  import { metadata } from "$lib/metadata"
  import {
    asAmountBracketAtInstant,
    asAmountScaleParameter,
    asRateBracketAtInstant,
    asRateBracketAtInstantOrNullable,
    asRateScaleParameter,
    asValueParameter,
    getParameter,
    rootParameter,
    rootParameterByReformName,
  } from "$lib/parameters"
  import publicConfig from "$lib/public_config"
  import { ParameterReformChangeType } from "$lib/reforms"
  import { billActive, billName, budgetDate, shared } from "$lib/shared.svelte"
  import { publishTestCaseSimulation } from "$lib/simulations"
  import {
    getSituationVariableValue,
    iterSituationVariablesName,
    type Situation,
  } from "$lib/situations"
  import { getUnitAtDate } from "$lib/units"
  import { formatValue } from "$lib/values"
  import type { VariableValue } from "$lib/variables"

  interface Props {
    displayMode: DisplayMode
    domain: GraphDomain
    situation: Situation
    situationIndex: number
    variableSummaryByName: VariableByName
    variableValues: VariableGraph[][]
    waterfall: Waterfall
    year: number
  }

  const { baseUrl } = publicConfig

  let {
    displayMode,
    domain,
    situation,
    situationIndex,
    variableSummaryByName,
    variableValues,
    waterfall,
    year,
  }: Props = $props()

  const dateFormatter = new Intl.DateTimeFormat("fr-FR", {
    day: "2-digit",
    month: "2-digit",
    year: "numeric",
    hour: "2-digit",
    minute: "2-digit",
    hour12: false,
  }).format
  const formatValueWithUnit = (
    value: number | undefined,
    unit: Unit | undefined,
  ) =>
    value === undefined
      ? undefined
      : unit?.ratio
        ? parseFloat((value * 100).toFixed(8)) // trick to round value * 100
        : value

  // Note: A reform parameters tree is always more complete than a parameters tree before reform.
  // And the children of a reform node parameter always contain the children of the node parameter
  // before reform (albeit with some different value parameters).
  let billRootParameter = $derived(
    rootParameterByReformName[billName] ?? rootParameter,
  )

  async function xlsxExport() {
    const workbook = XLSX.utils.book_new()
    workbook.Props = {
      Author: "Leximpact",
      Comments:
        "Tableur créé automatiquement par le simulateur socio-fiscal Leximpact, utilisant le moteur libre OpenFisca",
      Company: "Assemblée nationale",
      CreatedDate: new Date(),
      Title: `${situation.title}`,
    }
    workbook.Workbook = {
      Names: [],
      WBProps: { date1904: true },
    }
    // 6 sheets: "À lire", "Impacts code en vigueur", "Impacts PLF - PLFSS", "Impacts Réforme", "Réforme", "Caractérstiques cas type"

    // "À lire" sheet:
    {
      const sheetName = "À lire"
      const token = await publishTestCaseSimulation(
        null, // blobImageOpenGraph
        displayMode,
        shared.inputInstantsByVariableNameArray,
        shared.parametricReform,
        shared.testCases,
      )
      const sheetData = [
        [],
        [],
        [],
        [
          undefined,
          {
            v: 'Export "cas types" du simulateur socio-fiscal LexImpact',
            t: "s",
            s: { font: { bold: true, sz: 16 } },
          },
        ],
        [],
        [
          undefined,
          {
            v: "Caractéristiques du graphique exporté :",
            t: "s",
            s: { font: { bold: true, sz: 14 } },
          },
        ],
        [
          undefined,
          "Lien du cas type et de la réforme :",
          new URL(`/test_cases/simulations/${token}`, baseUrl).toString(),
        ],
        [
          undefined,
          "Type de fiche de paie exportée (particulier ou employeur) :",
          waterfall.name === "brut_to_disponible"
            ? "particulier"
            : waterfall.name === "brut_to_super_brut"
              ? "employeur"
              : "N/A",
        ],
        [
          undefined,
          "Fourchette de l'axe des X :",
          `${domain.x[0]}-${domain.x[1]}`,
        ],
        [undefined, "Date d'export :", dateFormatter(new Date())],
        [
          undefined,
          "Version d'openfisca :",
          metadata.distributions["OpenFisca-France"].version ?? "N/A",
        ],
        [],
        [],
        [
          undefined,
          {
            v: "Sources :",
            t: "s",
            s: { font: { bold: true, sz: 14 } },
          },
        ],
        [
          undefined,
          "Lien du simulateur socio-fiscal :",
          "https://socio-fiscal.leximpact.an.fr/",
        ],
        [
          undefined,
          "Lien vers openfisca github :",
          "https://github.com/openfisca/openfisca-france",
        ],
        [],
        [],
        [
          undefined,
          {
            v: "Note explicative de ce qui est exporté :",
            t: "s",
            s: { font: { bold: true, sz: 14 } },
          },
        ],
        [
          undefined,
          'Ce tableur est un export des impacts présents sur le graphique "cas type"  que vous avez sélectionné sur le simulateur socio-fiscal LexImpact.\nLa fonctionnalité exporte uniquement ce que vous voyez sur le graphique, par exemple : \n- si vous avez effectué l\'export depuis la partie "employeur" de la feuille de paie, vous n\'aurez que les valeurs des dispositifs employeurs. \n- si vous avez zoomé sur le graphique, l\'export ne contiendra que les valeurs des nouveaux 100 cas types qui auront été calculés.',
        ],
      ]
      // Add the sheet to the workbook
      const worksheet = XLSX.utils.aoa_to_sheet(sheetData)
      XLSX.utils.book_append_sheet(workbook, worksheet, sheetName)
    }

    const exportVariables = variableValues.flat()
    const calculationRows = []
    // "Impact code en vigueur" sheet:
    {
      const sheetName = billActive
        ? `Impact Code ${year} sans PLF.SS`
        : "Impact code en vigueur"
      const sheetData = []
      // Add the column titles
      sheetData.push([
        "Cas type",
        ...exportVariables.map((variable) => variable.label),
      ])
      // Add the rows
      for (let i = 0; i < 100; i++) {
        const rowData = [i]
        for (const variable of exportVariables) {
          const calculation = variable.rows.find(({ calculationName }) =>
            ["revaluation", "law"].includes(calculationName),
          )
          rowData.push(calculation?.delta[i])
        }
        calculationRows.push(rowData)
        sheetData.push(rowData)
      }
      // Add the sheet to the workbook
      const worksheet = XLSX.utils.aoa_to_sheet(sheetData)
      XLSX.utils.book_append_sheet(workbook, worksheet, sheetName)
    }

    // "Impact PLF.SS" sheet:
    if (billActive) {
      const sheetName = `Impact PLF.SS ${year}`
      const sheetData = []
      // Add the column titles
      sheetData.push([
        "Cas type",
        ...exportVariables.map((variable) => variable.label),
      ])
      // Add the rows
      for (let i = 0; i < 100; i++) {
        const rowData = [...calculationRows[i]]
        for (const [j, variable] of exportVariables.entries()) {
          const calculation = variable.rows.find(
            ({ calculationName }) => calculationName === "bill",
          )
          if (calculation !== undefined) {
            rowData[j + 1] = calculation?.delta[i]
          }
        }
        sheetData.push(rowData)
      }
      // Add the sheet to the workbook
      const worksheet = XLSX.utils.aoa_to_sheet(sheetData)
      XLSX.utils.book_append_sheet(workbook, worksheet, sheetName)
    }

    // "Impact Réforme" sheet:
    {
      const sheetName = billActive
        ? `Impact Réforme ${year} vs PLF.SS`
        : "Impact Réforme"
      const sheetData = []
      if (Object.keys(shared.parametricReform).length > 0) {
        // Add the column titles
        sheetData.push([
          "Cas type",
          ...exportVariables.map((variable) => variable.label),
        ])
        // Add the rows
        for (let i = 0; i < 100; i++) {
          const rowData = [...calculationRows[i]]
          for (const [j, variable] of exportVariables.entries()) {
            const calculation = variable.rows.find(
              ({ calculationName }) => calculationName === "amendment",
            )
            if (calculation !== undefined) {
              rowData[j + 1] = calculation?.delta[i]
            }
          }
          sheetData.push(rowData)
        }
      }
      // Add the sheet to the workbook
      const worksheet = XLSX.utils.aoa_to_sheet(sheetData)
      XLSX.utils.book_append_sheet(workbook, worksheet, sheetName)
    }

    // "Paramètres de votre réforme" sheet:
    {
      const sheetName = "Paramètres de votre réforme"
      const sheetData: unknown[][] = [
        [
          {
            v: "Réforme",
            t: "s",
            s: { font: { bold: true, sz: 12 } },
          },
        ],
        [],
      ]
      for (const [parameterName, parameterReform] of Object.entries(
        shared.parametricReform,
      )) {
        const parameter = getParameter(billRootParameter, parameterName)
        if (parameterReform.type === ParameterReformChangeType.Parameter) {
          const parameterValue = formatValue(
            parameterReform.value,
            asValueParameter(parameter)?.unit,
          )
          sheetData.push([parameter?.title, undefined, parameterValue])
        } else if (parameterReform.type === ParameterReformChangeType.Scale) {
          for (const [i, bracketAtInstant] of parameterReform.scale.entries()) {
            const thresholdUnit = getUnitAtDate(
              (parameter as ScaleParameter).threshold_unit,
              budgetDate,
            )

            const thresholdValue =
              bracketAtInstant.threshold === "expected"
                ? undefined
                : (bracketAtInstant.threshold?.value ?? undefined)

            const xlsxRow = [
              i === 0 ? parameter?.title : undefined,
              undefined,
              formatValueWithUnit(thresholdValue, thresholdUnit),
            ]

            const isAmountScale = isAmountScaleParameter(
              parameter as ScaleParameter,
            )

            if (isAmountScale) {
              const amountUnit = getUnitAtDate(
                asAmountScaleParameter(parameter as ScaleParameter).amount_unit,
                budgetDate,
              )

              const amountValue =
                asAmountBracketAtInstant(bracketAtInstant).amount === "expected"
                  ? undefined
                  : ((
                      asAmountBracketAtInstant(bracketAtInstant)
                        .amount as NumberValue
                    ).value ?? undefined)

              xlsxRow.push(formatValueWithUnit(amountValue, amountUnit))
            } else {
              const usesBase = scaleParameterUsesBase(
                parameter as ScaleParameter,
              )

              if (usesBase) {
                const baseValue =
                  asRateBracketAtInstant(bracketAtInstant).base === "expected"
                    ? undefined
                    : ((
                        asRateBracketAtInstantOrNullable(bracketAtInstant)
                          ?.base as NumberValue | undefined
                      )?.value ?? undefined)

                xlsxRow.push(formatValueWithUnit(baseValue, thresholdUnit))
              }

              const rateUnit = getUnitAtDate(
                asRateScaleParameter(parameter as ScaleParameter).rate_unit,
                budgetDate,
              )

              const rateValue =
                asRateBracketAtInstant(bracketAtInstant).rate === "expected"
                  ? undefined
                  : ((
                      asRateBracketAtInstant(bracketAtInstant)
                        .rate as MaybeNumberValue
                    )?.value ?? undefined)

              xlsxRow.push(formatValueWithUnit(rateValue, rateUnit))
            }

            sheetData.push(xlsxRow)
          }
        }
      }
      // Add the sheet to the workbook
      const worksheet = XLSX.utils.aoa_to_sheet(sheetData)
      XLSX.utils.book_append_sheet(workbook, worksheet, sheetName)
    }

    // "Paramètres du cas type" sheet:
    {
      const sheetName = "Paramètres du cas type"
      const sheetData: unknown[][] = [
        [
          {
            v: "Caractéristiques du cas type principal",
            t: "s",
            s: { font: { bold: true, sz: 12 } },
          },
        ],
        [],
      ]
      let variablesName = new Set(
        Object.entries(decompositionCoreByName)
          .filter(([, decomposition]) => !decomposition.virtual)
          .map(([name]) => name),
      )
      const newVariablesName = [
        ...iterSituationVariablesName(entityByKey, situation),
      ]
      if (
        newVariablesName.some(
          (variableName) => !variablesName.has(variableName),
        )
      ) {
        variablesName = new Set([...variablesName, ...newVariablesName])
      }
      const inputInstantsByVariableName =
        shared.inputInstantsByVariableNameArray[situationIndex]
      const entityLabelByKey: { [key: string]: string } = {
        famille: "Famille",
        foyer_fiscal: "Foyer fiscal",
        individu: "Individu",
        menage: "Ménage",
      }
      let variablesByEntity: {
        [entity: string]: [
          { v: string | undefined; t: string; s: { font: { sz: number } } },
          VariableValue,
        ][]
      } = {}
      for (const variableName of variablesName) {
        if (
          (inputInstantsByVariableName[variableName] ?? new Set()).has(
            year.toString(),
          )
        ) {
          const capitalize = (s: string) => s[0].toUpperCase() + s.slice(1)

          const variable = variableSummaryByName[variableName]

          const entity = entityByKey[variable.entity]
          const entityLabel =
            entityLabelByKey[variable.entity] ??
            capitalize(variable.entity.replace("_", " "))
          const entitySituation = situation[entity.key_plural]

          for (const populationId of Object.keys(entitySituation ?? {})) {
            if (!Object.hasOwn(variablesByEntity, entityLabel)) {
              variablesByEntity[entityLabel] = []
            }
            variablesByEntity[entityLabel].push([
              {
                v: variable.label,
                t: "s",
                s: { font: { sz: 12 } },
              },
              getSituationVariableValue(
                situation,
                variable,
                populationId,
                year,
              ),
            ])
          }
        }
      }
      for (const [entityLabel, variables] of Object.entries(
        variablesByEntity,
      )) {
        for (const variable of variables) {
          sheetData.push([entityLabel, ...variable])
        }
        sheetData.push([])
      }
      // Add the sheet to the workbook
      const worksheet = XLSX.utils.aoa_to_sheet(sheetData)
      XLSX.utils.book_append_sheet(workbook, worksheet, sheetName)
    }

    XLSX.writeFile(
      workbook,
      `leximpact_export_cas_type_${situationIndex}.xlsx`,
      { compression: true },
    )
  }
</script>

<div class="flex flex-col justify-end gap-2">
  <button
    class="lx-link-simple text-right text-gray-600 hover:text-black active:text-gray-700 disabled:cursor-not-allowed disabled:opacity-50"
    onclick={xlsxExport}
  >
    <iconify-icon
      class="mr-1 align-[-0.2rem] text-xl"
      icon="ri-file-download-line"
    ></iconify-icon>Exporter les données*
  </button>
  <span class="text-right text-xs text-gray-400"
    >*Données des 100 cas types du graphique | .xlsx - 50 Ko</span
  >
</div>
