export type AxisDomain = [number, number] // [min, max]

export interface GraphDomain {
  x: AxisDomain
  y: AxisDomain
}
