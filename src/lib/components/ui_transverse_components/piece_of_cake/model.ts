import { scaleLinear, type ScaleLinear } from "d3-scale"

import type { CalculationName } from "$lib/calculations.svelte"
import type { VariableCustomization } from "$lib/graphes"

export class CurveModel {
  #height?: number
  #valuesScaled?: Array<[number, number]>
  #width?: number
  #xDomain?: [number, number]
  #xRange?: [number, number]
  #xScale?: ScaleLinear<number, number, never>
  #xValues?: number[]
  #xValuesScaled?: number[]
  #yDomain?: [number, number]
  #yRange?: [number, number]
  #yScale?: ScaleLinear<number, number, never>
  #yValues?: number[]
  #yValuesScaled?: number[]

  public readonly xNice?: boolean | number = undefined
  public readonly xScaler = scaleLinear
  public readonly yNice?: boolean | number = undefined
  public readonly yScaler = scaleLinear

  public readonly calculationName?: CalculationName
  public readonly containerHeight: number = 240
  public readonly containerWidth: number = 320
  public readonly customizations?: VariableCustomization
  public readonly depth?: number
  public readonly id?: string
  public readonly ignore?: boolean
  public readonly label?: string
  public readonly name?: string
  public readonly neg?: boolean
  public readonly padding: {
    bottom: number
    left: number
    right: number
    top: number
  } = { bottom: 0, left: 0, right: 0, top: 0 }
  public readonly trunk?: boolean

  constructor(
    public readonly values: Array<[number, number]>,
    {
      calculationName,
      containerHeight,
      containerWidth,
      customizations,
      depth,
      id,
      ignore,
      label,
      name,
      neg,
      padding,
      trunk,
      xDomain,
      xNice,
      xScaler,
      yDomain,
      yNice,
      yScaler,
    }: {
      calculationName?: string
      containerHeight?: number
      containerWidth?: number
      customizations?: VariableCustomization
      depth?: number
      id?: string
      ignore?: boolean
      label?: string
      name?: string
      neg?: boolean
      padding?: {
        bottom: number
        left: number
        right: number
        top: number
      }
      trunk?: boolean
      xDomain?: [number, number]
      xNice?: boolean | number
      xScaler?: typeof scaleLinear
      yDomain?: [number, number]
      yNice?: boolean | number
      yScaler?: typeof scaleLinear
    } = {},
  ) {
    if (calculationName !== undefined) {
      this.calculationName = calculationName
    }
    if (
      containerHeight !== undefined &&
      containerHeight !== this.containerHeight
    ) {
      this.containerHeight = containerHeight
    }
    if (
      containerWidth !== undefined &&
      containerWidth !== this.containerWidth
    ) {
      this.containerWidth = containerWidth
    }
    if (customizations !== undefined) {
      this.customizations = customizations
    }
    if (depth !== undefined) {
      this.depth = depth
    }
    if (id !== undefined) {
      this.id = id
    }
    if (ignore !== undefined) {
      this.ignore = ignore
    }
    if (label !== undefined) {
      this.label = label
    }
    if (name !== undefined) {
      this.name = name
    }
    if (neg !== undefined) {
      this.neg = neg
    }
    if (padding !== undefined && padding !== this.padding) {
      this.padding = padding
    }
    if (trunk !== undefined) {
      this.trunk = trunk
    }
    if (xDomain !== undefined) {
      this.#xDomain = xDomain
    }
    if (xNice !== undefined) {
      this.xNice = xNice
    }
    this.xScaler = xScaler ?? scaleLinear
    if (yDomain !== undefined) {
      this.#yDomain = yDomain
    }
    if (yNice !== undefined) {
      this.yNice = yNice
    }
    this.yScaler = yScaler ?? scaleLinear
  }

  get height(): number {
    if (this.#height === undefined) {
      const { bottom, top } = this.padding
      this.#height = this.containerHeight - top - bottom
    }
    return this.#height
  }

  setContainerDimensions(
    containerWidth: number,
    containerHeight: number,
  ): CurveModel {
    return new CurveModel(this.values, {
      calculationName: this.calculationName,
      containerHeight,
      containerWidth,
      customizations: this.customizations,
      depth: this.depth,
      id: this.id,
      ignore: this.ignore,
      label: this.label,
      name: this.name,
      neg: this.neg,
      padding: this.padding,
      trunk: this.trunk,
      xDomain: this.xDomain,
      xNice: this.xNice,
      xScaler: this.xScaler,
      yDomain: this.yDomain,
      yNice: this.yNice,
      yScaler: this.yScaler,
    })
  }

  setPadding(padding: {
    bottom: number
    left: number
    right: number
    top: number
  }): CurveModel {
    return new CurveModel(this.values, {
      calculationName: this.calculationName,
      containerHeight: this.containerHeight,
      containerWidth: this.containerWidth,
      customizations: this.customizations,
      depth: this.depth,
      id: this.id,
      ignore: this.ignore,
      label: this.label,
      name: this.name,
      neg: this.neg,
      padding,
      trunk: this.trunk,
      xDomain: this.xDomain,
      xNice: this.xNice,
      xScaler: this.xScaler,
      yDomain: this.yDomain,
      yNice: this.yNice,
      yScaler: this.yScaler,
    })
  }

  setValues(values: Array<[number, number]>): CurveModel {
    return new CurveModel(values, {
      calculationName: this.calculationName,
      containerHeight: this.containerHeight,
      containerWidth: this.containerWidth,
      customizations: this.customizations,
      depth: this.depth,
      id: this.id,
      ignore: this.ignore,
      label: this.label,
      name: this.name,
      neg: this.neg,
      padding: this.padding,
      trunk: this.trunk,
      xDomain: this.xDomain,
      xNice: this.xNice,
      xScaler: this.xScaler,
      yDomain: this.yDomain,
      yNice: this.yNice,
      yScaler: this.yScaler,
    })
  }

  get valuesScaled(): Array<[number, number]> {
    if (this.#valuesScaled === undefined) {
      const xScale = this.xScale
      const yScale = this.yScale
      this.#valuesScaled = this.values.map(([x, y]) => [xScale(x), yScale(y)])
    }
    return this.#valuesScaled
  }

  get width(): number {
    if (this.#width === undefined) {
      const { left, right } = this.padding
      this.#width = this.containerWidth - left - right
    }
    return this.#width
  }

  get xDomain(): [number, number] {
    if (this.#xDomain === undefined) {
      this.#xDomain = this.xValues.reduce(
        ([min, max]: [number | null, number | null], x): [number, number] => {
          if (max === null || max < x) {
            max = x
          }
          if (min === null || min > x) {
            min = x
          }
          return [min, max]
        },
        [null, null],
      ) as [number, number]
    }
    return this.#xDomain
  }

  get xRange() {
    if (this.#xRange === undefined) {
      this.#xRange = [0, this.width]
    }
    return this.#xRange
  }

  get xScale() {
    if (this.#xScale === undefined) {
      const xScale = this.xScaler().domain(this.xDomain).range(this.xRange)
      if (this.xNice !== undefined && this.xNice !== false) {
        if (typeof xScale.nice === "function") {
          xScale.nice(typeof this.xNice === "number" ? this.xNice : undefined)
        } else {
          console.error(
            "[Piece of Cake] Can't use `xNice`, because `xScale` doesn't have a `nice` method. Ignoring…",
          )
        }
      }
      this.#xScale = xScale
    }
    return this.#xScale
  }

  get xValues(): number[] {
    if (this.#xValues === undefined) {
      this.#xValues = this.values.map(([x]) => x)
    }
    return this.#xValues
  }

  get xValuesScaled(): number[] {
    if (this.#xValuesScaled === undefined) {
      const xScale = this.xScale
      this.#xValuesScaled = this.xValues.map((x) => xScale(x))
    }
    return this.#xValuesScaled
  }

  get yDomain(): [number, number] {
    if (this.#yDomain === undefined) {
      this.#yDomain = this.yValues.reduce(
        ([min, max]: [number | null, number | null], y): [number, number] => {
          if (max === null || max < y) {
            max = y
          }
          if (min === null || min > y) {
            min = y
          }
          return [min, max]
        },
        [null, null],
      ) as [number, number]
    }
    return this.#yDomain
  }

  get yRange() {
    if (this.#yRange === undefined) {
      this.#yRange = [this.height, 0]
    }
    return this.#yRange
  }

  get yScale() {
    if (this.#yScale === undefined) {
      const yScale = this.yScaler().domain(this.yDomain).range(this.yRange)
      if (this.yNice !== undefined && this.yNice !== false) {
        if (typeof yScale.nice === "function") {
          yScale.nice(typeof this.yNice === "number" ? this.yNice : undefined)
        } else {
          console.error(
            "[Piece of Cake] Can't use `yNice`, because `yScale` doesn't have a `nice` method. Ignoring…",
          )
        }
      }
      this.#yScale = yScale
    }
    return this.#yScale
  }

  get yValues(): number[] {
    if (this.#yValues === undefined) {
      this.#yValues = this.values.map(([, y]) => y)
    }
    return this.#yValues
  }

  get yValuesScaled(): number[] {
    if (this.#yValuesScaled === undefined) {
      const yScale = this.yScale
      this.#yValuesScaled = this.yValues.map((y) => yScale(y))
    }
    return this.#yValuesScaled
  }
}
