import { browser } from "$app/environment"
import type { ParametricReform } from "$lib/reforms"
import type { VariableValue } from "$lib/variables"

function trackEvent(category: string, action: string, name: string) {
  if (browser) {
    // @ts-expect-error: global variable needed by Matomo.
    window._paq ??= []
    // @ts-expect-error: global variable needed by Matomo.
    const _paq = window._paq
    _paq.push(["trackEvent", category, action, name])
  }
}

// export function trackEventSearch(
//   queries: string[],
//   clickedResult: string,
//   indexResult: number,
// ) {
//   trackEvent(
//     "user_interests",
//     "search",
//     JSON.stringify({
//       queries,
//       clickedResult,
//       indexResult,
//     }),
//   )
// }

function trackSearch(query: string, category: string, resultCount: number) {
  if (browser) {
    // @ts-expect-error: global variable needed by Matomo.
    window._paq ??= []
    // @ts-expect-error: global variable needed by Matomo.
    const _paq = window._paq
    _paq.push([
      "setCustomUrl",
      encodeURI(
        `?search_query=${query}&search_category=${category}&search_count=${resultCount}`,
      ),
    ])
    _paq.push(["trackPageView"])
  }
}

// function trackSearchResult(category: string, resultValue: string) {
//   if (browser) {
//     // @ts-expect-error: global variable needed by Matomo.
//     window._paq ??= []
//     // @ts-expect-error: global variable needed by Matomo.
//     const _paq = window._paq
//     _paq.push([
//       "setCustomUrl",
//       encodeURI(
//         `search_result/${category}/${resultValue}`,
//       ),
//     ])
//     _paq.push(["trackPageView"])
//   }
// }

// Global site-wide search tracking
export function trackSearchGlobal(query: string, resultCount: number) {
  const category = "global"
  trackSearch(query, category, resultCount)
}

// Variable search tracking
export function trackSearchVariable(
  query: string,
  variable: string,
  resultCount: number,
) {
  trackSearch(query, variable, resultCount)
}

// // Global site-wide search result tracking
// export function trackSearchResultGlobal(result: string) {
//   trackSearchResult("global", result)
// }

// trackEvent(
//   "discovery",
//   "search",
//   JSON.stringify({
//     queries,
//     clickedResult,
//     indexResult,
//   }),
// )

export function trackPageView(url: URL, prependDomain: boolean | undefined) {
  if (browser) {
    // @ts-expect-error: global variable needed by Matomo.
    window._paq ??= []
    // @ts-expect-error: global variable needed by Matomo.
    const _paq = window._paq
    _paq.push(["setCustomUrl", url.pathname + url.search])
    _paq.push([
      "setDocumentTitle",
      prependDomain ? document.domain + "/" + document.title : document.title,
    ])
    _paq.push(["trackPageView"])
  }
}

export function trackParametricReform(
  parametricReform: ParametricReform,
  prependDomain: boolean | undefined,
) {
  if (browser) {
    // @ts-expect-error: global variable needed by Matomo.
    window._paq ??= []
    // @ts-expect-error: global variable needed by Matomo.
    const _paq = window._paq
    _paq.push([
      "setCustomUrl",
      `/parametric_reform/${JSON.stringify(parametricReform)}`,
    ])
    _paq.push([
      "setDocumentTitle",
      prependDomain
        ? document.domain + "/Réforme paramétrique"
        : "Réforme paramétrique",
    ])
    _paq.push(["trackPageView"])
  }
}

/**
 * Law Edit tracking - modification de la loi (panneau gauche "droit en vigueur")
 */

export function trackLawEditParameterStatus() {
  trackEvent(
    "law_edit",
    "parameter_status",
    "Consultation dernière maj du paramètre",
  )
}

export function trackLawEditParameterHistory() {
  trackEvent(
    "law_edit",
    "parameter_history",
    "Consultation historique du paramètre",
  )
}

export function trackLawEditParameterArticles() {
  trackEvent(
    "law_edit",
    "parameter_articles",
    "Consultation articles du paramètre",
  )
}

/**
 * Budget tracking - impact budgétaire
 */

export function trackBudgetCalculateButton() {
  trackEvent(
    "budget",
    "calculate_button",
    "Clic bouton calculer l'impact budgétaire",
  )
}

export function trackBudgetSignInButton() {
  trackEvent("budget", "sign_in_button", "Clic bouton se connecter")
}

export function trackBudgetPublicSimulation() {
  trackEvent(
    "budget",
    "public_simulation",
    "Partage d'une simulation budgétaire",
  )
}

/**
 * Test Case tracking - impact cas type
 */

export function trackTestCaseEdit() {
  trackEvent("test_case", "edit", "Édition du cas type")
}

export function trackTestCaseLinked() {
  trackEvent("test_case", "linked", "Clic cas type lié")
}

export function trackTestCaseComparedLinked() {
  trackEvent("test_case", "linked", "Clic cas type lié comparé")
}

export function trackTestCaseShowAllVariables() {
  trackEvent(
    "test_case",
    "show_all_variables",
    "Affichage tous les dispositifs - waterfall",
  )
}

export function trackTestCaseShare() {
  trackEvent("test_case", "share", "Partage d'une simulation cas type")
}

export function trackTestCaseGraph(activity: VariableValue | undefined) {
  let variable = "revenu"
  switch (activity) {
    case "actif":
      variable = "salaire"
      break
    case "retraite":
      variable = "retraite"
      break
    case "chomeur":
      variable = "chômage"
      break
  }
  trackEvent("test_case", "show_graph", `Ouverture du graphique "${variable}"`)
}
