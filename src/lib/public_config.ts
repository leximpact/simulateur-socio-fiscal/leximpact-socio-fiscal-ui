import {
  PUBLIC_ADVANCED,
  PUBLIC_API_BASE_URLS,
  PUBLIC_TITLE,
  PUBLIC_BASE_URL,
  PUBLIC_CHILDREN_KEY,
  PUBLIC_FAMILY_KEY,
  PUBLIC_HIDDEN_ENTITIES,
  PUBLIC_HOUSEHOLD_KEY,
  PUBLIC_LEGAL_URL,
  PUBLIC_MATOMO_PREPEND_DOMAIN,
  PUBLIC_MATOMO_SUBDOMAINS,
  PUBLIC_MATOMO_SITE_ID,
  PUBLIC_MATOMO_URL,
  PUBLIC_OPENFISCA_BRANCH,
  PUBLIC_OPENFISCA_GROUP,
  PUBLIC_OPENFISCA_PROJECT,
  PUBLIC_OPENFISCA_REPOSITORY_RAW_URL_TEMPLATE,
  PUBLIC_OPENFISCA_REPOSITORY_URL_TEMPLATE,
  PUBLIC_PORTAL_URL,
  PUBLIC_REFORM,
  PUBLIC_REFORM_REVALUATION,
  PUBLIC_SHOW_TUTORIAL,
  PUBLIC_TAXABLE_HOUSEHOLD_KEY,
  PUBLIC_TERRITOIRES_URL,
} from "$env/static/public"
import { validatePublicConfig } from "$lib/auditors/public_config"
import type { RepositoryConfig } from "$lib/repositories"

export interface PublicConfig {
  advanced: boolean
  apiBaseUrls: string[]
  appTitle: string
  baseUrl: string
  childrenKey: string
  familyEntityKey: string
  hiddenEntitiesKeyPlural?: string[]
  householdEntityKey: string
  legalUrl: string
  matomo?: {
    prependDomain?: boolean
    siteId: number
    subdomains?: string
    url: string
  }
  openfiscaRepository: RepositoryConfig
  portalUrl: string
  reformName?: string
  revaluationName?: string
  showTutorial?: boolean
  taxableHouseholdEntityKey: string
  territoiresUrl: string
}

const [validConfig, error] = validatePublicConfig({
  advanced: PUBLIC_ADVANCED,
  apiBaseUrls: PUBLIC_API_BASE_URLS,
  appTitle: PUBLIC_TITLE,
  baseUrl: PUBLIC_BASE_URL,
  childrenKey: PUBLIC_CHILDREN_KEY,
  familyEntityKey: PUBLIC_FAMILY_KEY,
  hiddenEntitiesKeyPlural: PUBLIC_HIDDEN_ENTITIES,
  householdEntityKey: PUBLIC_HOUSEHOLD_KEY,
  legalUrl: PUBLIC_LEGAL_URL,
  matomo:
    PUBLIC_MATOMO_SITE_ID && PUBLIC_MATOMO_URL
      ? {
          prependDomain: PUBLIC_MATOMO_PREPEND_DOMAIN,
          siteId: PUBLIC_MATOMO_SITE_ID,
          subdomains: PUBLIC_MATOMO_SUBDOMAINS,
          url: PUBLIC_MATOMO_URL,
        }
      : null,
  openfiscaRepository: {
    branch: PUBLIC_OPENFISCA_BRANCH,
    group: PUBLIC_OPENFISCA_GROUP,
    project: PUBLIC_OPENFISCA_PROJECT,
    rawUrlTemplate: PUBLIC_OPENFISCA_REPOSITORY_RAW_URL_TEMPLATE,
    urlTemplate: PUBLIC_OPENFISCA_REPOSITORY_URL_TEMPLATE,
  },
  portalUrl: PUBLIC_PORTAL_URL,
  reformName: PUBLIC_REFORM,
  revaluationName: PUBLIC_REFORM_REVALUATION,
  showTutorial: PUBLIC_SHOW_TUTORIAL,
  taxableHouseholdEntityKey: PUBLIC_TAXABLE_HOUSEHOLD_KEY,
  territoiresUrl: PUBLIC_TERRITOIRES_URL,
}) as [PublicConfig, unknown]
if (error !== null) {
  console.error(
    `Error in configuration:\n${JSON.stringify(
      validConfig,
      null,
      2,
    )}\nError:\n${JSON.stringify(error, null, 2)}`,
  )
  process.exit(-1)
}
const publicConfig = validConfig

export default publicConfig
