import "intro.js/introjs.css"

import { type Waterfall } from "@openfisca/json-model"
import "iconify-icon"

import type { BudgetSimulation } from "$lib/budgets"
import {
  buildDecompositionByNameFromCore,
  decompositionCoreByName,
  decompositionCoreByNameByReformName,
  waterfalls,
  type EvaluationByName,
  type DecompositionByName,
} from "$lib/decompositions"
import type { DisplayMode } from "$lib/displays"
import { trackTestCaseShowAllVariables } from "$lib/matomo"
import { getNavbarConfig, type NavbarConfig } from "$lib/navbar"
import publicConfig from "$lib/public_config"
import { type ParametricReform, reformMetadataByName } from "$lib/reforms"
import {
  extractInputInstantsFromTestCases,
  testCasesCore,
  type Axis,
  type CalculationByName,
  type Situation,
} from "$lib/situations"
import { type ValuesByCalculationNameByVariableName } from "$lib/variables"

export interface Shared {
  axes: Axis[][]
  displayMode?: DisplayMode
  budgetSimulation?: BudgetSimulation
  calculationByName: CalculationByName
  decompositionByName: DecompositionByName
  evaluationByNameArray: EvaluationByName[]
  inputInstantsByVariableNameArray: Array<{
    [name: string]: Set<string>
  }>
  navbarConfig: NavbarConfig
  parametricReform: ParametricReform
  requestedSimulationEmail?: string // Budget simulation requests
  requestedSimulationSent: boolean // Budget simulation requests
  requestedVariablesNameToCalculate?: Set<string> // Budget simulation requests
  savedSituation?: Situation
  savedSituationIndex?: number
  searchActive: boolean // Search
  searchVariableName?: string // Search
  showNulls: boolean
  showTutorial: boolean
  testCases: Situation[]
  testCasesIndex: number[]
  valuesByCalculationNameByVariableNameArray: ValuesByCalculationNameByVariableName[]
  variableModalOpen: boolean
  vectorLength: number
  waterfall: Waterfall
}

const { reformName: reformBillName, revaluationName: reformRevaluationName } =
  publicConfig

/**
 * Bill (PLF) and revaluation (contrefactuel)
 */

export const billName: string | undefined =
  reformBillName === undefined
    ? undefined
    : reformMetadataByName[reformBillName] === undefined
      ? undefined
      : reformBillName
export const billActive: boolean = billName !== undefined

export const revaluationName: string | undefined =
  reformRevaluationName === undefined
    ? undefined
    : reformMetadataByName[reformRevaluationName] === undefined
      ? undefined
      : reformRevaluationName

const today = new Date()
export const year =
  today.getFullYear() + (today.getMonth() > 2 /* => After March */ ? 1 : 0)
export const yearPLF =
  today.getFullYear() +
  (revaluationName !== undefined && today.getMonth() > 2 /* => After March */
    ? 1
    : 0)
export const date = `${year}-01-01`
export const budgetDate = `${yearPLF}-01-01`

export const shared: Shared = $state({
  axes: [],
  calculationByName: {},
  // Note: We always use the reform decomposition, because it is more
  // complete than decomposition before reform.
  decompositionByName: buildDecompositionByNameFromCore(
    billName === undefined
      ? decompositionCoreByName
      : (decompositionCoreByNameByReformName[billName] ??
          decompositionCoreByName),
  ),
  evaluationByNameArray: new Array(testCasesCore.length).fill(
    {},
  ) as EvaluationByName[],
  inputInstantsByVariableNameArray:
    extractInputInstantsFromTestCases(testCasesCore),
  navbarConfig: getNavbarConfig("/"),
  parametricReform: {},
  requestedSimulationSent: false,
  searchActive: false,
  showNulls: false,
  showTutorial: false,
  testCases: structuredClone(testCasesCore),
  testCasesIndex: [],
  valuesByCalculationNameByVariableNameArray: new Array(
    testCasesCore.length,
  ).fill({}),
  variableModalOpen: false,
  vectorLength: 1,
  waterfall: waterfalls[0],
})

if (shared.showNulls) {
  trackTestCaseShowAllVariables()
}
