import { getUnitByName } from "$lib/units"

export function formatValue(value: number, unitName?: string): string {
  return valueFormatter(value, unitName)(value)
}

/// This function is needed, because Intl.NumberFormat option
/// `signDisplay: "negative"` is not yet supported by most browsers.
export function removeNegativeZero(value: number): number {
  return Object.is(value, -0) ? 0 : value
}

export function valueFormatter(
  baseValue: unknown,
  unitName?: string | undefined | null,
  compact = false,
): (value: unknown) => string {
  const unit = getUnitByName(unitName)
  return baseValue === undefined
    ? ((() => "") as (value: unknown) => string)
    : typeof baseValue === "boolean"
      ? (((value: boolean) => (value ? "vrai" : "faux")) as (
          value: unknown,
        ) => string)
      : typeof baseValue === "number"
        ? unit?.ratio // rate
          ? (value: unknown): string =>
              Math.abs(value as number) < 0.01
                ? "moins de 1%"
                : new Intl.NumberFormat("fr-FR", {
                    maximumFractionDigits: 2,
                    minimumFractionDigits: 0,
                    style: "percent",
                  }).format(removeNegativeZero(value as number))
          : unitName != null && unitName.startsWith("currency-")
            ? compact
              ? (value: unknown): string =>
                  new Intl.NumberFormat("fr-FR", {
                    currency: unitName.replace(/^currency-/, ""),
                    maximumFractionDigits: 2,
                    minimumFractionDigits: 0,
                    notation: "compact", // Example: 3,2 Md €
                    style: "currency",
                  }).format(removeNegativeZero(Math.round(value as number)))
              : (value: unknown): string =>
                  new Intl.NumberFormat("fr-FR", {
                    currency: unitName.replace(/^currency-/, ""),
                    maximumFractionDigits: 0,
                    minimumFractionDigits: 0,
                    style: "currency",
                  }).format(removeNegativeZero(value as number))
            : unitName === "people" && compact
              ? (value: unknown): string =>
                  new Intl.NumberFormat("fr-FR", {
                    maximumFractionDigits: 1,
                    minimumFractionDigits: 0,
                    notation: "compact", // Example: 3,2 M
                  }).format(removeNegativeZero(value as number))
              : (value: unknown): string =>
                  new Intl.NumberFormat("fr-FR").format(
                    removeNegativeZero(value as number),
                  )
        : (((value: unknown) => (value as string).toString()) as (
            value: unknown,
          ) => string)
}
