import { json } from "@sveltejs/kit"
import fs from "fs-extra"

import type { RequestHandler } from "./$types"

import { hashObject } from "$lib/hash"
import publicConfig from "$lib/public_config"
import {
  getTestCasesCacheDirPath,
  getTestCasesCacheFilePath,
} from "$lib/server/test_cases"

const { apiBaseUrls } = publicConfig

export const POST: RequestHandler = async ({ fetch, request }) => {
  const input = await request.clone().json()

  const digest = hashObject({
    period: input.period,
    parametric_reform: Object.entries(input.parametric_reform ?? {}).sort(
      ([parameterNameA], [parameterNameB]) =>
        parameterNameA.localeCompare(parameterNameB),
    ),
    reform: input.reform,
    situation: input.situation,
    // Ignore title.
    // Ignore token.
    variables: input.variables.sort(),
  })

  const cacheDir = getTestCasesCacheDirPath(digest)
  const cacheFilePath = getTestCasesCacheFilePath(digest)

  if (await fs.pathExists(cacheFilePath)) {
    return json(await fs.readJson(cacheFilePath))
  }

  const apiBaseUrlIndex = Math.floor(Math.random() * apiBaseUrls.length)
  const apiBaseUrl = apiBaseUrls[apiBaseUrlIndex]
  const response = await fetch(new URL("simulations", apiBaseUrl), {
    body: JSON.stringify(input),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json; charset=utf-8",
    },
    method: "POST",
  })
  if (!response.ok) {
    return response
  }

  const output = await response.json()

  if (!(await fs.pathExists(cacheFilePath))) {
    await fs.ensureDir(cacheDir)
    await fs.writeJson(cacheFilePath, output, {
      encoding: "utf-8",
      spaces: 2,
    })
  }
  return json(output)
}
