import {
  auditCleanArray,
  auditRequire,
  cleanAudit,
  type Audit,
} from "@auditors/core"
import { error, json } from "@sveltejs/kit"
import fs from "fs-extra"

import type { RequestHandler } from "./$types"

import { hashString } from "$lib/hash"
import {
  getTestCasesOpenGraphsDirPath,
  getTestCasesOpenGraphsFilePath,
  getTestCasesRequestsDirPath,
  getTestCasesRequestsFilePath,
} from "$lib/server/test_cases"

function auditBody(audit: Audit, dataUnknown: unknown): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "displayMode",
    true,
    errors,
    remainingKeys,
    // TODO
    auditRequire,
  )
  audit.attribute(
    data,
    "inputInstantsByVariableNameArray",
    true,
    errors,
    remainingKeys,
    // TODO
    auditRequire,
  )
  audit.attribute(
    data,
    "parametricReform",
    true,
    errors,
    remainingKeys,
    // TODO
    auditRequire,
  )
  audit.attribute(
    data,
    "testCases",
    true,
    errors,
    remainingKeys,
    auditCleanArray(),
    // TODO
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export const POST: RequestHandler = async ({ request, url }) => {
  const formData = await request.formData()

  const [body, bodyError] = auditBody(
    cleanAudit,
    JSON.parse(formData.get("body") as string),
  )
  if (bodyError !== null) {
    console.error(
      `Error in ${url.pathname} body:\n${JSON.stringify(
        body,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(bodyError, null, 2)}`,
    )
    error(400, `Invalid body: ${JSON.stringify(bodyError, null, 2)}`)
  }
  const bodyJson = JSON.stringify(body, null, 2)

  const digest = hashString(bodyJson)

  const blob = formData.get("blob") as Blob | null | undefined

  if (blob !== null && blob !== undefined) {
    const openGraphsDir = getTestCasesOpenGraphsDirPath(digest)
    const openGraphsFilePath = getTestCasesOpenGraphsFilePath(digest)
    if (!(await fs.pathExists(openGraphsFilePath))) {
      await fs.ensureDir(openGraphsDir)

      const fileStream = fs.createWriteStream(openGraphsFilePath)
      const arrayBuffer = await new File([blob], `${digest}.png`, {
        type: blob.type,
      }).arrayBuffer()
      const buffer = Buffer.from(arrayBuffer)
      fileStream.write(buffer)
      fileStream.end()
    }
  }

  const simulationDir = getTestCasesRequestsDirPath(digest)
  const simulationFilePath = getTestCasesRequestsFilePath(digest)
  if (!(await fs.pathExists(simulationFilePath))) {
    await fs.ensureDir(simulationDir)
    await fs.writeFile(simulationFilePath, bodyJson)
  }

  return json({ token: digest })
}
