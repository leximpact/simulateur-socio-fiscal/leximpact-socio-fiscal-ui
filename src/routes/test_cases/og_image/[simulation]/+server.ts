import fs from "fs-extra"

import type { RequestHandler } from "./$types"

import { getTestCasesOpenGraphsFilePath } from "$lib/server/test_cases"

export const GET: RequestHandler = async ({ params }) => {
  const { simulation: digest } = params as { simulation: string }

  const openGraphsFilePath = getTestCasesOpenGraphsFilePath(digest)
  const image = await fs.readFile(openGraphsFilePath)
  const response = new Response(image)
  response.headers.append("Content-Type", "image/png")
  response.headers.append(
    "Cache-Control",
    "s-maxage=604800, stale-while-revalidate=604800",
  )
  return response
}
