import { randomBytes } from "crypto"

import { strictAudit } from "@auditors/core"
import {
  auditEditableParameter,
  convertEditableParameterToRaw,
  ParameterClass,
  yamlFromRawParameter,
  type Parameter,
} from "@openfisca/json-model"
import { error } from "@sveltejs/kit"

import type { RequestHandler } from "./$types"

import { getParameter, rootParameter } from "$lib/parameters"
import publicConfig from "$lib/public_config"
import serverConfig from "$lib/server/server_config"
import { units } from "$lib/units"

const { openfiscaRepository } = publicConfig
const { githubPersonalAccessToken } = serverConfig

export const PUT: RequestHandler = async ({ request, params }) => {
  if (githubPersonalAccessToken === undefined) {
    error(
      403,
      "L'édition de paramètres est impossible car le serveur n'a pas de jeton d'accès à GitHub.",
    )
  }

  const { parameter: name } = params
  const parameter = getParameter(rootParameter, name)
  if (parameter === undefined) {
    error(404, `Paramètre ${name} non trouvé`)
  }
  const [validParameter, parameterError] = auditEditableParameter(units)(
    strictAudit,
    await request.json(),
  )
  if (parameterError !== null) {
    error(400, JSON.stringify(parameterError, null, 2))
  }

  // Note: validParameter has no file_path
  if (parameter.file_path === undefined) {
    error(
      400,
      `L'édition du paramètre "${name}" est impossible car le paramètre est généré automatiquement et n'a pas de fichier source.`,
    )
  }

  const rawParameter = convertEditableParameterToRaw(
    validParameter as Parameter,
  )

  // Get current SHA of parameter.
  const filePath =
    parameter.class === ParameterClass.Node
      ? parameter.file_path + "/index.yaml"
      : parameter.file_path
  const parameterContentUrl = `https://api.github.com/repos/${
    openfiscaRepository.group
  }/${
    openfiscaRepository.project
  }/contents/${filePath}?ref=${encodeURIComponent(openfiscaRepository.branch)}`
  const parameterContentResponse = await fetch(parameterContentUrl, {
    headers: {
      Accept: "application/vnd.github.v3+json",
      Authorization: `Token ${githubPersonalAccessToken}`,
    },
  })
  if (!parameterContentResponse.ok) {
    error(
      parameterContentResponse.status,
      await parameterContentResponse.text(),
    )
  }
  const parameterContent = await parameterContentResponse.json()

  // Get SHA of latest commit of OpenFisca branch.
  const sourceBranchUrl = `https://api.github.com/repos/${openfiscaRepository.group}/${openfiscaRepository.project}/git/refs/heads/${openfiscaRepository.branch}`
  const sourceBranchResponse = await fetch(sourceBranchUrl, {
    headers: {
      Accept: "application/vnd.github.v3+json",
      Authorization: `Token ${githubPersonalAccessToken}`,
    },
  })
  if (!sourceBranchResponse.ok) {
    error(sourceBranchResponse.status, await sourceBranchResponse.text())
  }
  const sourceBranch = await sourceBranchResponse.json()

  // Create new branch.
  const newBranchName = `parameter_${name}_${randomBytes(4).toString("hex")}`
  const newBranchUrl = `https://api.github.com/repos/${openfiscaRepository.group}/${openfiscaRepository.project}/git/refs`
  const newBranchResponse = await fetch(newBranchUrl, {
    body: JSON.stringify(
      { ref: `refs/heads/${newBranchName}`, sha: sourceBranch.object.sha },
      null,
      2,
    ),
    headers: {
      Accept: "application/vnd.github.v3+json",
      Authorization: `Token ${githubPersonalAccessToken}`,
    },
    method: "POST",
  })
  if (!newBranchResponse.ok) {
    error(newBranchResponse.status, await newBranchResponse.text())
  }

  // Update parameter & commit it.
  const parameterUpdateUrl = `https://api.github.com/repos/${openfiscaRepository.group}/${openfiscaRepository.project}/contents/${filePath}`
  const parameterUpdateResponse = await fetch(parameterUpdateUrl, {
    body: JSON.stringify(
      {
        branch: newBranchName,
        commiter: {
          author: {
            email: "test@example.com",
            name: "Jean Dupont",
          },
        },
        content: Buffer.from(yamlFromRawParameter(rawParameter)).toString(
          "base64",
        ),
        message: `Édition du paramètre ${name}`,
        sha: parameterContent.sha,
      },
      null,
      2,
    ),
    headers: {
      Accept: "application/vnd.github.v3+json",
      Authorization: `Token ${githubPersonalAccessToken}`,
    },
    method: "PUT",
  })
  if (!parameterUpdateResponse.ok) {
    error(parameterUpdateResponse.status, await parameterUpdateResponse.text())
  }
  /* const parameterUpdate = */ await parameterUpdateResponse.json()

  // Create pull request.
  const pullRequestUrl = `https://api.github.com/repos/${openfiscaRepository.group}/${openfiscaRepository.project}/pulls`
  const pullRequestResponse = await fetch(pullRequestUrl, {
    body: JSON.stringify(
      {
        base: openfiscaRepository.branch,
        // body:
        draft: false,
        head: newBranchName,
        // issue:
        maintainer_can_modify: true,
        title: `Édition du paramètre ${name}`,
      },
      null,
      2,
    ),
    headers: {
      Accept: "application/vnd.github.v3+json",
      Authorization: `Token ${githubPersonalAccessToken}`,
    },
    method: "POST",
  })
  if (!pullRequestResponse.ok) {
    error(pullRequestResponse.status, await pullRequestResponse.text())
  }
  /* const pullRequest = */ await pullRequestResponse.json()

  return new Response(undefined, { status: 204 })
}
