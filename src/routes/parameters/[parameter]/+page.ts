import { error } from "@sveltejs/kit"

import { billName } from "$lib/shared.svelte"
import {
  getParameter,
  rootParameter,
  rootParameterByReformName,
} from "$lib/parameters"

import type { PageLoad } from "./$types"

export const load: PageLoad = function ({ params }) {
  const { parameter: name } = params
  // Note: A reform parameters tree is always more complete than a parameters tree before reform.
  // And the children of a reform node parameter always contain the children of the node parameter
  // before reform (albeit with some different value parameters).
  const billRootParameter =
    billName === undefined
      ? rootParameter
      : (rootParameterByReformName[billName] ?? rootParameter)
  const billParameter = getParameter(billRootParameter, name)
  if (billParameter === undefined) {
    error(404, `Parameter "${name}" not found`)
  }
  const parameter = getParameter(rootParameter, name)
  return {
    billParameter,
    parameter,
  }
}
