import { strictAudit } from "@auditors/core"
import {
  auditRawParameterToEditable,
  ParameterClass,
  rawParameterFromYaml,
  type Parameter,
} from "@openfisca/json-model"
import { error } from "@sveltejs/kit"

import type { PageLoad } from "./$types"

import { getParameter, rootParameter } from "$lib/parameters"
import publicConfig from "$lib/public_config"
import { newParameterRepositoryRawUrl } from "$lib/repositories"
import { units } from "$lib/units"

const { openfiscaRepository } = publicConfig

export const load: PageLoad = async function ({ fetch, params }) {
  const { parameter: name } = params
  const processedParameter = getParameter(rootParameter, name)
  if (processedParameter === undefined) {
    error(404, `Parameter "${name}" not found`)
  }

  let unprocessedParameterUrl = newParameterRepositoryRawUrl(
    openfiscaRepository,
    processedParameter,
  )
  if (unprocessedParameterUrl === undefined) {
    error(
      400,
      `L'édition du paramètre "${processedParameter.name}" est impossible car le paramètre est généré automatiquement et n'a pas de fichier source.`,
    )
  }
  if (processedParameter.class === ParameterClass.Node) {
    unprocessedParameterUrl += "/index.yaml"
  }
  const unprocessedParameterResponse = await fetch(unprocessedParameterUrl)
  if (!unprocessedParameterResponse.ok) {
    error(500, `Could not load ${unprocessedParameterUrl}`)
  }
  const rawUnprocessedParameter = rawParameterFromYaml(
    await unprocessedParameterResponse.text(),
  )
  const [unprocessedParameter, unprocessedParameterError] =
    auditRawParameterToEditable(units)(
      strictAudit,
      rawUnprocessedParameter,
    ) as [Parameter, unknown]
  if (unprocessedParameterError !== null) {
    error(
      400,
      `Parameter "${processedParameter.name}" is not editable, because its raw unprocessed parameter YAML file is not valid`,
    )
  }

  return {
    parameter: unprocessedParameter,
    processedParameter,
  }
}
