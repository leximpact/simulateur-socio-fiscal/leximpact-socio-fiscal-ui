import { redirect } from "@sveltejs/kit"

import type { PageLoad } from "./$types"

export const load: PageLoad = function () {
  // Dummy page created only to ensure that Matomo fake URLs dont generate an error.
  redirect(301, "/")
}
