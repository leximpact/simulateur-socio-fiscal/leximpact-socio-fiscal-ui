import type { RequestHandler } from "./$types"

import publicConfig from "$lib/public_config"

export const GET: RequestHandler = async () => {
  const redirectUrl = new URL(
    `/auth/restore_state`,
    publicConfig.baseUrl,
  ).toString()
  return new Response(`Redirecting to ${redirectUrl}…`, {
    status: 302,
    headers: {
      location: redirectUrl,
    },
  })
}
