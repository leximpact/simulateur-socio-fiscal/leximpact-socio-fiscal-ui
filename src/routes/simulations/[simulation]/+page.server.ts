import {
  type Audit,
  auditRequire,
  auditTest,
  auditTrimString,
  cleanAudit,
} from "@auditors/core"
import { error, redirect } from "@sveltejs/kit"

import type { PageServerLoad } from "./$types"

function auditParams(audit: Audit, dataUnknown: unknown): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "simulation",
    true,
    errors,
    remainingKeys,
    auditTrimString,
    auditTest(
      (value) => /^[0-9a-f]{16}$/.test(value),
      "Invalid simulation token",
    ),
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export const load: PageServerLoad = async ({ params: requestParams, url }) => {
  const [params, paramsError] = auditParams(cleanAudit, requestParams)
  if (paramsError !== null) {
    console.error(
      `Error in ${url.pathname} params:\n${JSON.stringify(
        params,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(paramsError, null, 2)}`,
    )
    error(400, JSON.stringify(paramsError, null, 2))
  }
  const { simulation: digest } = params as { simulation: string }

  redirect(301, `/test_cases/simulations/${digest}`)
}
