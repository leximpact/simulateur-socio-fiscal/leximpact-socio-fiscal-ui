import path from "path"

import { json } from "@sveltejs/kit"
import fs from "fs-extra"

import type { RequestHandler } from "./$types"

import serverConfig from "$lib/server/server_config"
import type { CachedSimulation } from "$lib/simulations"

const { simulationsBudgetDir } = serverConfig

export const POST: RequestHandler = async () => {
  const indexDir = path.join(simulationsBudgetDir, "index.json")

  const cachedSimulations = (await fs.readJson(indexDir)) as CachedSimulation[]

  return json({
    simulations: (await fs.pathExists(indexDir))
      ? cachedSimulations
          .filter(
            (simulation) =>
              simulation.public && simulation.parameters.length > 0,
          )
          .toReversed()
      : [],
  })
}
