import path from "path"

import {
  type Audit,
  auditRequire,
  auditTest,
  auditTrimString,
  cleanAudit,
} from "@auditors/core"
import { error } from "@sveltejs/kit"
import fs from "fs-extra"

import type { PageServerLoad } from "./$types"

import type { DisplayMode } from "$lib/displays"
import type { ParametricReform } from "$lib/reforms"
import { getBudgetRequestsFilePath } from "$lib/server/budgets"
import serverConfig from "$lib/server/server_config"
import type { CachedSimulation } from "$lib/simulations"

const { simulationsBudgetDir } = serverConfig

function auditParams(audit: Audit, dataUnknown: unknown): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "simulation",
    true,
    errors,
    remainingKeys,
    auditTrimString,
    auditTest(
      (value) => /^[0-9a-f]{16}$/.test(value),
      "Invalid simulation token",
    ),
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export const load: PageServerLoad = async ({
  locals,
  params: requestParams,
  url,
}) => {
  const [params, paramsError] = auditParams(cleanAudit, requestParams)
  if (paramsError !== null) {
    console.error(
      `Error in ${url.pathname} params:\n${JSON.stringify(
        params,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(paramsError, null, 2)}`,
    )
    error(400, JSON.stringify(paramsError, null, 2))
  }
  const { simulation: digest } = params as { simulation: string }

  const indexFilePath = path.join(simulationsBudgetDir, "index.json")
  const indexContents: CachedSimulation[] = (await fs.pathExists(indexFilePath))
    ? await fs.readJson(indexFilePath)
    : []

  const simulationFilePath = getBudgetRequestsFilePath(digest)
  if (
    !(await fs.pathExists(simulationFilePath)) ||
    (locals.user === undefined &&
      !indexContents.find(
        (cachedSimulation) => cachedSimulation.hash === digest,
      )?.public)
  ) {
    error(404, `Simulation ${digest} not found`)
  }
  return {
    simulation: (await fs.readJson(simulationFilePath)) as {
      displayMode: DisplayMode
      parametricReform: ParametricReform
    },
  }
}
