import path from "path"

import { auditRequire, cleanAudit, type Audit } from "@auditors/core"
import { error, json } from "@sveltejs/kit"
import fs from "fs-extra"
import jwt from "jsonwebtoken"

import type { RequestHandler } from "./$types"

import type { DisplayMode } from "$lib/displays"
import type { ParametricReform } from "$lib/reforms"
import {
  getBudgetRequestsDirPath,
  getBudgetRequestsFilePath,
} from "$lib/server/budgets"
import serverConfig from "$lib/server/server_config"
import type { CachedSimulation } from "$lib/simulations"
import type { User } from "$lib/users"

const { simulationsBudgetDir } = serverConfig

function auditBody(audit: Audit, dataUnknown: unknown): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "parametricReform",
    true,
    errors,
    remainingKeys,
    // TODO
    auditRequire,
  )

  audit.attribute(
    data,
    "displayMode",
    true,
    errors,
    remainingKeys,
    // TODO
    auditRequire,
  )

  audit.attribute(
    data,
    "hash",
    true,
    errors,
    remainingKeys,
    // TODO
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export const POST: RequestHandler = async ({ locals, request, url }) => {
  let user = locals.user

  const requestJwtToken = request.headers.get("jwt-token")
  if (requestJwtToken !== null) {
    try {
      user = jwt.verify(requestJwtToken, serverConfig.jwtSecret) as User
    } catch (e) {
      console.warn(
        `Invalid JSON Web Token for id_token: ${requestJwtToken}. ${e}`,
      )
    }
  }

  if (user === undefined) {
    error(401, "Unauthorized")
  }

  const [body, bodyError] = auditBody(cleanAudit, await request.json())
  if (bodyError !== null) {
    console.error(
      `Error in ${url.pathname} body:\n${JSON.stringify(
        body,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(bodyError, null, 2)}`,
    )
    error(400, `Invalid body: ${JSON.stringify(bodyError, null, 2)}`)
  }

  const { parametricReform, displayMode, hash } = body as {
    parametricReform: ParametricReform
    displayMode: DisplayMode
    hash: string
  }

  /*
   *
   * Make public the request data (parametric reform and display mode)
   *
   * */
  const requestParamsFilePath = getBudgetRequestsFilePath(hash)
  if (!(await fs.pathExists(requestParamsFilePath))) {
    await fs.ensureDir(getBudgetRequestsDirPath(hash))
    await fs.writeFile(
      requestParamsFilePath,
      JSON.stringify({ parametricReform, displayMode }, null, 2),
    )
  }

  /*
   *
   * Modify index.json file to indicate cache is now public
   *
   * */
  const indexFilePath = path.join(simulationsBudgetDir, "index.json")
  const indexContents = fs.readJsonSync(indexFilePath) as CachedSimulation[]
  let found = false
  indexContents.forEach((cachedSimulation) => {
    if (cachedSimulation.hash === hash) {
      cachedSimulation.public = true
      found = true
    }
  })
  fs.writeFileSync(indexFilePath, JSON.stringify(indexContents, null, 2))

  return json({ hash, error: found ? undefined : "Hash not foud" })
}
