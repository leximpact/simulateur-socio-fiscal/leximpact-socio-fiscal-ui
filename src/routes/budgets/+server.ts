import path from "path"

import { error, json } from "@sveltejs/kit"
import fs from "fs-extra"
import jwt from "jsonwebtoken"

import type { RequestHandler } from "./$types"

import { hashObject } from "$lib/hash"
import { getParameter, rootParameter } from "$lib/parameters"
import type { ParametricReform } from "$lib/reforms"
import {
  getBudgetCacheDirPath,
  getBudgetCacheFilePath,
} from "$lib/server/budgets"
import serverConfig from "$lib/server/server_config"
import type { CachedSimulation } from "$lib/simulations"
import type { User } from "$lib/users"
import { budgetEditableParametersName } from "$lib/variables"

interface PayloadUser extends User {
  exp?: number
  iat?: number
}

const { simulationsBudgetDir } = serverConfig

function hashBudgetSimulationCache(
  parametricReform: ParametricReform,
  outputVariables: string[],
): string {
  return hashObject({
    parametricReform: Object.entries(parametricReform)
      .sort(([parameterNameA], [parameterNameB]) =>
        parameterNameA.localeCompare(parameterNameB),
      )
      .reduce((filtered: ParametricReform, [parameterName, value]) => {
        if (budgetEditableParametersName.has(parameterName)) {
          filtered[parameterName] = value
        }
        return filtered
      }, {}),
    outputVariables: outputVariables.sort().join("."),
  })
}

export const POST: RequestHandler = async ({ fetch, locals, request }) => {
  if (
    serverConfig.budgetApiUrl === undefined ||
    serverConfig.budgetJwtSecret === undefined
  ) {
    error(
      404,
      "La configuration de l'application ne permet pas d'accéder aux calculs budgétaires",
    )
  }

  let user = locals.user as PayloadUser

  // If request contains a user in its header, use that.
  const requestJwtToken = request.headers.get("jwt-token")
  if (requestJwtToken !== null) {
    try {
      user = jwt.verify(requestJwtToken, serverConfig.jwtSecret) as User
    } catch (e) {
      console.warn(
        `Invalid JSON Web Token for id_token: ${requestJwtToken}. ${e}`,
      )
    }
  }

  const requestJson = await request.clone().json()

  // Path of the file where the cached simulations are indexed
  const simulationsIndexFilePath = path.join(simulationsBudgetDir, "index.json")
  let simulationsIndexContents: CachedSimulation[] = (await fs.pathExists(
    simulationsIndexFilePath,
  ))
    ? await fs.readJson(simulationsIndexFilePath)
    : []

  // Path of the no-reform simulation cache (hashing empty object)
  const baseCacheDigest = hashBudgetSimulationCache(
    {},
    requestJson.output_variables,
  )

  // Path of the no-reform simulation cache
  const baseCacheFilePath = getBudgetCacheFilePath(baseCacheDigest)

  // Hash digest of the current simulation
  const simulationCacheDigest = hashBudgetSimulationCache(
    requestJson.amendement,
    requestJson.output_variables,
  )

  // Path of the current simulation's cache
  const simulationCacheFilePath = getBudgetCacheFilePath(simulationCacheDigest)

  // Indicates if the current simulation is calculated and is public
  const isSimulationPublic = simulationsIndexContents.find(
    (cachedSimulation) => cachedSimulation.hash === simulationCacheDigest,
  )?.public

  if (
    (await fs.pathExists(simulationCacheFilePath)) &&
    (user !== undefined || isSimulationPublic)
  ) {
    // If cache exists and user has permission to see it.
    // User permission logic:
    //     - if authenticated => always allowed
    //     - if unauthenticated => allowed if simulation is public

    return json({
      errors: [],
      result: await fs.readJson(simulationCacheFilePath),
      hash: simulationCacheDigest,
      isPublic: isSimulationPublic,
    })
  } else if (user === undefined) {
    // If the user doesn't have permission to calculate a budget simulation.
    if (
      isSimulationPublic ||
      (baseCacheDigest == simulationCacheDigest &&
        !(await fs.pathExists(baseCacheFilePath)))
    ) {
      // If cache exists in the index and is already public
      // (or simulation is a base cache)
      // but the path doesn't exist (no data),
      // calculate it again using a dummy user.

      const timestamp = Math.floor(Date.now() / 1000) // in seconds
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      user = {
        iat: timestamp,
        exp: timestamp + 60 * 60, // + 1 hour in seconds
        email: "emmanuel@batache.com",
      }
    } else if (await fs.pathExists(baseCacheFilePath)) {
      // If cache doesn't exist, try to show the base simulation.
      return json({
        errors: [],
        result: await fs.readJson(baseCacheFilePath),
        hash: baseCacheDigest,
        isPublic: true,
      })
    } else {
      // If everything else fails, show an error.
      //
      // This shouldn't happen, because base path should always be calculated
      // upon visiting the simulator (with a dummy user if necessary) since it's public.
      return json({
        errors: [
          "Nous sommes désolés, les impacts budgétaires ne sont pas disponibles pour le moment. Si vous êtes habilité, veuillez-vous connecter avec la clé en haut à droite. Autrement, écrivez-nous à leximpact@assemblee-nationale.fr.",
        ],
      })
    }
  }

  // Generate the payload for the budget calculations API
  const payload = {
    exp: user.exp,
    iat: user.iat,
    user: { ...user },
  }
  delete payload.user.exp
  delete payload.user.iat

  // Request calculation of the current reform
  const response = await fetch(serverConfig.budgetApiUrl, {
    body: await request.arrayBuffer(),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json; charset=utf-8",
      "jwt-token": jwt.sign(payload, serverConfig.budgetJwtSecret),
    },
    method: "POST",
  })

  if (!response.ok) {
    // Gateway timeout, etc.
    // Budget API never returns a JSON when a status other than 200 occurs.
    console.error(
      `An error occurred while calling budget API at ${serverConfig.budgetApiUrl}`,
    )
    console.error(`${response.status} ${response.statusText}`)
    console.error(await response.text())
    return json({
      errors: [
        `Une erreur inattendue (${response.status} ${response.statusText}) s'est produite et les impacts budgétaires ne sont pas disponibles pour le moment. Écrivez-nous à leximpact@assemblee-nationale.fr.`,
      ],
    })
  }

  const responseJson = await response.json()

  // Add necessary information to the response JSON:

  // Add hash digest
  responseJson.hash = simulationCacheDigest

  // Add flag indicating whether the simulation is public or not
  responseJson.isPublic =
    simulationCacheDigest === baseCacheDigest || isSimulationPublic

  if (
    Object.keys(responseJson.result ?? {}).length > 0 &&
    (responseJson.errors === undefined ||
      responseJson.errors === null ||
      responseJson.errors?.length === 0)
  ) {
    // If response is OK, cache the calculated simulation.

    const modifiedParametersTitles = Object.keys(requestJson.amendement).map(
      (parameterName) => getParameter(rootParameter, parameterName)?.title,
    )

    const modifiedParametersNames = Object.keys(requestJson.amendement).map(
      (parameterName) => getParameter(rootParameter, parameterName)?.name,
    )

    if (!(await fs.pathExists(simulationCacheFilePath))) {
      await fs.ensureDir(getBudgetCacheDirPath(simulationCacheDigest))
      await fs.writeFile(
        simulationCacheFilePath,
        JSON.stringify(responseJson.result, null, 2),
      )
    }

    if (fs.pathExistsSync(simulationsIndexFilePath)) {
      simulationsIndexContents = fs.readJsonSync(simulationsIndexFilePath)
    }
    if (
      !simulationsIndexContents.find(
        (simulation) => simulation.hash === simulationCacheDigest,
      )
    ) {
      simulationsIndexContents.push({
        date: new Intl.DateTimeFormat("fr-FR").format(new Date()),
        hash: simulationCacheDigest,
        output_variables: requestJson.output_variables,
        public: responseJson.isPublic,
        parameters: modifiedParametersNames,
        title: modifiedParametersTitles.join(" | "),
      } as CachedSimulation)

      fs.writeFileSync(
        simulationsIndexFilePath,
        JSON.stringify(simulationsIndexContents, null, 2),
      )

      if (responseJson.isPublic) {
        const publisherRes = await fetch("/budgets/simulations", {
          body: JSON.stringify({
            displayMode: requestJson.displayMode,
            hash: simulationCacheDigest,
            parametricReform: requestJson.amendement,
          }),
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json; charset=utf-8",
            "jwt-token": jwt.sign(user, serverConfig.jwtSecret),
          },
          method: "POST",
        })
        if (!publisherRes.ok) {
          console.error(
            `Error ${
              publisherRes.status
            } while sharing public budget simulation\n\n${await publisherRes.text()}`,
          )
        }
      }
    }
  } else {
    // If response is not OK, send an error.
    responseJson.errors = [
      ...(responseJson.errors ?? []),
      `${response.status} - ${response.statusText}. Une erreur inattendue s'est produite et les impacts budgétaires ne sont pas disponibles pour le moment. Écrivez-nous à leximpact@assemblee-nationale.fr.`,
    ]
  }

  return json(responseJson)
}
