import {
  auditRequire,
  auditEmail,
  cleanAudit,
  type Audit,
} from "@auditors/core"
import { error, json } from "@sveltejs/kit"

import type { RequestHandler } from "./$types"

import type { DisplayMode } from "$lib/displays"
import publicConfig from "$lib/public_config"
import type { ParametricReform } from "$lib/reforms"
import serverConfig from "$lib/server/server_config"

function auditBody(audit: Audit, dataUnknown: unknown): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "displayMode",
    true,
    errors,
    remainingKeys,
    // TODO
    auditRequire,
  )

  audit.attribute(
    data,
    "email",
    true,
    errors,
    remainingKeys,
    // TODO
    auditEmail,
    auditRequire,
  )

  audit.attribute(
    data,
    "request",
    true,
    errors,
    remainingKeys,
    // TODO
    auditRequire,
  )

  audit.attribute(
    data,
    "simulation",
    true,
    errors,
    remainingKeys,
    // TODO
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export const POST: RequestHandler = async ({ fetch, request, url }) => {
  const [body, bodyError] = auditBody(cleanAudit, await request.clone().json())
  if (bodyError !== null) {
    console.error(
      `Error in ${url.pathname} body:\n${JSON.stringify(
        body,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(bodyError, null, 2)}`,
    )
    error(400, `Invalid body: ${JSON.stringify(bodyError, null, 2)}`)
  }

  const {
    displayMode,
    email,
    request: parametricReform,
    simulation,
  } = body as {
    displayMode: DisplayMode
    email: string
    request: ParametricReform
    simulation: unknown
  }

  const response = await fetch(serverConfig.budgetDemandPipelineUrl!, {
    body: JSON.stringify({
      variables: [
        { key: "DISPLAY_MODE", value: JSON.stringify(displayMode) },
        { key: "EMAIL", value: email },
        { key: "SIMULATION_REQUEST", value: JSON.stringify(parametricReform) },
        { key: "SIMULATION", value: JSON.stringify(simulation) },
        {
          key: "USE_PROD",
          value: JSON.stringify(
            publicConfig.baseUrl.includes("socio-fiscal.leximpact.an.fr"),
          ),
        },
      ],
    }),
    headers: {
      "Content-Type": "application/json; charset=utf-8",
      "PRIVATE-TOKEN": serverConfig.budgetDemandPipelineToken!,
    },
    method: "POST",
  })

  if (!response.ok) {
    const message = "Error while demanding a budget simulation calculation"
    console.error(message)
    console.error(`${response.status} ${response.statusText}`)
    console.error(await response.text())
    error(500, message)
  }

  return json({})
}
