import { json } from "@sveltejs/kit"

import type { RequestHandler } from "./$types"

import { metadata } from "$lib/metadata"

export const GET: RequestHandler = () => {
  return json({ metadata })
}
