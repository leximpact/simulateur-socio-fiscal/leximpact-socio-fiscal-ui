import type { LayoutLoad } from "./$types"

export const load: LayoutLoad = function ({ data, fetch }) {
  return {
    ...data,
    fetch,
  }
}
