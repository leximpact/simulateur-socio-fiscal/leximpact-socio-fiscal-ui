import type { LayoutServerLoad } from "./$types"

import serverConfig from "$lib/server/server_config"
import type { User } from "$lib/users"

export const load: LayoutServerLoad = async (
  event,
): Promise<{
  authenticationEnabled: boolean
  canDemandBudgetSimulation: boolean
  hasGithubPersonalAccessToken: boolean
  user?: User
}> => {
  const { locals } = event
  const { user } = locals
  return {
    authenticationEnabled: serverConfig.openIdConnect !== undefined,
    canDemandBudgetSimulation:
      serverConfig.budgetDemandPipelineUrl !== undefined &&
      serverConfig.budgetDemandPipelineToken !== undefined,
    hasGithubPersonalAccessToken:
      serverConfig.githubPersonalAccessToken !== undefined,
    user,
  }
}
