import type { RequestHandler } from "@sveltejs/kit"
import dedent from "dedent-js"

import serverConfig from "$lib/server/server_config"

const { allowRobots } = serverConfig

export const GET: RequestHandler = async () => {
  return new Response(
    dedent`
      # https://www.robotstxt.org/robotstxt.html
      User-agent: *
      Disallow:${allowRobots ? "" : " /"}
    `,
    { headers: { "Content-Type": "text/plain; charset=utf-8" } },
  )
}
