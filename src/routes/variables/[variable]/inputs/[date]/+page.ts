import { error } from "@sveltejs/kit"

import type { PageLoad } from "./$types"

import {
  iterVariableInputVariables,
  variableSummaryByName,
} from "$lib/variables"

export const load: PageLoad = function ({ params }) {
  const { date, variable: name } = params
  const variable = variableSummaryByName[name]
  if (variable === undefined) {
    error(404, `Variable "${name}" not found`)
  }
  const inputs = [...iterVariableInputVariables(variable, date, new Set())]
  return {
    inputs,
    variable,
  }
}
