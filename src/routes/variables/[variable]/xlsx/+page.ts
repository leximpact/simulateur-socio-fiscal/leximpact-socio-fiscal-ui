import type { Variable } from "@openfisca/json-model"
import { error } from "@sveltejs/kit"

import type { PageLoad } from "./$types"

export const load: PageLoad = async function ({ fetch, params }) {
  const { variable: name } = params
  const response = await fetch(`/variables/${encodeURIComponent(name)}/ast`, {
    headers: { Accept: "application/json" },
  })
  if (!response.ok) {
    error(response.status, `Could not load AST`)
  }
  const { variables } = (await response.json()) as { variables: Variable[] }

  return {
    variables,
  }
}
