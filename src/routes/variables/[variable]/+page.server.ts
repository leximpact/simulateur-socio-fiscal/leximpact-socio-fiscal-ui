import path from "path"

import { error } from "@sveltejs/kit"
import fs from "fs-extra"
import sanitizeFilename from "sanitize-filename"

import type { PageServerLoad } from "./$types"

export const load: PageServerLoad = async ({ params }) => {
  const { variable: name } = params
  const variableFilePath = path.join(
    "node_modules",
    "@leximpact",
    "socio-fiscal-openfisca-json",
    "variables",
    `${sanitizeFilename(name)}.json`,
  )
  if (!(await fs.pathExists(variableFilePath))) {
    error(404, `Variable ${name} not found`)
  }
  const variable = await fs.readJson(variableFilePath)
  return { variable }
}
