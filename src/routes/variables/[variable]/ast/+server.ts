import path from "path"

import {
  type Audit,
  auditRequire,
  auditTrimString,
  cleanAudit,
  auditFunction,
} from "@auditors/core"
import {
  getVariableLatestFormulaDate,
  type Variable,
} from "@openfisca/json-model"
import { error, json } from "@sveltejs/kit"
import fs from "fs-extra"
import sanitizeFilename from "sanitize-filename"

import type { RequestHandler } from "./$types"

const variablesDir = path.join(
  "node_modules",
  "@leximpact",
  "socio-fiscal-openfisca-json",
  "variables",
)

/// Retrieve recursively all variables used by a variable,
/// using a depth first approach to ensure that input variables are
/// first in sequence.
async function addVariableAndInputs(
  name,
  encounteredVariablesName = new Set<string>(),
  variables: Variable[] = [],
): Promise<Variable[]> {
  encounteredVariablesName.add(name)
  const variableFilePath = path.join(variablesDir, `${name}.json`)
  const variable = (await fs.readJson(variableFilePath)) as Variable
  const latestFormulaDate = getVariableLatestFormulaDate(variable)
  if (latestFormulaDate !== null) {
    const latestFormula = variable.formulas[latestFormulaDate]
    const inputVariablesName = latestFormula.variables
    if (inputVariablesName !== undefined) {
      for (const inputVariableName of inputVariablesName) {
        if (!encounteredVariablesName.has(inputVariableName)) {
          await addVariableAndInputs(
            inputVariableName,
            encounteredVariablesName,
            variables,
          )
        }
      }
    }
  }
  variables.push(variable)
  return variables
}

function auditParams(audit: Audit, dataUnknown: unknown): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "variable",
    true,
    errors,
    remainingKeys,
    auditTrimString,
    auditFunction(sanitizeFilename),
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export const GET: RequestHandler = async ({ params: requestParams, url }) => {
  const [params, paramsError] = auditParams(cleanAudit, requestParams)
  if (paramsError !== null) {
    console.error(
      `Error in ${url.pathname} params:\n${JSON.stringify(
        params,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(paramsError, null, 2)}`,
    )
    error(400, JSON.stringify(paramsError, null, 2))
  }
  const { variable: name } = params as { variable: string }
  // TODO: Handle reforms (=> search variable in reform directory before searching it
  // in main directory).
  const variableFilePath = path.join(variablesDir, `${name}.json`)
  if (!(await fs.pathExists(variableFilePath))) {
    error(404, `Variable ${name} not found`)
  }

  const variables = await addVariableAndInputs(name)
  return json({ variables })
}
