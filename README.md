# leximpact-socio-fiscal-ui: Interface du simulateur socio-fiscal LexImpact

Ceci est le code source de l'application [simulateur-socio-fiscal.leximpact.dev](https://simulateur-socio-fiscal.leximpact.dev).
Il comprend une représentation graphique des effets de la loi sociale et fiscale sur des situations de personnes.

## Présentation

Ce projet est construit sur la base du framework JavaScript [SvelteKit](https://kit.svelte.dev/) (et [TailwindCSS](https://tailwindcss.com/)).

Il emploie l'API Web définie par [leximpact-socio-fiscal-api](https://git.leximpact.dev/leximpact/leximpact-socio-fiscal-api) pour faire appel au moteur de microsimulation socio-fiscale [OpenFisca-France](https://github.com/openfisca/openfisca-france) et calculer les effets de la loi.

## Installation

Ce projet fonctionne avec [NodeJS](https://nodejs.org/fr/) version 16 ou supérieure.

Exécuter les commandes ci-dessous pour télécharger le projet, vous déplacer dans son répertoire, puis installer les librairies nécessaires à son fonctionnement :

```shell
git clone https://git.leximpact.dev/leximpact/leximpact-socio-fiscal-ui
cd leximpact-socio-fiscal-ui
npm install
```

L'installation indique les librairies installées et doit s'achever sans erreur.

## Exécuter l'application - mode développeur

Dans le répertoire de l'application `leximpact-socio-fiscal-ui/`, créer un fichier `.env`, par exemple en faisant un lien vers `example.env` (ou en l'adaptant à vos besoins) :

```shell
ln -s example.env .env
```

Puis, exécuter la commande :

```shell
npm run dev
```

La commande conserve la main tout en restant à l'écoute des modifications de code.

L'application peut alors être consultée dans un navigateur à l'adresse indiquée (par défaut : `http://localhost:3000`), mais la première fois il peut être nécessaire de la recharger plusieurs fois, le temps que toutes les dépendances se compilent.

Bravo ! Vous êtes prêts à utiliser et contribuer à `leximpact-socio-fiscal-ui` 🎉

## Documentation

### Afficher un contenu uniquement en mode PLF/PLFSS

Il faut utiliser la variable de contexte `billActive`

Ajouter en haut du fichier :

```js
  const billActive = getContext("billActive") as Writable<boolean>
```

Et ensuite utiliser un test :

```js
{#if !billActive}
  "COUCOU qui s'affiche si le PLF n'est pas activé"
{/if}
```

```js
{#if billActive}
  "COUCOU qui s'affiche si le PLF est activé"
{/if}
```

billActive => plf activé

### Ajouter une variable calculée dans le simulateur

Dans l'application, seules les variables indiquées dans la feuille de paie sont calculées. Pour afficher les résultats d'une autre variable il faut donc demander à l'application de la récuéprer.

#### Variable affichée dans l'étiquette du cas type :

1. Dans le fichier variables.ts, ajouter le nom de la viariable OpenFisca :

```js
/// test case summaries.
export const summaryCalculatedVariablesName = [
  "assiette_csg_revenus_capital",
  "assiette_csg_plus_values",
  "niveau_de_vie",
  "plus_values_base_large",
  "rente_viagere_titre_onereux_net",
  "rsa",
  "zone_apl",
```

2. Dans le fichier TestCaseSummary.svelte, Appeler la variable dans la bonne catégorie (Ménage, foyer fiscal, individu, etc), comme fait dans cet exemple avec la variable `zone_apl`

```js
{@const zone_apl =
getCalculatedVariableNumberValue(
situation,
valuesByCalculationNameByVariableName,
"zone_apl",
populationId,
) ?? 0}
```

3. Ensuite, appeler la variable dans le composant choisi :

```js
<div>{zone_apl}</div>
```

#### Variable affichée dans la décomposition :

1. Dans le fichier variables.ts, ajouter le nom de la viariable OpenFisca :

```js
/// Autres variables à calculer
export const otherCalculatedVariablesName = [
  "niveau_de_vie",
  "unites_consommation",
  "taxes_tous_carburants",
]
```

2. Dans le fichier souhaité, Appeler la variable dans la bonne catégorie (Ménage, foyer fiscal, individu, etc), comme fait dans cet exemple avec la variable `niveau_de_vie` dans le fichier `PaySlipView.svelte` :

```js
{@const niveau_de_vie =
getCalculatedVariableNumberValue(
    situation,
    valuesByCalculationNameByVariableName,
    "niveau_de_vie",
    populationId,
) ?? 0}
```

3. Ensuite, appeler la variable dans le composant choisi :

```js
{#if niveau_de_vie !== 0}
    <div
        class="border-t flex items-center justify-between text-gray-500"
    >
        <a
        href={newSimulationUrl({
            ...displayMode,
            parametersVariableName: "niveau_de_vie",
        })}
        class="border-t border-gray-400 ml-4 hover:underline"
        >Niveau de vie</a
        >
        <p class="flex text-sm mr-4 border-t border-gray-400 p-0.5 gap-1">
        = <ValueChange
            unitName="currency-EUR"
            valueByCalculationName={getCalculatedVariableValueByCalculationName(
            situation,
            valuesByCalculationNameByVariableName,
            variableSummaryByName["niveau_de_vie"],
            populationId,
            )}
        />
        </p>
    </div>
{/if}
```

Utiliser le composant `ValueChange` pour afficher le montant.

### Ajouter la valeur d'un paramètre dans l'UI

Exemple ici avec l'ajout du paramètre smic mensuel `marche_travail.salaire_minimum.smic.smic_b_mensuel` dans le `TestCaseSummary.svelte`. L'objectif était d'afficher l'équivalent du revenu du cas type en nombre de smic. Il était donc nécessaire d'importer la dernière valeur du paramètre dans l'UI.

1. Dans le fichier visé, ici `TestCaseSummary.svelte`, ajouter :

```js
$: parameterSmicMensuel = getParameter(
    rootParameter,
    "marche_travail.salaire_minimum.smic.smic_b_mensuel",
  ) as ValueParameter
## On utilise la fonction getParameter pour aller chercher le paramètre dans openfisca. À la fin on précise as ValueParameter si c'est un paramètre value, ou as ScaleParameter si c'est un barème. Attention il faut importer ValueParameter (import type { ValueParameter} from from "@openfisca/json-model")

$: smicInstantValueCouplesArray = Object.entries(
parameterSmicMensuel?.values ?? [],
).sort(([instant1], [instant2]) => instant2.localeCompare(instant1))

## Ici, comme le paramètre a plusieurs valeurs dans le temps, on lui dit de trier les valeurs par date décroissante.

$: smicLatestInstantValueCouple = smicInstantValueCouplesArray[0]

## Ici, on lui demande donc de prendre le premier couple (date,valeur) de ce tableau.

$: smicValue = smicLatestInstantValueCouple?.[1] as NumberValue

## Enfin ici, on lui dit de prendre la valeur, c'est à dire le deuxième élément du tableau. Attention il faut importer NumberValue (import type { NumberValue} from from "@openfisca/json-model")
```

2- Il ne faut pas oublier de faire les imports et les déclarations pour que ces fonctions puissent s'effectuer :

```js
  import type {
    ...
    NumberValue,
    ValueAtInstant,
    ValueParameter,
  } from "@openfisca/json-model"

  import {
    ...
    getParameter,
    rootParameter,
  } from "$lib/parameters"


  let smicInstantValueCouplesArray: [string, ValueAtInstant][]

```

3- Appeler le paramètre dans le fichier :

```js
<div>
  {smicValue.value}
  ## Ne pas oublier d'ajouter .value, pour cibler la value dans cet objet.
</div>
```

Pour s'aider, on peut afficher la valeur dans la console :

```js
$: console.log(parameterSmicMensuel, smicValue)
```

### Générer les YAML de test OpenFisca à partir des cas-type LexImpact

```bash
npx tsx src/scripts/generate_openfisca_tests_yaml.ts -y 2025 ../../openfisca-france/tests/leximpact/
```

- Le paramètre `year` générera les variables d'output pour l'année `year`
- L'argument par défaut `outdir` est le chemin où l'on veut exporter les YAML générés. Ceux-ci ont vocation à être pushés dans openfisca_france/tests/leximpact

Les YAML générés contiennent, pour chaque cas-type, toutes les variables d'entrées du cas-type dans le fichier `test_cases.json`.
Les valeurs sont mensualisées pour toutes les variables mensuelles.

La section `output:` du YAML contient toutes les variables calculées, présentes à la fois dans les décompositions LexImpact **et** dans les variables du country-package Openfisca-France.

Les valeurs des variables de type Enum, sont remplacées par leur index dans les valeurs possibles de l'Enum.
