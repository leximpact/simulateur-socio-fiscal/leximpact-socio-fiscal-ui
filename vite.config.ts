import { sveltekit } from "@sveltejs/kit/vite"
import yaml from "js-yaml"
import { defineConfig } from "vite"

// import yamlPlugin from "@rollup/plugin-yaml"
import yamlPlugin from "./plugin-yaml-patched"

export default defineConfig({
  build: {
    // Increase size of chunks to 5 MB, to be able to import
    // JSON files extracted from OpenFisca country package.
    chunkSizeWarningLimit: 7680,
  },
  plugins: [
    yamlPlugin({
      schema: yaml.JSON_SCHEMA, // Keep dates as strings.
    }), // To import YAML files
    sveltekit(),
  ],
  ssr: {
    noExternal: ["d3-scale"],
  },
})
